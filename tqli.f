      subroutine tred2(a,n,np,d,e)
      implicit double precision(a-h,o-z)
      dimension a(np,np),d(np),e(np)
      if(n.gt.1)then
        do 18 i=n,2,-1  
          l=i-1
          h=0.
          scale=0.d0
          if(l.gt.1)then
            do 11 k=1,l
              scale=scale+abs(a(i,k))
11          continue
            if(scale.eq.0.d0)then
              e(i)=a(i,l)
            else
              do 12 k=1,l
                a(i,k)=a(i,k)/scale
                h=h+a(i,k)**2
12            continue
              f=a(i,l)
              g=-sign(sqrt(h),f)
              e(i)=scale*g
              h=h-f*g
              a(i,l)=f-g
              f=0.
              do 15 j=1,l
                a(j,i)=a(i,j)/h
                g=0.d0
                do 13 k=1,j
                  g=g+a(j,k)*a(i,k)
13              continue
                if(l.gt.j)then
                  do 14 k=j+1,l
                    g=g+a(k,j)*a(i,k)
14                continue
                endif
                e(j)=g/h
                f=f+e(j)*a(i,j)
15            continue
              hh=f/(h+h)
              do 17 j=1,l
                f=a(i,j)
                g=e(j)-hh*f
                e(j)=g
                do 16 k=1,j
                  a(j,k)=a(j,k)-f*e(k)-g*a(i,k)
16              continue
17            continue
            endif
          else
            e(i)=a(i,l)
          endif
          d(i)=h
18      continue
      endif
      d(1)=0.d0
      e(1)=0.d0
      do 23 i=1,n
        l=i-1
        if(d(i).ne.0.d0)then
          do 21 j=1,l
            g=0.d0
            do 19 k=1,l
              g=g+a(i,k)*a(k,j)
19          continue
            do 20 k=1,l
              a(k,j)=a(k,j)-g*a(k,i)
20          continue
21        continue
        endif
        d(i)=a(i,i)
        a(i,i)=1.
        if(l.ge.1)then
          do 22 j=1,l
            a(i,j)=0.d0
            a(j,i)=0.d0
22        continue
        endif
23    continue
      return
      end

      subroutine tqli(d,e,n,np,z)
      implicit double precision(a-h,o-z)
      dimension d(np),e(np),z(np,np)
      if (n.gt.1) then
        do 11 i=2,n
          e(i-1)=e(i)
11      continue
        e(n)=0.d0
        do 15 l=1,n
          iter=0
1         do 12 m=l,n-1
            dd=dabs(d(m))+dabs(d(m+1))
            if (dabs(e(m))+dd.eq.dd) go to 2
12        continue
          m=n
2         if(m.ne.l)then
            if(iter.eq.30) then
		write(33,*) 'too many iterations'
		goto 15
		endif
            iter=iter+1
            g=(d(l+1)-d(l))/(2.d0*e(l))
            r=dsqrt(g**2+1.d0)
            g=d(m)-d(l)+e(l)/(g+sign(r,g))
            s=1.d0
            c=1.d0
            p=0.d0
            do 14 i=m-1,l,-1
              f=s*e(i)
              b=c*e(i)
              if(dabs(f).ge.dabs(g))then
                c=g/f
                r=dsqrt(c**2+1.d0)
                e(i+1)=f*r
                s=1.d0/r
                c=c*s
              else
                s=f/g
                r=dsqrt(s**2+1.d0)
                e(i+1)=g*r
                c=1.d0/r  
                s=s*c
              endif
              g=d(i+1)-p
              r=(d(i)-g)*s+2.d0*c*b
              p=s*r
              d(i+1)=g+p
              g=c*r-b
              do 13 k=1,n
 	       f=z(k,i+1)
 	       z(k,i+1)=s*z(k,i)+c*f
 	       z(k,i)=c*z(k,i)-s*f
13            continue
14          continue
            d(l)=d(l)-p
            e(l)=g
            e(m)=0.d0
            go to 1
          endif
15      continue
      endif
      return
      end
      
      subroutine eigsrt(d,v,n,np)
      real*8 d(np),v(np,np)
      integer n,np
      integer i,j,k
      real*8 p
      do 13 i=1,n-1
        k=i
        p=d(i)
        do 11 j=i+1,n
          if(d(j).le.p)then
            k=j
            p=d(j)
          endif
11      continue
        if(k.ne.i)then
          d(k)=d(i)
          d(i)=p
          do 12 j=1,n
            p=v(j,i)
            v(j,i)=v(j,k)
            v(j,k)=p
12        continue
        endif
13    continue
      return
      end

