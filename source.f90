module global_parameters
implicit none
integer*4                    :: nonp,ngpt,ngpx,ngpu,np,lp,nh,lh,par
integer*4                    :: pmin,pmax,occ
real*8                       :: fo,omegao,ekmax,ekmin,dgam,nrm_ver,h1,h2,h2_min,h3
logical                      :: compute_qp2,look,solved_in_mp_mh,use_Ps_energy,modified_coulomb
character(len=1)             :: acs,ais,afs,method,behavior
character(len=2)             :: id
character(len=3)             :: form,sens
character(len=4)             :: thres,mode
character(len=20)            :: target,approx,basis
character(len=90)            :: path
logical                      :: PsX_target
dimension dgam(1:2),ngpx(1:2),path(1:4)
end module global_parameters

module ipcms
use global_parameters
use gauss_quadrature
use ieee_arithmetic
use iso_fortran_env, only : error_unit
implicit none
real*8                                         :: phb,phbp,npbar,vpbar,psd,eta,rc,epso_qp2,accur_1,accur_2,gap,zeta
real*8                                         :: eh,pi,v,nrm,epso,cons,cp,ek,bx_ad,bx,sig3b_var,sig4b_var,sig_phH_plus,sig_phH_mines
real*8                                         :: gam_phH_plus,gam_phH_mines,rad,sig3b_1s,sig4b_1s,sig3b_3d,sig4b_3d,sig3b_3d_on,sig4b_3d_on
real*8                                         :: sig3b_5f,sig4b_5f,period,Zp
integer*4                                      :: ngpq,deg,cmax,lmax,xmax,xmax_tab,nexch,sup
integer                                        :: ngrid,nquad
real*8, dimension(:), allocatable              :: ui,vi,wi,xi,yi,zi,ei
real*8, dimension(:), allocatable              :: t,wt,tq,wtq,tu,wtu,ekw,kf,ef,nb,rfs,sidv
real*8, dimension(:), allocatable              :: gx_in,gx_ox
complex*16, dimension(:), allocatable          :: egv,mod_in,mod_ox
real*8, dimension(:,:), allocatable            :: kc,na,tx,wtx,csw,sxm,sigma,hx_in,hx_ox,alpha_PRA86,d_PRA86,wx_in,wx_ox,fi
integer*4, dimension(:,:), allocatable         :: idex,idex_tab
real*8, dimension(:,:,:), allocatable          :: ylmt,w3j_reduce,bso,fx_in,fx_ox
real*8, dimension(:,:,:), allocatable          :: sig_mod
complex*16, dimension(:,:,:), allocatable      :: gfs
real*8, dimension(:,:,:,:), allocatable        :: fpqo,bis,bfs
complex*16, dimension(:,:,:,:), allocatable    :: jcs
real*8, dimension(:,:,:,:,:), allocatable      :: bco,w3j
complex*16, dimension(:,:,:,:,:), allocatable  :: jis
integer                                        :: rank,nproc,ierr,iproc
character(len=1)                               :: field
character(len=2)                               :: state_mod
dimension bx_ad(1:2),bx(1:2),nrm(1:2),epso(1:3),eta(1:2)
dimension deg(1:2),sup(1:9),cmax(1:3),lmax(1:3),rc(1:2),ngrid(1:2),Zp(1:2)
data eh/27.211384523d0/,pi/3.14159265358979d0/
data ngpq/64/,accur_1/1.d-16/,accur_2/1.d-8/
parameter (nexch=21)
dimension sig3b_1s(1:nexch),sig4b_1s(1:nexch),sig3b_3d(1:nexch),sig4b_3d(1:nexch),sig3b_3d_on(1:nexch),sig4b_3d_on(1:nexch)
dimension sig3b_var(1:nexch),sig4b_var(1:nexch),sig3b_5f(1:nexch),sig4b_5f(1:nexch)
dimension gam_phH_plus(1:nexch),gam_phH_mines(1:nexch),sig_phH_plus(1:nexch),sig_phH_mines(1:nexch),phb(1:nexch),rad(1:nexch,1:nexch)
      include 'mpif.h'
      contains

subroutine acces
      implicit real*8 (a-h,o-z)
      character(len=20)  :: str
      path(1)="/home/kevin/Desktop/3b/data/"
      path(2)="/home/kevin/Desktop/gbar/ground_state_cdw/"
      path(3)="/home/kevin/Desktop/mendeleiev/frozen_core_mod/"
      if (compute_qp2.eqv..false.) then
        if (target=="PsCl") str="sorbonne/"
        if (target=="PsH") str="unistra/"
      else if (compute_qp2.eqv..true.) then 
        if (behavior.eq."-") str="qp2_electron/"
        if (behavior.eq."+") str="qp2_positron/"
      end if
      path(1)=trim(path(1))//trim(str)
end subroutine acces
  
subroutine green(state,z,ex,x,r,rr,ox)  
      implicit complex*16 (a-h,o-z)
      character(len=2)  :: state 
      integer*4         :: x,iret
      real*8            :: z,demi_mu,rmin,rmax,r,rr
      logical ver
      intent(in) state,z,ex,x,r,rr
      intent(out) ox
      dimension fc(0:x),gc(0:x),fcp(0:x),gcp(0:x),sig(0:x)
      dimension cx(1:3),ver(1:2)
      data zmin/(0.d0,0.d0)/,ci/(0.d0,1.d0)/
      if (state.eq."fs") then 
        demi_mu=0.5d0
      else if (state.eq."is") then
        demi_mu=0.25d0
      end if  
      beta=z*cdsqrt(-demi_mu/ex)
      if ((dimag(ex).eq.0.d0).and.(dreal(ex).gt.0d0)) beta=-beta
      cx(1)=-2.d0*ci*beta/(z*r*rr)
      betam=-ci*beta
      rmin=dmin1(r,rr); rhom=dcmplx(0.d0,-z*rmin)/beta
      fc(:)=(0.d0,0.d0); fcp(:)=(0.d0,0.d0); gc(:)=(0.d0,0.d0); gcp(:)=(0.d0,0.d0); sig(:)=(0.d0,0.d0)
      call coulcc(rhom,betam,zmin,x+1,fc,gc,fcp,gcp,sig,21,0,iret)
      if (iret.eq.0) then
        cx(2)=fc(x)
      else 
        call asymp(1,17,x,rhom,betam,sig,cx(2))
      end if
      rmax=dmax1(r,rr); rhom=dcmplx(0.d0,-z*rmax)/beta
      fc(:)=(0.d0,0.d0); fcp(:)=(0.d0,0.d0); gc(:)=(0.d0,0.d0); gcp(:)=(0.d0,0.d0); sig(:)=(0.d0,0.d0)
      call coulcc(rhom,betam,zmin,x+1,fc,gc,fcp,gcp,sig,21,0,iret)
      if (iret.eq.0) then
        cx(3)=gc(x)
      else 
        call asymp(2,17,x,rhom,betam,sig,cx(3))
      end if
      ox=product(cx)
      ver(1)=ieee_is_nan(dreal(ox))
      ver(2)=ieee_is_nan(dimag(ox))
      if ((ver(1).eqv..true.).or.(ver(2).eqv..true.)) ox=(0.d0,0.d0)
  end subroutine green
  
subroutine asymp(input,kmax,x,rhom,etam,sig,ox)
      implicit complex*16 (a-h,o-z)
      integer*4  :: input,x,k,kmax
      intent(in) input,kmax,x,rhom,etam,sig
      intent(out) ox
      dimension sig(0:x),f(0:kmax),g(0:kmax),a(0:kmax-1),u(0:kmax-1)
      data ci/(0.d0,1.d0)/
      ph=rhom-etam*cdlog(2.d0*rhom)-0.5d0*x*pi+sig(x)
      f(0)=(1.d0,0.d0)
      g(0)=(0.d0,0.d0)
      do k=0,kmax-1
        a(k)=(dfloat(2*k+1)/dfloat(2*k+2))*(etam/rhom)
        u(k)=(dfloat(x*(x+1)-k*(k+1))+etam**2.d0)
        u(k)=u(k)/(dfloat(2*k+2)*rhom)
        f(k+1)=a(k)*f(k)-u(k)*g(k)
        g(k+1)=a(k)*g(k)+u(k)*f(k)
      end do
      if (input.eq.1) ox=sum(g)*cdcos(ph)+sum(f)*cdsin(ph)
      if (input.eq.2) ox=cdexp(-ci*ph)*(sum(f)-ci*sum(g))
  end subroutine asymp
  
subroutine asymp2(rho,beta,x,sphas)
      implicit real*8 (a-h,o-z)
      integer*4  :: x
      complex*16 :: arg,ph,clgam
      intent(in) rho,beta,x
      intent(out) sphas
      dimension ox(1:4)
      external clgam,fact
      arg=dcmplx(dfloat(x+1),beta)
      ph=cdexp(clgam(arg))       
      ox(1)=cdabs(ph)
      ox(2)=(2.d0*rho)**(dfloat(x))
      ox(3)=dexp(-pi*beta/2.d0)
      ox(4)=1.d0/fact(2*x+1)
      sphas=product(ox)
  end subroutine asymp2

subroutine norme(ds,r)
      implicit real*8 (a-h,o-z)
      intent(in) ds
      intent(out) r
      dimension ds(1:3)
      r=dsqrt(ds(1)**2.d0+ds(2)**2.d0-2.d0*product(ds))
  end subroutine norme

subroutine vpo(lvar,l,ds,wf,ox) 
      implicit real*8 (a-h,o-z)
      double precision :: lagint
      integer*4        :: lvar,l
      complex*16       :: wf,ox,ax
      external plgndr,lagint
      intent(in) lvar,l,ds,wf
      intent(out) ox
      dimension ds(1:4),ax(1:4)
      x=ds(1); y=ds(2); u=ds(3); r=ds(4)
      if (id.eq."pr") var=y
      if (id.eq."po") var=r
      ax(1)=1.d0/x-1.d0/var
      if (mode.eq."chem") ax(1)=ax(1)+lagint(x,xi,zi,ngrid(1),3)
      ax(2)=r**(-dfloat(lvar))
      ax(3)=plgndr(l,0,u)
      ax(4)=wf/2.d0
      ox=product(ax)
      if ((ds(1).eq.0.d0).or.(ds(2).eq.0.d0)) ox=dcmplx(0.d0,0.d0)
  end subroutine vpo

subroutine para_data(state,kx)
      implicit real*8 (a-h,o-z)  
      character(len=2)  :: state
      integer           :: nr
      integer*4         :: l,x,x_mpi,no,m,lpp,k,i,kx
      complex*16        :: u,wf,cu,ox,grox
      intent(in) state,kx
      external radial
      dimension cu(1:ngpu),u(1:ngpt)
      dimension ds(1:4)
      call mpi_comm_size(mpi_comm_world,nproc,ierr) 
      call mpi_comm_rank(mpi_comm_world,rank,ierr)
      no=xmax_tab/nproc; nr=mod(xmax_tab,nproc)
      do iproc=0,nr-1
        if (iproc.eq.rank) no=no+1
      end do
      mod_in(:)=0.d0; mod_ox(:)=0.d0
      do x_mpi=1,no
        x=(x_mpi-1)*nproc+rank+1
        ds(1)=tx(1,idex_tab(1,x))
        ds(2)=tx(kx,idex_tab(2,x))
        l=idex_tab(3,x)
        if (state.eq."cs") then
          cu(:)=dcmplx(0.d0,0.d0)
          do m=1,ngpu
            ds(3)=tu(m)
            call norme(ds,r); ds(4)=r
            wf=dcmplx(radial("is",1.d0,"exp-",np,lp,r),0.d0) 
            call vpo(lp,l,ds,wf,ox)
            cu(m)=ox
          end do
        else if (state.eq."is") then
          lpp=idex_tab(4,x)
          if (mod((1+lp+lpp),2).eq.0) then  
            k=idex_tab(5,x)
            cu(:)=dcmplx(0.d0,0.d0)  
            do m=1,ngpu
              ds(3)=tu(m)
              call norme(ds,r); ds(4)=r
              u(:)=dcmplx(0.d0,0.d0)
              do i=1,ngpt
                uo=radial(state,1.d0,"norm",np,lp,t(i))
                call green(state,1.d0,egv(k),lpp,r,t(i),grox) 
                u(i)=uo*grox
              end do
              wf=dot_product(u,wt)
              call vpo(lpp,l,ds,wf,ox)
              cu(m)=ox
            end do
          end if  
        end if
        mod_in(x)=dot_product(cu,wtu)
      end do
      call mpi_barrier(mpi_comm_world,ierr)
      call mpi_allreduce(mod_in,mod_ox,xmax_tab,mpi_complex16,mpi_sum,mpi_comm_world,ierr) 
  end subroutine para_data 

subroutine data(state,kmax)
      implicit integer*4 (a-z)
      character(len=2)  :: state
      integer*4         :: dimx
      real*8            :: r,d3j30,d3j,ylm,fact,uo,radial
      complex*16        :: out,grox,u
      intent(in) state,kmax
      optional kmax
      dimension dimx(1:2),u(1:ngpt)
      external d3j30,d3j,ylm,fact,radial
      dimx(1)=ngpx(1)*ngpx(2)
      if (state.eq."fs") then 
        gfs(:,:,:)=dcmplx(0.d0,0.d0)
        do i=1,ngpx(2)
          do lhh=iabs(lh-1),lh+1
            if (mod((1+lh+lhh),2).eq.0) then 
              do k=1,kmax
                u(:)=dcmplx(0.d0,0.d0)
                do j=1,ngpt
                  uo=radial(state,1.d0,"norm",nh,lh,t(j))
                  call green(state,1.d0,egv(k),lhh,tx(2,i),t(j),grox) 
                  u(j)=uo*grox
                end do
                out=sum(u(:)*wt(:))
                gfs(i,lhh,k)=dconjg(out)
              end do
            end if
          end do
        end do 
      else if (state.eq."is") then 
        dimx(2)=(sup(5)+1)*(lp+2-iabs(lp-1))*kmax
        xmax_tab=product(dimx)
        allocate(idex_tab(1:5,1:xmax_tab),mod_in(1:xmax_tab),mod_ox(1:xmax_tab))
        idex_tab(:,:)=0; x=1
        do i=1,ngpx(1)
          do j=1,ngpx(2)
            do l=0,sup(5)
              do lpp=iabs(lp-1),lp+1
                do k=1,kmax
                  idex_tab(1,x)=i
                  idex_tab(2,x)=j
                  idex_tab(3,x)=l
                  idex_tab(4,x)=lpp
                  idex_tab(5,x)=k
                  x=x+1
                end do
              end do
            end do  
          end do
        end do               
        call para_data(state,2)
        jis(:,:,:,:,:)=dcmplx(0.d0,0.d0) 
        do x=1,xmax_tab
          i=idex_tab(1,x)
          j=idex_tab(2,x)
          l=idex_tab(3,x)
          lpp=idex_tab(4,x)
          if (mod((1+lp+lpp),2).eq.0) then  
            k=idex_tab(5,x)
            jis(i,j,l,lpp,k)=mod_ox(x)
          end if   
        end do 
        deallocate(idex_tab,mod_in,mod_ox)
      else if (state.eq."cs") then
        dimx(2)=sup(5)+1
        xmax_tab=product(dimx)
        allocate(idex_tab(1:3,1:xmax_tab))
        idex_tab(:,:)=0; x=1
        do i=1,ngpx(1)
          do j=1,ngpx(2)
            do l=0,sup(5)
              idex_tab(1,x)=i
              idex_tab(2,x)=j
              idex_tab(3,x)=l
              x=x+1
            end do  
          end do
        end do      
        jcs(:,:,:,:)=dcmplx(0.d0,0.d0)
        do kx=1,2
          allocate(mod_in(1:xmax_tab),mod_ox(1:xmax_tab))
          call para_data(state,kx)
          do x=1,xmax_tab
            i=idex_tab(1,x)
            j=idex_tab(2,x)
            l=idex_tab(3,x)
            jcs(kx,i,j,l)=mod_ox(x)
          end do 
          deallocate(mod_in,mod_ox)
        end do  
        deallocate(idex_tab)
      else if (state.eq."os") then
        do l=0,sup(1)
          do m=-l,l
            if (iabs(m).le.l) then
              do q=1,ngpq
                ylmt(l,m,q)=ylm(tq(q),l,m)
              end do
            end if
          end do
        end do
        do la=0,cmax(1)
          do lb=0,cmax(2)
        do lc=0,cmax(3)
        if (((iabs(la-lb).le.lc).and.(lc.le.la+lb)).and.&
           &((iabs(la-lc).le.lb).and.(lb.le.la+lc)).and.&
           &((iabs(lb-lc).le.la).and.(la.le.lb+lc)).and.& 
           &(mod((la+lb+lc),2).eq.0)) then
             w3j_reduce(la,lb,lc)=d3j30(la,lb,lc)
           do ma=-la,la
             do mb=-lb,lb
               if (iabs(ma+mb).le.lc) then
                 w3j(la,lb,lc,ma,mb)=d3j(la,lb,lc,ma,mb,-ma-mb)
               end if
             end do
           end do  
        end if    
        end do
          end do
        end do  
        nrm(1)=dsqrt(dfloat(product(deg)))*(4.d0*pi)**(3.d0/2.d0)
        do x=0,lp+1
          nb(x)=dsqrt(fact(2*x+1))
          do l=0,x
            na(x,l)=dsqrt(fact(2*l)*fact(2*(x-l)))
          end do
        end do        
      end if
  end subroutine data

subroutine green_data(state,kmax)
      implicit real*8 (a-h,o-z)
      character(len=2)  :: state
      integer*4         :: kmax,m
      intent(in)  state,kmax
      allocate(egv(1:kmax),t(1:ngpt),wt(1:ngpt)) 
      egv(:)=dcmplx(0.d0,0.d0)
      if (state.eq."fs") m=1
      if (state.eq."is") m=2
      call cgqf(ngpt,5,3.d0,0.d0,0.d0,bx_ad(m),t,wt)
      egv(1)=dcmplx(epso(m)-omegao,dgam(m)/2.d0)
      egv(2)=dcmplx(epso(m)+omegao,dgam(m)/2.d0)
      call data(state,kmax)
      deallocate(egv,t,wt)
  end subroutine green_data
 
subroutine tabular(input,state)
      implicit real*8 (a-h,o-z)
      integer*4         :: input,k,i,kmax
      character(len=3)  :: state
      character(len=4)  :: str
      intent(in) input,state
      external radial
      data kmax/2/
      if (state.eq."com") then 
        if (input.eq.1) then
          bx_ad(1)=1.d+0/dfloat(nh)
          bx_ad(2)=5.d-1/dfloat(np)
          allocate(alpha_PRA86(1:2,-max0(lp,lh):max0(lp,lh)),d_PRA86(1:2,-max0(lp,lh):max0(lp,lh)))
          alpha_PRA86(:,:)=0.d0; d_PRA86(:,:)=0.d0
          state_mod="fs"
          !call H_polarization
          if (compute_qp2.eqv..true.) then
            mode="chem"
          else if (compute_qp2.eqv..false.) then
            allocate(tu(1:ngpu),wtu(1:ngpu))      
            call cgqf(ngpu,1,0.d0,0.d0,-1.d0,1.d0,tu,wtu)  
          end if
          if (method.eq."g") then 
            bx(1)=1.d0
            if (mode.eq."chem") bx(2)=1.d0
            if (mode.eq."gbar") bx(2)=bx_ad(1)
          end if
          allocate(tx(1:2,1:maxval(ngpx)),wtx(1:2,1:maxval(ngpx)))
          tx(:,:)=0.d0; wtx(:,:)=0.d0
          if (method.eq."g") then 
            do k=1,2
              a=2.d0; b=bx(k)
              if ((k.eq.2).and.(mode.eq."chem")) a=0.d0
              allocate(t(1:ngpx(k)),wt(1:ngpx(k)))  
              call cgqf(ngpx(k),5,a,0.d0,0.d0,b,t,wt) 
              do i=1,ngpx(k)
                tx(k,i)=t(i); wtx(k,i)=wt(i)
              end do  
              deallocate(t,wt) 
            end do
          else if (method.eq."r") then 
            call grid_parameters(1); tx(1,:)=tx(2,:); wtx(1,:)=wtx(2,:)
          end if  
          allocate(rfs(1:ngpx(2))); rfs(:)=0.d0
          if (compute_qp2.eqv..true.) then
            call extrapo(1)
            call extrapo(2)
          else if (compute_qp2.eqv..false.) then
            if (mode.eq."gbar") then
              if (method.eq."g") str="norm"
              if (method.eq."r") str="exp-"
              do k=1,ngpx(2)
                rfs(k)=radial("fs",1.d0,str,nh,lh,tx(2,k)) 
              end do 
            else if (mode.eq."chem") then 
              call extrapo(2)
            end if
            if (modified_coulomb.eqv..true.) call extrapo(3)
            allocate(jcs(1:2,1:ngpx(1),1:ngpx(2),0:sup(5)))
            call data("cs")
            allocate(tq(1:ngpq),wtq(1:ngpq))      
            call cgqf(ngpq,1,0.d0,0.d0,-1.d0,1.d0,tq,wtq)  
            allocate(ylmt(0:sup(1),-sup(1):sup(1),1:ngpq))
            allocate(w3j_reduce(0:cmax(1),0:cmax(2),0:cmax(3)))
            allocate(w3j(0:cmax(1),0:cmax(2),0:cmax(3),-cmax(1):cmax(1),-cmax(2):cmax(2)))
            allocate(na(0:lp+1,0:lp+1),nb(0:lp+1))
            call data("os")
          end if
        else if (input.eq.2) then
          deallocate(rfs,tx,wtx)
          if (compute_qp2.eqv..false.) deallocate(tu,wtu,jcs,tq,wtq,ylmt,w3j_reduce,w3j,na,nb)
          deallocate(alpha_PRA86,d_PRA86)
          if ((mode.eq."chem").or.(compute_qp2.eqv..true.)) then
            deallocate(xi,yi,zi)
            if (compute_qp2.eqv..true.) deallocate(ei)
          end if  
          if (modified_coulomb.eqv..true.) then 
            deallocate(ei)
            if ((mode.eq."chem").and.(form.eq."cdw")) deallocate(fi)
          end if  
        end if   
      else if (state.eq."var") then
        if (field.eq."o") then
          if (input.eq.1) then   
            if (afs.eq."o") then
              allocate(gfs(1:ngpx(2),iabs(lh-1):lh+1,1:kmax))
              call green_data("fs",kmax)
            end if  
            if (ais.eq."o") then 
              allocate(jis(1:ngpx(1),1:ngpx(2),0:sup(5),iabs(lp-1):lp+1,1:kmax))
              call green_data("is",kmax)
            end if  
          else if (input.eq.2) then
            if (afs.eq."o") deallocate(gfs)
            if (ais.eq."o") deallocate(jis)
          end if
        end if   
      else if (state.eq."bes") then
        if (input.eq.1) then
          call grid(1)
          allocate(kf(pmin:pmax),kc(pmin:pmax,1:ngpq),ef(pmin:pmax))
          allocate(bso(0:2,pmin:pmax,1:ngpq))
          allocate(fx_in(1:xmax,-lp:lp,-lh:lh),fx_ox(1:xmax,-lp:lp,-lh:lh),idex(1:2,1:xmax))
          allocate(bis(1:maxval(ngpx),1:2,0:sup(8),-lp:lp))
          allocate(bfs(1:ngpx(1),pmin:pmax,0:sup(1),-lh:lh))
          allocate(bco(1:ngpx(1),pmin:pmax,1:ngpq,0:2,0:sup(9)))     
          if (modified_coulomb.eqv..true.) allocate(sig_mod(1:3,0:max0(sup(8),sup(1)),-max0(lp,lh):max0(lp,lh)))
          call grid(2)
        else if (input.eq.2) then
          deallocate(kf,kc,ef)
          deallocate(bso)
          deallocate(bis,bfs,bco)
          if (modified_coulomb.eqv..true.) deallocate(sig_mod)
          deallocate(fx_in,fx_ox,idex) 
        end if  
      end if  
  end subroutine tabular

subroutine data_add
      implicit real*8 (a-h,o-z)
      integer*4                                    :: k,kmax,i,idex_i
      real*8                                       :: idex_ekw
      logical                                      :: cond
      character(len=20)                            :: entr,output
      integer*4, dimension(:), allocatable         :: nmax
      character(len=2), dimension(:), allocatable  :: c
      if (mode.eq."gbar") then 
        kmax=nh
      else if (mode.eq."chem") then 
        if (PsX_target.eqv..true.) then
          kmax=np
        else 
          if (target=="Ne") kmax=3
          if (target=="Ar") kmax=4
          if (target=="Kr") kmax=7
          if (target=="Xe") kmax=8
        end if
      end if
      allocate(nmax(kmax),c(kmax))
      if (mode.eq."gbar") then      
        if (kmax.eq.1) then
          c(1)="1s"
        else if (kmax.eq.2) then
          c(1)="2s"; c(2)="2p"
        else if (kmax.eq.3) then 
          c(1)="3s"; c(2)="3p"; c(3)="3d"
        else if (kmax.eq.4) then 
          c(1)="4s"; c(2)="4p"; c(3)="4d"; c(4)="4f"
        else if (kmax.ge.21) then
          c(1)="1s"
          c(2)="2s"; c(3)="2p"
          c(4)="3s"; c(5)="3p"; c(6)="3d"
          c(7)="4s"; c(8)="4p"; c(9)="4d"; c(10)="4f"
          c(11)="5s"; c(12)="5p"; c(13)="5d"; c(14)="5f"; c(15)="5g"
          c(16)="6s"; c(17)="6p"; c(18)="6d"; c(19)="6f"; c(20)="6g"; c(21)="6h"
          if (kmax.eq.28) then
            c(22)="7s"; c(23)="7p"; c(24)="7d"; c(25)="7f"; c(26)="7g"; c(27)="7h"; c(28)="7i"
          end if
        end if  
      else if (mode.eq."chem") then 
        if (PsX_target.eqv..true.) then
          if (kmax.eq.1) then
            c(1)="1s"
          else if (kmax.eq.2) then
            c(1)="2s"; c(2)="2p"
          else if (kmax.eq.3) then  
            c(1)="3s"; c(2)="3p"; c(3)="3d"
          else if (kmax.eq.4) then
            c(1)="4s"; c(2)="4p"; c(3)="4d"; c(4)="4f"
          else if (kmax.eq.5) then
            c(1)="5s"; c(2)="5p"; c(3)="5d"; c(4)="5f"; c(5)="5g"
          end if
        else
          if (target=="Ne") then
            c(1)="1s"; c(2)="2s"; c(3)="2p"
          else if (target=="Ar") then
            c(1)="2s"; c(2)="2p"; c(3)="3s"; c(4)="3p"
          else if (target=="Kr") then 
            c(1)="2s"; c(2)="2p"; c(3)="3s"; c(4)="3p"
            c(5)="3d"; c(6)="4s"; c(7)="4p"
          else if (target=="Xe") then
            c(1)="3s"; c(2)="3p"; c(3)="3d"; c(4)="4s"
            c(5)="4p"; c(6)="4d"; c(7)="5s"; c(8)="5p"
          end if
        end if
      end if
      do k=1,kmax 
        entr=trim(c(k))
        nmax(k)=0
        if ((mode.eq."chem").and.(PsX_target.eqv..false.)) entr=trim(target)//"-"//entr
        open(unit=1,file=trim(path(2))//trim(entr),form="formatted",iostat=ierr)
        do while (ierr==0)
          read(1,*,iostat=ierr)
          if (ierr==0) nmax(k)=nmax(k)+1
        end do
        close(1)
      end do 
      allocate(ekw(maxval(nmax)),csw(kmax,maxval(nmax)))
      entr=trim(c(maxloc(nmax,1)))
      if ((mode.eq."chem").and.(PsX_target.eqv..false.)) entr=trim(target)//"-"//entr
      open(unit=2,file=trim(path(2))//trim(entr),form="formatted",iostat=ierr)
      do i=1,maxval(nmax)
        read(2,*) ekw(i),csw(maxloc(nmax,1),i)
      end do 
      close(2)
      if (mode.eq."gbar") then
        open(unit=3,file="para",form="formatted")
        do k=1,20
          read(3,*)
        end do
        read(3,*) h1
        close(3)
      end if
      do k=1,kmax
        if (k.eq.maxloc(nmax,1)) cycle
        entr=trim(c(k))
        if ((mode.eq."chem").and.(PsX_target.eqv..false.)) entr=trim(target)//"-"//entr
        open(unit=4,file=trim(path(2))//trim(entr),form="formatted",iostat=ierr)
        read(4,*) threshold_k
        close(4)
        idex_i=1
        idex_ekw=ekw(1)
        cond=.true.
        do while (cond.eqv..true.)
          if (idex_ekw.lt.threshold_k) then
            csw(k,idex_i)=0.d0
            idex_i=idex_i+1
            if (mode.eq."chem") call log_scale(idex_ekw,4)
            idex_ekw=idex_ekw+h1
          else 
            open(unit=5,file=trim(path(2))//trim(entr),form="formatted",iostat=ierr)
            do i=idex_i,maxval(nmax)
              read(5,*) ekw(i),csw(k,i)
            end do
            close(5)
            cond=.false.
          end if
        end do
      end do
      if (kmax.lt.10) write(output,'(I1)') kmax
      if (kmax.ge.10) write(output,'(I2)') kmax
      if ((mode.eq."chem").and.(PsX_target.eqv..false.)) output=trim(target)//"-"//output
      open(unit=6,file=trim(path(2))//"sum-"//trim(output),form="formatted",iostat=ierr)
      do i=1,maxval(nmax)
        write(6,101) ekw(i),sum(csw(:,i))
      end do
      close(6)
      deallocate(ekw,csw,nmax,c)
      101 format (F13.4,E25.12)
end subroutine data_add
  
subroutine extrapo(input)
      implicit real*8 (a-h,o-z)
      real*8, dimension(:,:), allocatable  :: wf,vp
      double precision                     :: lagint
      integer*4                            :: k,i,input,kmax
      integer*4                            :: n_starter,n,l,count_wf
      character(len=50)                    :: entr,nh_str,lh_str
      intent(in) input
      external lagint,rint,radial
      dimension entr(3),tab(1:ngpx(2))
      call mpi_comm_size(mpi_comm_world,nproc,ierr)
      call mpi_comm_rank(mpi_comm_world,rank,ierr)
      if (input.eq.1) then 
        occ=2*(2*lh+1) 
        entr(1)=trim("eg")//trim(target)
        entr(2)=trim("wf")//trim(target)
        entr(3)=trim("vp")//trim(target)       
        if (compute_qp2.eqv..false.) then
          if (PsX_target.eqv..true.) then 
            occ=1 
            do k=1,3
              entr(k)=trim(entr(k))//trim("_")//trim(approx)//trim("_")//trim(basis)
            end do
          end if  
        else if (compute_qp2.eqv..true.) then
          do k=1,3
            entr(k)=trim(entr(k))//trim("_")//trim(approx)//trim("_")//trim(basis)
          end do
          if (behavior.eq."-") then
            write(nh_str,'(i1)') nh  
            write(lh_str,'(i1)') lh    
            entr(3)=trim(entr(3))//trim("_")//trim(nh_str)//trim(lh_str)
          else if (behavior.eq."+") then
            occ=1
          end if
        end if
        epso(1)=0.d0
        n_starter=1
        kmax=0
        open(unit=1,file=trim(path(1))//trim(entr(1)),form="formatted",iostat=ierr)
        do while (ierr==0)
          read(1,*,iostat=ierr)
          if (ierr==0) kmax=kmax+1
        end do
        close(1)
        open(unit=2,file=trim(path(1))//trim(entr(1)),form="formatted",iostat=ierr)
        n=n_starter; l=0
        do k=1,kmax
          read(2,*) eigenvalues
          if ((nh.eq.n).and.(lh.eq.l)) then
            epso(1)=eigenvalues
            count_wf=k
          end if  
          if (l.eq.(n-1)) then
            n=n+1; l=0
          else 
            if (target=="Xe") then
              if ((n.eq.4).and.(l.eq.2)) then
                n=5; l=0
              else 
                l=l+1
              end if  
            else
              l=l+1
            end if
          end if  
        end do
        close(2)
        if (epso(1).eq.0.d0) then
          if (rank.eq.0) write(error_unit,'(A)') "error: no binding energy for this orbital"
          stop
        end if
        ngrid(1)=0
        open(unit=3,file=trim(path(1))//trim(entr(2)),form="formatted",iostat=ierr)
        do while (ierr==0)
          read(3,*,iostat=ierr)
          if (ierr==0) ngrid(1)=ngrid(1)+1
        end do
        close(3)
        kmax=kmax+1
        count_wf=count_wf+1
        allocate(xi(1:ngrid(1)),yi(1:ngrid(1)),zi(1:ngrid(1)))
        if (compute_qp2.eqv..true.) then 
          ngrid(2)=ngrid(1); allocate(ei(1:ngrid(2)))
        end if
        allocate(wf(1:kmax,1:ngrid(1)),vp(1:2,1:ngrid(1)))
        open(unit=4,file=trim(path(1))//trim(entr(2)),form="formatted",iostat=ierr)
        open(unit=5,file=trim(path(1))//trim(entr(3)),form="formatted",iostat=ierr)
        do i=1,ngrid(1)
          read(4,*) (wf(k,i),k=1,kmax)
          read(5,*) (vp(k,i),k=1,2)
        end do           
        close(4); close(5)
        xi(:)=wf(1,:); if (compute_qp2.eqv..true.) ei(:)=xi(:)
        yi(:)=wf(count_wf,:)
        zi(:)=vp(2,:)
        deallocate(wf,vp)
      else if (input.eq.2) then
        do k=1,ngpx(2)
          x=tx(2,k); rfs(k)=lagint(x,xi,yi,ngrid(1),3)
        end do  
        if (method.eq."g") then   
          tab(:)=dexp(tx(2,:))*rfs(:)**2.d0; nrm_ver=dot_product(tab,wtx(2,:))
        else if (method.eq."r") then
          tab(:)=wtx(2,:)*rfs(:)**2.d0; nrm_ver=rint(tab,2,ngpx(2),7,h2)             
        end if
        if ((rank.eq.0).and.(look.eqv..true.).and.(compute_qp2.eqv..true.)) then
          if (method.eq."g") write(11,'(A)') "GAUSS"
          if (method.eq."r") write(11,'(A)') "RINT"
          write(11,'(A)') "--------------------" 
          write(11,'(A, F13.6)') "nrm_ver",nrm_ver
        end if
        if (compute_qp2.eqv..true.) then
          call atomic_subshell_ionization
        end if
      else if (input.eq.3) then
        if ((mode.eq."gbar").or.((mode.eq."chem").and.(form.eq."cba"))) then 
          call stromgren_parameters
          ngrid(2)=max0(idint(maxval(tx)),idint(rc(2)))+1
          ngrid(2)=ngrid(2)*2d+3
        else if ((mode.eq."chem").and.(form.eq."cdw")) then 
          entr(3)=trim("vq")//trim(target)//trim("_")//trim(approx)//trim("_")//trim(basis)
          if (PsX_target.eqv..false.) then 
            write(nh_str,'(i1)') nh  
            write(lh_str,'(i1)') lh 
            entr(3)=trim(entr(3))//trim("_")//trim(nh_str)//trim(lh_str)
          end if 
          ngrid(2)=0
          open(unit=1,file=trim(path(1))//trim(entr(3)),form="formatted",iostat=ierr)
          do while (ierr==0)
            read(1,*,iostat=ierr)
            if (ierr==0) ngrid(2)=ngrid(2)+1
          end do
          close(1)
        end if
        allocate(ei(1:ngrid(2)))
        if ((mode.eq."gbar").or.((mode.eq."chem").and.(form.eq."cba"))) then 
          ei(1)=1.d-3
          do k=1,ngrid(2)-1
            ei(k+1)=ei(k)+1.d-3
          end do
        else if ((mode.eq."chem").and.(form.eq."cdw")) then 
          allocate(fi(1:2,1:ngrid(2)))
          open(unit=2,file=trim(path(1))//trim(entr(3)),form="formatted",iostat=ierr)
          do i=1,ngrid(2)
            read(2,*) ei(i),fi(1,i),fi(2,i)
          end do  
          close(2)
        end if  
      end if 
  end subroutine extrapo

subroutine grid_parameters(input)
      implicit none
      integer*4  :: input,k
      intent(in) input
      if (input.eq.0) then
        if (method.eq."r") ngpx(1)=500
        ngpx(2)=ngpx(1)
      else if (input.eq.1) then
        h2=0.022d0
        h2_min=0.005d0
        do k=1,ngpx(2)
          wtx(2,k)=dexp(h2*dfloat(k-1))
          tx(2,k)=h2_min*(wtx(2,k)-1.d0)
          wtx(2,k)=h2_min*wtx(2,k)
        end do
      end if
  end subroutine grid_parameters

subroutine grid(input)
      implicit none
      integer*4                          :: input,p,q,k,x,pver
      real*8                             :: rm,bessj
      real*8, dimension(:), allocatable  :: a
      external  bessj
      logical ver
      intent(in) input
      if (input.eq.0) then
        epso(2)=-1.d0/(4.d0*np**2.d0)
        if (sens.eq."dir") then 
          if ((mode.eq."chem").and.(PsX_target.eqv..true.)) use_Ps_energy=.true.
          if (use_Ps_energy.eqv..false.) then 
            if (mode.eq."gbar") rm=1.d+3/918.076434730d0   
            if (mode.eq."chem") then
              if (target=="Ne") rm=1.d+3/18392.684232d0
              if (target=="Ar") rm=1.d+3/36410.400041d0
              if (target=="Kr") rm=1.d+3/76377.258002d0
              if (target=="Xe") rm=1.d+3/119666.33255d0
            end if
          else
            rm=1.d-3
          end if
        else if (sens.eq."inv") then
          rm=1.d0
        end if
        cp=rm/eh
        if (mode.eq."gbar") then 
          epso(1)=-1.d0/(2.d0*nh**2.d0) 
        else if (mode.eq."chem") then 
          if (compute_qp2.eqv..false.) call extrapo(1)
        end if  
        deg(1)=2*lp+1
        deg(2)=2*lh+1
        sup(1)=lmax(2)
        sup(2)=sup(1)
        sup(3)=lp+lh+2
        sup(4)=lmax(3)
        sup(5)=sup(3)+sup(4)
        sup(6)=lmax(1)
        sup(7)=sup(2)+sup(6)
        sup(8)=max0(sup(4),sup(6))
        sup(9)=0; if (field.eq."o") sup(9)=sup(5)+sup(7)
        cmax(1)=max0(sup(1),sup(4),sup(6),sup(9))
        cmax(2)=max0(sup(2),sup(5))
        cmax(3)=cmax(1)+cmax(2) 
      else if (input.eq.1) then
        if (sens.eq."dir") then 
          v=dsqrt(cp*ek)
        else if (sens.eq."inv") then
          v=dsqrt(epso(1)-epso(2)+ek/eh)
        end if
        nrm(2)=dfloat(deg(1))*(pi**2.d0)*(2.d0*v)
        cons=v**2.d0+epso(2)-epso(1)-epso(3)
        if (field.eq."n") then
          pmax=0; pmin=-pmax
        else if (field.eq."o") then
          pver=int(cons/omegao)
          open(unit=1,file="para",form="formatted")
          do k=1,12
            read(1,*) 
          end do
          read(1,*) pmax
          close(1)
          if (pmax.gt.pver) pmax=pver
        end if
        xmax=(pmax-pmin+1)*ngpq
      else if (input.eq.2) then
        idex(:,:)=0; x=1
        do p=pmin,pmax
          do q=1,ngpq
            idex(1,x)=p
            idex(2,x)=q
            x=x+1
          end do
        end do  
        ef(:)=0.d0; kf(:)=0.d0; kc(:,:)=0.d0; bso(:,:,:)=0.d0
        allocate(a(pmin:pmax))      
        a(:)=0.d0
        do p=pmin,pmax
          ef(p)=cons-dfloat(p)*omegao
          kf(p)=dsqrt(2.d0*ef(p))
          if (field.eq."o") a(p)=fo*kf(p)/omegao**2.d0
          do q=1,ngpq 
            kc(p,q)=omegao/(kf(p)*tq(q))
            do k=0,2
              x=p+k-1 
              bso(k,p,q)=bessj(iabs(x),dabs(a(p)*tq(q))) 
              ver=(x.lt.0).and.(mod(iabs(x),2).eq.1)
              if (ver.eqv..true.) bso(k,p,q)=-bso(k,p,q)
            end do
          end do  
        end do
        deallocate(a)
        call grid_for_coulomb_function
      end if
  end subroutine grid 
  
subroutine coulomb_function(rho,beta,x,fc) 
      implicit real*8 (a-h,o-z)
      integer*4  :: x,iret
      dimension fc(0:x),gc(0:x),fcp(0:x),gcp(0:x)
      intent(in) rho,beta,x
      intent(out) fc
      fc(:)=0.d0; fcp(:)=0.d0; gc(:)=0.d0; gcp(:)=0.d0
      call coulfg(rho,beta,0,x+1,fc,fcp,gc,gcp,accur_1,iret)
  end subroutine coulomb_function

subroutine grid_for_coulomb_function
      implicit real*8 (a-h,o-z)
      integer*4        :: r,p,q,k,x,kx,l,m,tab
      dimension sphfs(0:sup(1)),sphco(0:sup(9)),sphis(0:sup(8))
      logical ver
      dimension tab(1:2),ver(1:2)
      bis(:,:,:,:)=0.d0; bfs(:,:,:,:)=0.d0; bco(:,:,:,:,:)=0.d0 
      if (form.eq."cba") eta(1)=0.d0
      if (form.eq."cdw") eta(1)=1.d0/v
      eta(2)=-eta(1)
      ver(1)=((mode.eq."gbar").and.(modified_coulomb.eqv..false.)).or.(mode.eq."chem") 
      do r=1,maxval(ngpx)
        if (r.le.ngpx(1)) then 
          do p=pmin,pmax
            if (ver(1).eqv..true.) then
              rho=kf(p)*tx(1,r)
              call coulomb_function(rho,0.d0,sup(1),sphfs)
              do m=-lh,lh
                bfs(r,p,:,m)=sphfs(:)/rho
              end do
            end if
            do q=1,ngpq 
              do k=0,2
                x=p+k-1
                rho=x*kc(p,q)*tx(1,r)
                if (dabs(rho).eq.0.d0) then 
                  bco(r,p,q,k,0)=1.d0
                else 
                  call coulomb_function(dabs(rho),0.d0,sup(9),sphco)
                  do l=0,sup(9)
                    bco(r,p,q,k,l)=sphco(l)/dabs(rho) 
                    ver(2)=(rho.lt.0.d0).and.(mod(l,2).eq.1) 
                    if (ver(2).eqv..true.) bco(r,p,q,k,l)=-bco(r,p,q,k,l)
                  end do
                end if
              end do
            end do
          end do
        end if
        if (modified_coulomb.eqv..false.) then
          do kx=1,2
            if (((kx.eq.1).and.(r.le.ngpx(1))).or.(kx.eq.2)) then 
              rho=v*tx(kx,r); call coulomb_function(rho,eta(kx),sup(8),sphis)
              do l=0,sup(8)
                if (ieee_is_nan(sphis(l)).eqv..false.) then
                  bis(r,kx,l,:)=sphis(l)/rho  
                else 
                  call asymp2(rho,eta(kx),l,sphas); bis(r,kx,l,:)=sphas
                end if
              end do
            end if
          end do  
        end if
      end do
      if (modified_coulomb.eqv..true.) then
        sig_mod(:,:,:)=0.d0
        state_mod="is"; tab(1)=sup(8)
        if (mode.eq."chem") tab(2)=0
        if (mode.eq."gbar") tab(2)=lp
        call modified_coulomb_function(1,tab)
        call modified_coulomb_function(2,tab)
        if (mode.eq."gbar") then 
          state_mod="fs"; tab(1)=sup(1); tab(2)=lh
          call modified_coulomb_function(1,tab)
        end if
      end if
  end subroutine grid_for_coulomb_function   
  
subroutine angular_coefficients(state,ds,dp,ox)
      implicit integer*4 (a-z)
      character(len=2)                   :: state
      complex*16                         :: u,ox
      real*8, dimension(:), allocatable  :: ax
      intent(in) state,ds,dp
      intent(out) ox
      dimension ds(1:11),dp(1:2)
      kmax=2; if (state.ne."cs") kmax=5
      allocate(ax(0:kmax))
      call field_free_angular_coefficients(ds,dp,ax(0))
      mp=ds(3);mh=ds(4)
      u=dcmplx(0.d0,fo/2.d0)
      if (state.eq."cs") then
        ax(1)=(-1.d0)**(dfloat(lp))
        ax(2)=nb(lp)
        ox=product(ax)
      else   
        if (state.eq."fs") then
          lhh=dp(2); lhh_hat=2*lhh+1
          ax(1)=(-1.d0)**(dfloat(lp+mh))
          ax(2)=nb(lp)
          ax(3)=dfloat(lhh_hat)
          ax(4)=w3j_reduce(1,lh,lhh)
          ax(5)=w3j(1,lh,lhh,0,mh)
        else if (state.eq."is") then
          lpp=dp(1); lpp_hat=2*lpp+1
          ax(1)=(-1.d0)**(dfloat(lpp+mp))
          ax(2)=nb(lpp)
          ax(3)=dfloat(lpp_hat)
          ax(4)=w3j_reduce(1,lp,lpp)
          ax(5)=w3j(1,lp,lpp,0,mp)   
        end if
        ox=u*product(ax)
      end if  
      deallocate(ax)
  end subroutine angular_coefficients

subroutine field_free_angular_coefficients(ds,dp,ox)
      implicit real*8 (a-h,o-z)
      integer*4                          :: ds,dp,x,y,lf,lc,lambda,l1,l2,l3,l4,l5,l6
      integer*4                          :: mu,mp,mh,hat
      real*8, dimension(:), allocatable  :: ax
      intent(in) ds,dp
      intent(out) ox
      dimension ds(1:11),dp(1:2)
      dimension cx(1:3),hat(1:7)
      x=dp(1); y=dp(2)
      lf=ds(1);lc=ds(2);mp=ds(3);mh=ds(4)
      lambda=ds(5);l1=ds(6);l2=ds(7)
      l3=ds(8);l4=ds(9);l5=ds(10);l6=ds(11)
      hat(1)=2*l1+1
      hat(2)=2*l2+1
      hat(3)=2*l3+1 
      hat(4)=2*l4+1 
      hat(5)=2*l5+1
      hat(6)=2*l6+1
      hat(7)=2*lc+1
      cx(:)=0.d0
      allocate(ax(1:5))      
      ax(1)=w3j_reduce(lf,x-lambda,l4)
      ax(2)=w3j_reduce(lc,l1,l6)
      ax(3)=w3j_reduce(l2,l4,l6)
      ax(4)=w3j_reduce(y,lambda,l5)
      ax(5)=w3j_reduce(l3,l1,l5)
      cx(1)=product(ax) 
      deallocate(ax)
      do mu=-lambda,lambda
        allocate(ax(1:6))
        ax(1)=w3j(x-lambda,lambda,x,mp-mu,mu)
        ax(2)=w3j(lf,x-lambda,l4,mh-mp,mp-mu)
        ax(3)=w3j(lc,l1,l6,0,mu-mh)
        ax(4)=w3j(l2,l4,l6,0,mh-mu)
        ax(5)=w3j(y,lambda,l5,-mh,mu)
        ax(6)=w3j(l3,l1,l5,0,mh-mu)
        cx(2)=cx(2)+product(ax)
        deallocate(ax)
      end do
      allocate(ax(1:3))
      ax(1)=(-1.d0)**(dfloat(lambda))
      ax(2)=dfloat(product(hat))
      ax(3)=dsqrt(dfloat(2*lf+1))
      cx(3)=product(ax)/na(x,lambda)
      deallocate(ax)
      ox=product(cx)
  end subroutine field_free_angular_coefficients   

subroutine radial_coefficients(state,ds,dp,dq,ox)
      implicit real*8 (a-h,o-z)
      character(len=2)                       :: state
      integer*4                              :: lf,lc,lambda,l1,l2,l3,mp,mh
      integer*4                              :: x,y,p,q,ds,dp,dq,s,kx,expo
      real*8                                 :: ao,ap,am
      complex*16                             :: ox,irs,isx,ab,em      
      real*8, dimension(:), allocatable      :: ip,im,io,ur,us
      complex*16, dimension(:), allocatable  :: ir,is
      external rint
      intent(in) state,ds,dp,dq
      intent(out) ox
      dimension ds(1:11),dp(1:2),dq(1:2)
      dimension irs(1:ngpx(1)),em(1:ngpx(1)),ab(1:ngpx(1))
      x=dp(1); y=dp(2)
      p=dq(1); q=dq(2)
      lf=ds(1);lc=ds(2);lambda=ds(5)
      l1=ds(6);l2=ds(7);l3=ds(8);mp=ds(3);mh=ds(4)
      allocate(ur(1:ngpx(1)),us(1:ngpx(2)))
      ur(:)=0.d0; us(:)=0.d0
      ur(:)=bis(:,1,l2,mp)*bfs(:,p,lf,mh)
      ur(:)=ur(:)*tx(1,:)**(dfloat(x-lambda))
      if (method.eq."g") ur(:)=ur(:)*dexp(tx(1,:))
      if (method.eq."r") ur(:)=ur(:)*wtx(1,:)*tx(1,:)**2.d0
      if (state.eq."fs") then
        kx=1; us(:)=dexp(tx(1,:))
      else 
        kx=2; us(:)=rfs(:)   
        if (method.eq."r") then
          if (mode.eq."gbar") expo=2
          if (mode.eq."chem") expo=1
          us(:)=us(:)*wtx(2,:)*tx(2,:)**dfloat(expo)
        else if (method.eq."g") then
          if (mode.eq."chem") us(:)=us(:)*tx(2,:)*dexp(tx(2,:))
        end if
      end if  
      us(:)=us(:)*bis(:,kx,l3,mp)
      us(:)=us(:)*tx(kx,:)**(dfloat(lambda))
      if (state.eq."cs") then 
        ao=bso(1,p,q)
        allocate(io(1:ngpx(1)))
        io(:)=bco(:,p,q,1,lc)*ao 
      else 
        ap=bso(2,p,q); am=bso(0,p,q)  
        allocate(ip(1:ngpx(1)),im(1:ngpx(1)))
        ip(:)=bco(:,p,q,2,lc)*ap
        im(:)=bco(:,p,q,0,lc)*am
      end if
      allocate(ir(1:ngpx(1)),is(1:ngpx(2))) 
      ir(:)=dcmplx(0.d0,0.d0); is(:)=dcmplx(0.d0,0.d0); irs(:)=dcmplx(0.d0,0.d0)
      do s=1,ngpx(2)
        if (state.eq."cs") then
          irs(:)=-jcs(kx,:,s,l1)*io(:)
        else if (state.eq."is") then
          em(:)=im(:)*jis(:,s,l1,x,1)
          ab(:)=ip(:)*jis(:,s,l1,x,2)
          irs(:)=em(:)-ab(:)
        else if (state.eq."fs") then
          em(:)=ip(:)*gfs(s,y,1)
          ab(:)=im(:)*gfs(s,y,2)
          irs(:)=jcs(kx,:,s,l1)*(em(:)-ab(:))
        end if
        ir(:)=ur(:)*irs(:)
        if (method.eq."g") isx=dot_product(ir,wtx(1,:))
        if (method.eq."r") isx=rint(dreal(ir),2,ngpx(1),7,h2)
        is(s)=isx*us(s)
      end do
      if (method.eq."g") ox=dot_product(is,wtx(kx,:))
      if (method.eq."r") ox=rint(dreal(is),2,ngpx(2),7,h2)
      if (state.eq."cs") then
        deallocate(io)
      else
        deallocate(ip,im)
      end if
      deallocate(ir,is,ur,us)  
end subroutine radial_coefficients 

subroutine transition_amplitude_tools(state,dm,dp,dq,ox) 
      implicit integer*4 (a-z)
      character(len=2)  :: state
      real*8            :: sig
      integer*4         :: lpp,lhh,lf,lc,lambda,l1,l2,l3,l4,l5,l6,mp,mh,ds,x,y,dm,dp,dq
      integer*4         :: inf_loop,sup_loop
      complex*16        :: ox,cx
      intent(in) state,dm,dp,dq
      intent(out) ox
      dimension dm(1:3),ds(1:11),sig(1:3)
      dimension dp(1:2),dq(1:2),cx(1:4)
      x=dp(1); y=dp(2)
      lf=dm(1); mp=dm(2); mh=dm(3)
      ds(1)=lf; ds(3)=mp; ds(4)=mh
      ox=dcmplx(0.d0,0.d0); sig(:)=0.d0
      do lambda=0,x
      do l4=iabs(lf-x+lambda),lf+x-lambda
        if (mod((lf+x-lambda+l4),2).eq.0) then           
      do l5=iabs(y-lambda),y+lambda
        if (mod((y+lambda+l5),2).eq.0) then
      do l3=0,lmax(3)
      do l1=iabs(l3-l5),l3+l5
        if (mod((l3+l5+l1),2).eq.0) then           
      do l2=0,lmax(1)
      do l6=iabs(l2-l4),l2+l4
        if (mod((l2+l4+l6),2).eq.0) then           
      inf_loop=0; if (field.eq."o") inf_loop=iabs(l1-l6) 
      sup_loop=0; if (field.eq."o") sup_loop=l1+l6
      do lc=inf_loop,sup_loop
        if (mod((l1+l6+lc),2).eq.0) then           
          ds(2)=lc;ds(5)=lambda;ds(6)=l1;ds(7)=l2
          ds(8)=l3;ds(9)=l4;ds(10)=l5;ds(11)=l6
          cx(:)=dcmplx(0.d0,0.d0)
          call angular_coefficients(state,ds,dp,cx(1))
          if (cx(1).ne.dcmplx(0.d0,0.d0)) then 
            call radial_coefficients(state,ds,dp,dq,cx(2))
            call coulomb_phase(l2,eta(1),sig(1))
            call coulomb_phase(l3,eta(2),sig(2))
            if (modified_coulomb.eqv..true.) then 
              sig(1)=sig(1)+sig_mod(1,l2,mp)
              sig(2)=sig(2)+sig_mod(2,l3,mp)
              sig(3)=sig_mod(3,lf,mh)
            end if
            cx(3)=cdexp(dcmplx(0.d0,sum(sig)))
          end if  
          cx(4)=dcmplx(0.d0,1.d0)**dfloat(l2+l3-lf-lc)
          ox=ox+product(cx)    
        end if  
      end do
        end if
      end do
      end do
        end if
      end do
      end do
        end if
      end do
        end if
      end do
      end do
  end subroutine transition_amplitude_tools  
  
subroutine coulomb_phase(x,beta,ox)
      implicit complex*16 (a-h,o-z)
      integer*4  :: x
      real*8     :: beta,ox
      intent(in) x,beta
      intent(out) ox
      external clgam
      arg=dcmplx(dfloat(x+1),beta)
      ph=cdexp(clgam(arg))
      ox=datan2(dimag(ph),dreal(ph))
  end subroutine coulomb_phase

subroutine transition_amplitude(dn,dq,ox)
      implicit integer*4 (a-z)
      real*8      :: ox
      complex*16  :: s,sx,res,dx
      intent(in) dn,dq
      intent(out) ox
      dimension dn(1:2),dq(1:2),dm(1:3)
      dimension dp(1:2),s(1:3),dx(1:2)
      sx=dcmplx(0.d0,0.d0)
      mp=dn(1); mh=dn(2)
      p=dq(1); q=dq(2)
      dm(2)=mp; dm(3)=mh
      do lf=0,sup(1)
        if (iabs(mp-mh).le.lf) then
          dm(1)=lf
          s(:)=dcmplx(0.d0,0.d0)
          if (acs.eq."o") then
            dp(1)=lp; dp(2)=lh
            call transition_amplitude_tools("cs",dm,dp,dq,res)
            s(1)=res
          end if  
          if (field.eq."o") then 
            if (ais.eq."o") then
              do lpp=iabs(lp-1),lp+1
                if (mod((1+lp+lpp),2).eq.0) then
                  dp(1)=lpp; dp(2)=lh
                  call transition_amplitude_tools("is",dm,dp,dq,res)   
                  s(2)=s(2)+res
                end if
              end do
            end if
            if (afs.eq."o") then
              do lhh=iabs(lh-1),lh+1
                if (mod((1+lh+lhh),2).eq.0) then
                  dp(1)=lp; dp(2)=lhh
                  call transition_amplitude_tools("fs",dm,dp,dq,res)
                  s(3)=s(3)+res
                end if  
              end do   
            end if  
          end if  
          dx(1)=sum(s)
          dx(2)=ylmt(lf,mp-mh,q)
          sx=sx+product(dx)
        end if
      end do
      sx=nrm(1)*sx
      ox=sx*dconjg(sx)
  end subroutine transition_amplitude
  
subroutine sigma_mpi(x)
      implicit real*8 (a-h,o-z)
      integer*4  :: x,mp,mh,dn,dq
      logical ver
      intent(in) x
      dimension dn(1:2),dq(1:2),ver(1:2)
      dq(1)=idex(1,x)
      dq(2)=idex(2,x)
      allocate(sxm(-lp:lp,-lh:lh))
      sxm(:,:)=0.d0
      do mh=-lh,lh
        do mp=-lp,lp
          ver(1)=(mh.le.0).and.(mp.le.0)
          ver(2)=(mh.lt.0).and.(mp.gt.0)
          if ((ver(1).eqv..true.).or.(ver(2).eqv..true.)) then
            dn(1)=mp; dn(2)=mh
            call transition_amplitude(dn,dq,sxm(mp,mh))
          else 
            sxm(mp,mh)=sxm(-mp,-mh)
          end if  
        end do
      end do
      do mh=-lh,lh
        do mp=-lp,lp
          fx_in(x,mp,mh)=sxm(mp,mh)
        end do
      end do  
      deallocate(sxm)
  end subroutine sigma_mpi 

subroutine para_mpi
      implicit integer*4 (a-z)
      integer   :: nr
      integer*4 :: mp,mh
      call mpi_comm_size(mpi_comm_world,nproc,ierr) 
      call mpi_comm_rank(mpi_comm_world,rank,ierr)
      no=xmax/nproc; nr=mod(xmax,nproc)
      do iproc=0,nr-1
        if (iproc.eq.rank) no=no+1
      end do
      fx_in(:,:,:)=0.d0; fx_ox(:,:,:)=0.d0
      do x_mpi=1,no
        x=(x_mpi-1)*nproc+rank+1
        call sigma_mpi(x)
      end do
      call mpi_barrier(mpi_comm_world,ierr)
      do mh=-lh,lh
        do mp=-lp,lp
          call mpi_reduce(fx_in(:,mp,mh),fx_ox(:,mp,mh),xmax,mpi_real8,mpi_sum,0,mpi_comm_world,ierr) 
        end do
      end do  
  end subroutine para_mpi  
  
subroutine read_and_write(i,input,tpi)
      implicit real*8 (a-h,o-z)
      character(len=9)  :: input 
      integer           :: nx,tpo,tpi,tps
      integer*4         :: i,k,x,p,q,mp,mh
      logical           :: ver
      dimension ver(1:2)
      optional tpi
      intent(in) i,input,tpi
      dimension ox(1:2)
      if (i.eq.1) then 
        lmax(:)=nonp
        if (input.eq."incidence") then
          open(unit=1,file="para",form="formatted")
          do k=1,17
            read(1,*)
          end do
          read(1,*) fo
          read(1,*) omegao
          read(1,*) ekmax
          read(1,*) h1
          read(1,"(A)") thres
          if (compute_qp2.eqv..true.) read(1,*) ekmin
          close(1)
          if (fo.eq.0.d0) then
            field="n"
            omegao=0.d0
            epso(3)=0.d0        
          else  
            field="o"
            method="g"
            epso(3)=fo**2.d0/(4.d0*omegao**2.d0) 
          end if    
          call grid_parameters(0)
          call grid(0)
          if (compute_qp2.eqv..false.) then
            if (thres.eq."auto") then
              call threshold(ekmin) 
            else if (thres.eq."manu") then
              open(unit=1,file="para",form="formatted")
              do k=1,22
                read(1,*)
              end do
              read(1,*) ekmin
              close(1)
              call threshold(ekver)
              if (ekmin.lt.ekver) then
                if ((mode.eq."gbar").or.((mode.eq."chem").and.(PsX_target.eqv..true.))) then
                  ekmin=ekver
                  if (rank.eq.0) write(error_unit,'(A)') "warning: ekmin lower than the energy threshold"
                end if
              end if  
            end if
          end if
          look=.false.; if (ekmin.eq.ekmax) look=.true.
        else if (input.eq."resonance") then
          open(unit=1,file="para",form="formatted")
          do k=1,17
            read(1,*)
          end do
          read(1,*) fo
          do k=1,4
            read(1,*)
          end do
          read(1,*) ek
          close(1)  
          if (fo.eq.0.d0) stop
          field="o"
          call grid(0)
        end if
      else if (i.eq.2) then
        if (rank.eq.0) then
          call seconds(tpo)
          tps=tpo-tpi
          allocate(fpqo(pmin:pmax,1:ngpq,-lp:lp,-lh:lh),sidv(pmin:pmax))
          fpqo(:,:,:,:)=0.d0
          do x=1,xmax
            p=idex(1,x); q=idex(2,x)
            do mh=-lh,lh
              do mp=-lp,lp
                fpqo(p,q,mp,mh)=fx_ox(x,mp,mh)
              end do
            end do  
          end do
          allocate(sigma(-lp:lp,-lh:lh))
          sigma(:,:)=0.d0
          do mh=-lh,lh
            do mp=-lp,lp
              sidv(:)=0.d0
              do p=pmin,pmax
                ox(1)=kf(p)/nrm(2)
                if (mode.eq."chem") ox(1)=ox(1)*occ
                if (sens.eq."inv")  ox(1)=ox(1)*(dfloat(deg(1))/dfloat(deg(2)))*(2.d0*v/kf(p))**2.d0
                ox(2)=dot_product(fpqo(p,:,mp,mh),wtq)
                sidv(p)=product(ox)
              end do
              sigma(mp,mh)=sum(sidv)
            end  do
          end do  
          deallocate(fpqo,sidv)
          call lines("res",nx)
          open(unit=2,file="res",form="formatted")
          do p=1,nx
            read(2,*) 
          end do
          if (input.eq."incidence") then
            ek_var=ek
            if (use_Ps_energy.eqv..true.) ek_var=ek_var/1.d+3
            if (mode.eq."chem") then
              if (PsX_target.eqv..true.) then
                write(2,100) ek_var,sum(sigma)
              else 
                write(2,100) ek_var,sum(sigma)*pi
              end if
              100 format (f13.4,E25.12)
            else if (mode.eq."gbar") then
              if (solved_in_mp_mh.eqv..true.) then
                do mp=-lp,lp
                  do mh=-lh,lh
                    ver(1)=(mh.le.0).and.(mp.le.0)
                    ver(2)=(mh.lt.0).and.(mp.gt.0)
                    if ((ver(1).eqv..true.).or.(ver(2).eqv..true.)) then
                      write(2,101) ek_var,mp,mh,sigma(mp,mh)
                    end if  
                  end do
                end do  
                101 format (f13.4,I5,I5,E25.12)
              else 
                write(2,102) ek_var,sum(sigma)
                102 format (f13.4,E25.12)
              end if
            end if
          else if (input.eq."resonance") then
            write(2,*) omegao,sum(sigma),tps
          end if
          close(2)
          deallocate(sigma)
        end if
      end if
  end subroutine read_and_write

subroutine lines(input,nx)
      implicit none
      character(len=3)  :: input 
      integer           :: nx
      intent(in) input
      intent(out) nx
      nx=0
      open(unit=1,file=input,form="formatted",iostat=ierr)
      do while (ierr==0)
        read(1,*,iostat=ierr)
        if (ierr==0) nx=nx+1
      end do
      close(1)
  end subroutine lines       

subroutine log_scale(kin,aff)
      implicit none
      real*8    :: kin
      integer*4 :: aff
      intent(in) kin,aff
      if ((kin.ge.1.d+0).and.(kin.lt.1.d+1)) h1=1.d+0
      if ((kin.ge.1.d+1).and.(kin.lt.1.d+2)) h1=1.d+1
      if ((kin.ge.1.d+2).and.(kin.lt.1.d+3)) h1=1.d+2
      if ((kin.ge.1.d+3).and.(kin.lt.1.d+4)) h1=1.d+3
      if ((kin.ge.1.d+4).and.(kin.lt.1.d+5)) h1=1.d+4
      h1=h1/dfloat(aff)
  end subroutine log_scale

subroutine section(input)
      implicit integer*4 (a-z)
      character(len=9)  :: input
      integer           :: nx,tpi
      real*8            :: ekver
      intent(in) input
      call read_and_write(1,input)
      call tabular(1,"com")
      if (compute_qp2.eqv..false.) then
        if (input.eq."incidence") then 
          if ((mode.eq."chem").and.(PsX_target.eqv..false.)) then 
            if (rank.eq.0) write(error_unit,'(A)') "note: energy threshold adapted to the grid"
            npts_ek=1
            ek=ekmin
            do while (ek.le.ekmax) 
              call log_scale(ek,4)
              ek=ek+h1
              npts_ek=npts_ek+1
            end do
            npts_ek=npts_ek-1
            call threshold(ekver)
            allocate(csw(1:2,1:npts_ek))
            j=1
            ek=ekmin
            do while (ek.le.ekmax) 
              call log_scale(ek,4)
              csw(1,j)=ek
              csw(2,j)=dabs(csw(1,j)-ekver)
              ek=ek+h1
              j=j+1
            end do
            ekmin=csw(1,minloc(csw(2,:),1))
            if (ekmin.lt.ekver) ekmin=csw(1,minloc(csw(2,:),1)+1)
            deallocate(csw)
          end if
          call tabular(1,"var")
          ek=ekmin
          do while (ek.le.ekmax) 
            call block(input,tpi)
            if (mode.eq."chem") call log_scale(ek,4)
            ek=ek+h1
          end do
          call tabular(2,"var") 
        else if (input.eq."resonance") then
          call lines("npo",nx) 
          open(unit=3,file="npo",form="formatted")
          do k=1,nx
            read(3,*) omegao
            call threshold(ekver)
            if (ekmin.lt.ekver) ekmin=ekver 
            ek=ekmin
            epso(3)=fo**2.d0/(4.d0*omegao**2.d0)
            call tabular(1,"var")
            call block(input,tpi)
            call tabular(2,"var")
          end do
          close(3)
        end if
      end if  
      call tabular(2,"com") 
  end subroutine section
  
subroutine block(input,tpi)
     implicit none
      character(len=9)  :: input
      integer           :: tpi
      intent(in) input,tpi
      call seconds(tpi)
      call tabular(1,"bes")
      call para_mpi
      call read_and_write(2,input,tpi)
      call tabular(2,"bes")  
  end subroutine block

subroutine threshold(lim_inf)
      implicit real*8 (a-h,o-z)
      real*8  :: lim_inf
      intent(out) lim_inf
      if (sens.eq."dir") then 
        xmin=epso(1)-epso(2)+epso(3)
      else if (sens.eq."inv") then 
        xmin=epso(2)-epso(1)
      end if
      lim_inf=accur_2
      if (xmin.gt.0.d0) then 
        if (mode.eq."gbar") lim_inf=idint(xmin/cp)+1.d0
        if (mode.eq."chem") lim_inf=xmin/cp+accur_2
      end if
  end subroutine threshold   

subroutine transition_rate(state,n)
  implicit real*8 (a-h,o-z)
  character(len=2)  :: state 
  integer*4         :: n,l,k,ngpr,mp
  real*8, dimension(:,:,:), allocatable  :: coef_mp
  real*8, dimension(:,:), allocatable    :: itr,ox_pr
  real*8, dimension(:), allocatable      :: ox
  intent(in) state,n
  data ngpr/96/,th/2.41888432650516d-17/,one_over_c/7.297352566417d-3/
  external radial,d3j
  if (state.eq."is") a0=2.d0; if (state.eq."fs") a0=1.d0
  omega_bohr=0.5d0*dabs(1.d0/dfloat(n)**2.d0-1.d0/dfloat(np)**2.d0)/a0
  write(6,*) "Transition rate"
  do lp=0,np-1
  write(6,*) "lp",lp
  write(6,*) "---------------------------"
  allocate(t(1:ngpr),wt(1:ngpr))
  allocate(itr(iabs(lp-1):lp+1,1:ngpr),ox_pr(iabs(lp-1):lp+1,1:3),coef_mp(iabs(lp-1):lp+1,1:3,-lp:lp))
  t(:)=0.d0; wt(:)=0.d0
  call cgqf(ngpr,5,3.d0,0.d0,0.d0,(1.d0/dfloat(np)+1.d0/dfloat(n))/a0,t,wt)
  do l=iabs(lp-1),lp+1
    if (mod((1+l+lp),2).eq.0) then
      do k=1,ngpr
        itr(l,k)=radial(state,1.d0,"norm",n,l,t(k))*radial(state,1.d0,"norm",np,lp,t(k))
      end do
      coef_com=dot_product(itr(l,:),wt)*d3j(lp,1,l,0,0,0)*dsqrt(dfloat(2*l+1)*dfloat(2*lp+1))
      do k=1,3
        do mp=-lp,lp
          if (k.eq.3) coef_ad=coef_com*(d3j(lp,1,l,mp,-1,-mp+1)*(-1.d0)**(dfloat(mp-1))-&
          &d3j(lp,1,l,mp,1,-mp-1)*(-1.d0)**(dfloat(mp+1)))/dsqrt(2.d0)
          if (k.eq.2) coef_ad=coef_com*(d3j(lp,1,l,mp,-1,-mp+1)*(-1.d0)**(dfloat(mp-1))+&
          &d3j(lp,1,l,mp,1,-mp-1)*(-1.d0)**(dfloat(mp+1)))/dsqrt(2.d0)
          if (k.eq.1) coef_ad=coef_com*(d3j(lp,1,l,mp,0,-mp)*(-1.d0)**(dfloat(mp)))
          coef_mp(l,k,mp)=coef_ad
        end do
        ox_pr(l,k)=dot_product(coef_mp(l,k,:),coef_mp(l,k,:))/dfloat(2*l+1)
      end do
    end if
    gamma_rad=4.d0/3.d0*(omega_bohr*one_over_c)**3.d0*sum(ox_pr(l,:))
    if (mod((1+l+lp),2).eq.0) write(6,*) "Γ-rad [GHz]",n,l,gamma_rad/(1.d+9*th)
  end do
  deallocate(t,wt)
  deallocate(itr,ox_pr,coef_mp)
  write(6,*) "---------------------------"
  end do
  end subroutine transition_rate
  
subroutine para_inp
  implicit real*8 (a-h,o-z)
  character(len=2)  :: state 
  integer*4         :: n,l,k,ngpr,mp,kmax
  real*8            :: io,lambdao,kb
  real*8, dimension(:), allocatable    :: tab1,ox
  real*8, dimension(:,:), allocatable  :: itr,tab2
  data ngpr/96/,er/5.14220826d+9/,th/2.41888432650516d-17/,c/2.99792458d+8/,eps/8.85418782d-12/
  data hpJs/6.62607015d-34/,ehJ/4.35974434d-18/,cnvr2/4.55633525d+1/,kb/1.380649d-23/
  external radial,d3j
  if (np.eq.1) then 
    eo=5.d+4; n=2;  tau=2.d-12 
  end if
  if (np.eq.3) then
    eo=2.5d+4; n=5; tau=2.d-12
  end if  
  write(6,*) np,n
  state="is"
  if (state.eq."is") a0=2.d0; if (state.eq."fs") a0=1.d0
  omega_bohr=0.5d0*dabs(1.d0/dfloat(n)**2.d0-1.d0/dfloat(np)**2.d0)/a0
  cnvr1=hpJs/ehJ
  io=5.d-1*c*eps*eo**2.d0/1.d+6
  domega=-0.441d0/(2.d0*tau/cnvr1) 
  omegao=omega_bohr+domega
  lambdao=cnvr2/omegao
  write(6,*) "--------------------------"
  write(6,*) "Io [10^6 W/cm^2]",io
  write(6,*) "eo [10^4 V/cm]", eo/1.d+4
  write(6,*) "eo [ua]",eo/er
  write(6,*) "ωo [ua]",omegao
  write(6,*) "λo [nm]",lambdao
  write(6,*) "--------------------------"
  allocate(t(1:ngpr),wt(1:ngpr),itr(iabs(lp-1):lp+1,1:ngpr),tab1(iabs(lp-1):lp+1),tab2(iabs(lp-1):lp+1,-lp:lp),ox(iabs(lp-1):lp+1))
  t(:)=0.d0; wt(:)=0.d0; itr(:,:)=0.d0; tab1(:)=0.d0;  tab2(:,:)=0.d0; ox(:)=0.d0
  call cgqf(ngpr,5,3.d0,0.d0,0.d0,(1.d0/dfloat(np)+1.d0/dfloat(n))/a0,t,wt)
  do l=iabs(lp-1),lp+1
    if (mod((1+l+lp),2).eq.0) then
      do k=1,ngpr
        itr(l,k)=radial(state,1.d0,"norm",n,l,t(k))*radial(state,1.d0,"norm",np,lp,t(k))
      end do
      tab1(l)=eo/er*dot_product(itr(l,:),wt)*d3j(lp,1,l,0,0,0)*dsqrt(dfloat(2*l+1))
      do mp=-lp,lp
        tab2(l,mp)=tab1(l)*d3j(lp,1,l,mp,0,-mp)*(-1.d0)**(dfloat(mp))
      end do
      ox(l)=dot_product(tab2(l,:),tab2(l,:))
    end if
  end do
  omega1=dsqrt(sum(ox))
  deallocate(t,wt,itr,tab1,tab2,ox)
  amp_tdpt=(omega1/domega)**2.d0
  amp_rabi=omega1**2.d0/(omega1**2.d0+domega**2.d0)
  omegar=dsqrt(omega1**2.d0+domega**2.d0)
  write(6,*) "Ω1 [ua]",omega1
  write(6,*) "Ω1 [GHz]",omega1/(cnvr1*1.d+9)
  write(6,*) "𝛿ω [ua]",domega
  write(6,*) "Ω1/eo [ua]",omega1/(eo/er)
  write(6,*) "𝛿ν [GHz]",domega/(cnvr1*1.d+9)
  write(6,*) "Ωr [ua]",omegar
  write(6,*) "Ωr [GHz]",omegar/(cnvr1*1.d+9)
  write(6,*) "--------------------------"
  write(6,*) "amp_rab",amp_rabi
  write(6,*) "amp_tdp",amp_tdpt
  write(6,*) "1/4 amp_tdp",amp_tdpt/4.d0
  write(6,*) "--------------------------"
  Egs=48.d-3
  Temp=Egs*1.602176620898d-19/kb
  Vts=dsqrt(kb*Temp/9.10938356d-31)/1.d+3
  vmb=1.d+9/lambdao*sqrt(8.d0*dlog(2.d0)*kb*Temp/(2.d0*9.10938356d-31))
  cor=0.5d0*vmb*lambdao**2.d0/c
  write(6,*) "<E> [meV]", int(Egs*1.d+3)
  write(6,*) "T [K]",Temp
  write(6,*) "V [km/s]",vts
  write(6,*) "Δν [GHz]",vmb/1.d+9
  write(6,*) "Δλ/2 [nm]",cor/1.d+9
  omega_minus=omegao-vmb/(2.d0/cnvr1); omega_plus=omegao+vmb/(2.d0/cnvr1)
  write(6,*) "ωo-+Δω/2 [ua]", omega_minus,omega_plus
  domega_minus=omega_minus-omega_bohr; domega_plus=omega_plus-omega_bohr
  write(6,*) "𝛿ν-+Δν/2 [GHz]", domega_minus/cnvr1/1.d+9, domega_plus/cnvr1/1.d+9
  write(6,*) "amp_tdp-+Dopp",(omega1/domega_minus)**2.d0,(omega1/domega_plus)**2.d0
  call transition_rate(state,n)
  end subroutine para_inp

subroutine atomic_subshell_ionization
      implicit real*8 (a-h,o-z)
      integer*4                          :: npts,k,no,x,x_mpi
      integer                            :: nx
      real*8, dimension(:), allocatable  :: omega_mpi
      if (thres.eq."auto") omega=int(dabs(epso(1))*eh+1.d0)
      if (thres.eq."manu") omega=ekmin
      if (rank.eq.0) then
        do while (omega.le.ekmax)
          write(10,*) omega
          omega=omega+h1
        end do 
        npts=0
        open(unit=1,file="fort.10",form="formatted",iostat=ierr)
        do while (ierr==0)
          read(1,*,iostat=ierr)
          if (ierr==0) npts=npts+1
        end do
        close(1)
        do iproc=1,nproc-1
          call mpi_send(npts,1,mpi_int,iproc,1,mpi_comm_world,ierr)
        end do
      end if
      do iproc=1,nproc-1
        if (iproc.eq.rank) call mpi_recv(npts,3,mpi_int,0,1,mpi_comm_world,mpi_status_ignore,ierr)
      end do  
      allocate(omega_mpi(1:npts))
      if (rank.eq.0) then 
        open(unit=2,file="fort.10",form="formatted",iostat=ierr)
        do k=1,npts
          read(2,*) omega_mpi(k)
        end do
        close(2)
        do iproc=1,nproc-1
          call mpi_send(omega_mpi,npts,mpi_real8,iproc,1,mpi_comm_world,ierr)
        end do
      end if  
      do iproc=1,nproc-1
        if (iproc.eq.rank) call mpi_recv(omega_mpi,npts,mpi_real8,0,1,mpi_comm_world,mpi_status_ignore,ierr)
      end do  
      if (rank.eq.0) call system ("rm fort.10")
      call mpi_comm_size(mpi_comm_world,nproc,ierr) 
      call mpi_comm_rank(mpi_comm_world,rank,ierr)
      no=npts/nproc; nx=mod(npts,nproc)
      do iproc=0,nx-1
        if (iproc.eq.rank) no=no+1
      end do
      allocate(gx_in(1:npts),gx_ox(1:npts))
      gx_in(:)=0.d0; gx_ox(:)=0.d0
      do x_mpi=1,no; x=(x_mpi-1)*nproc+rank+1
        call atomic_subshell_ionization_tools(omega_mpi(x),gx_in(x))
      end do
      call mpi_barrier(mpi_comm_world,ierr)
      call mpi_reduce(gx_in,gx_ox,npts,mpi_real8,mpi_sum,0,mpi_comm_world,ierr) 
      if (rank.eq.0) then 
         open(unit=3,file="res",form="formatted",iostat=ierr)
         do x=1,npts
           write(3,100) omega_mpi(x),gx_ox(x)
         end do
         close(3)
         100 format(F13.4,E25.12)
      end if  
      deallocate(gx_in,gx_ox,omega_mpi)
  end subroutine atomic_subshell_ionization
  
subroutine atomic_subshell_ionization_tools(omega,cs)
      implicit real*8 (a-h,o-z)
      integer*4                          :: i,j,npts
      real*8                             :: lagint,momentum,nrm_num
      real*8, dimension(:), allocatable  :: itr,tab,mtr
      dimension comp(4),a(2)
      intent(in) omega
      intent(out) cs
      external lagint
      epso_qp2=omega/eh-dabs(epso(1))
      momentum=dsqrt(2.d0*epso_qp2)
      h3=xi(3)-xi(1)
      npts=ngrid(2)/2
      period=2.d0*pi/momentum
      allocate(mtr(iabs(lh-1):lh+1))
      allocate(ui(1:npts),vi(1:npts),tab(1:ngpx(2)))
      do j=iabs(lh-1),lh+1
        if (mod((1+lh+j),2).ne.0) cycle
        if ((rank.eq.0).and.(look.eqv..true.)) write(11,'(A,I13)')   "lhh    ",j
        call stromgren_parameters
        call common_rk4(j,npts,0,nrm_num)
        do i=1,ngpx(2)
          x=tx(2,i)
          comp(1)=lagint(x,ui,vi,npts,3)
          comp(2)=rfs(i)
          comp(3)=x
          if (method.eq."g") comp(4)=dexp(x)
          if (method.eq."r") comp(4)=wtx(2,i)
          tab(i)=product(comp)
        end do
        if (method.eq."g") mtr(j)=dot_product(tab,wtx(2,:)) 
        if (method.eq."r") mtr(j)=rint(tab,2,ngpx(2),7,h2)
      end do
      deallocate(ui,vi,tab)
      mtr(:)=mtr(:)**2.d0
      comp(1)=8.d0*pi**2/(3.d0*1.37035999174d+2)
      comp(2)=dfloat(occ)/dfloat(2*lh+1)
      comp(3)=omega/eh
      a(1)=0.d0; if (lh.ne.0) a(1)=dfloat(lh)*mtr(lh-1)
      a(2)=dfloat(lh+1)*mtr(lh+1)
      comp(4)=sum(a)
      cs=product(comp)
      cs=cs*2.800285205391d+1
      deallocate(mtr)
      if ((rank.eq.0).and.(look.eqv..true.)) then
        write(11,'(A,F13.6)') "hv [eV]",omega
        write(11,'(A,I13)')   "occ    ",occ
        write(11,'(A,F13.6)') "BE [ua]",dabs(epso(1))
        write(11,'(A,F13.6)') "BE [eV]",dabs(epso(1)*eh)
        write(11,'(A,F13.6)') "2π/k   ",period
        write(11,'(A,F13.6)') "gap    ",gap
        write(11,'(A,F13.6)') "rc(1)  ",rc(1)
        write(11,'(A,F13.6)') "rc(2)  ",rc(2)
        write(11,'(A,F13.6)') "k-local",zeta
        write(11,'(A,F13.6)') "nrm_num",nrm_num
        write(11,'(A,F13.6)') "σ [Mb] ",cs
      end if
  end subroutine atomic_subshell_ionization_tools

subroutine modified_coulomb_function(kx,tab)
      implicit real*8 (a-h,o-z)
      integer*4   :: k,kx,j,m,no,x,x_mpi,dimx,q,tab,iret,ivar
      integer     :: nx
      real*8      :: momentum
      complex*16  :: fc,gc,fcp,gcp,sig,zmin
      intent(in) kx,tab
      dimension dimx(1:2),comp(1:2)
      dimension tab(1:2)
      dimension fc(0:tab(1)),gc(0:tab(1)),fcp(0:tab(1)),gcp(0:tab(1)),sig(0:tab(1))
      data zmin/(0.d0,0.d0)/
      Zp(1)=0.d0
      Zp(2)=kx
      if (state_mod.eq."is") then
        if (form.eq."cdw") then 
         if (kx.eq.1) Zp(1)=+1.d0
         if (kx.eq.2) Zp(1)=-1.d0
        end if  
        ivar=1
        momentum=v
        q=kx
      else if (state_mod.eq."fs") then
        ivar=2
        momentum=kf(0)
        q=3
      end if
      beta=Zp(1)/momentum      
      call stromgren_parameters
      rho=rc(1)*momentum
      fc(:)=(0.d0,0.d0); fcp(:)=(0.d0,0.d0); gc(:)=(0.d0,0.d0); gcp(:)=(0.d0,0.d0); sig(:)=(0.d0,0.d0)
      call coulcc(dcmplx(rho,0.d0),dcmplx(beta,0.d0),zmin,tab(1)+1,fc,gc,fcp,gcp,sig,1,0,iret)
      dimx(1)=tab(1)+1
      dimx(2)=tab(2)+1
      xmax_tab=product(dimx)
      allocate(idex_tab(1:2,1:xmax_tab))
      idex_tab(:,:)=0; x=1
      do j=0,tab(1)
        do m=0,tab(2)
          idex_tab(1,x)=j
          idex_tab(2,x)=m
          x=x+1
        end do
      end do 
      call mpi_comm_size(mpi_comm_world,nproc,ierr) 
      call mpi_comm_rank(mpi_comm_world,rank,ierr)
      no=xmax_tab/nproc; nx=mod(xmax_tab,nproc)
      do iproc=0,nx-1
        if (iproc.eq.rank) no=no+1
      end do
      allocate(wx_in(1:2,1:xmax_tab),wx_ox(1:2,1:xmax_tab))
      wx_in(:,:)=0.d0; wx_ox(:,:)=0.d0
      allocate(hx_in(1:ngpx(kx),1:xmax_tab),hx_ox(1:ngpx(kx),1:xmax_tab))
      hx_in(:,:)=0.d0; hx_ox(:,:)=0.d0
      do x_mpi=1,no; x=(x_mpi-1)*nproc+rank+1
        call modified_coulomb_function_tools(kx,x,momentum)
      end do
      call mpi_barrier(mpi_comm_world,ierr)
      do k=1,ngpx(kx)
        call mpi_allreduce(hx_in(k,:),hx_ox(k,:),xmax_tab,mpi_real8,mpi_sum,mpi_comm_world,ierr) 
        if (k.le.2) then 
          call mpi_allreduce(wx_in(k,:),wx_ox(k,:),xmax_tab,mpi_real8,mpi_sum,mpi_comm_world,ierr) 
        end if
      end do
      do x=1,xmax_tab
        if (state_mod.eq."is") then
          bis(:,kx,idex_tab(1,x),idex_tab(2,x))=hx_ox(:,x)  
          bis(:,kx,idex_tab(1,x),-idex_tab(2,x))=bis(:,kx,idex_tab(1,x),idex_tab(2,x))
        else if (state_mod.eq."fs") then
          bfs(:,0,idex_tab(1,x),idex_tab(2,x))=hx_ox(:,x)
          bfs(:,0,idex_tab(1,x),-idex_tab(2,x))=bfs(:,0,idex_tab(1,x),idex_tab(2,x))
        end if
      end do
      deallocate(hx_in,hx_ox)
      fcp(:)=fcp(:)*momentum; gcp(:)=gcp(:)*momentum
      do x=1,xmax_tab
        comp(1)=wx_ox(1,x)*dreal(fcp(idex_tab(1,x)))-wx_ox(2,x)*dreal(fc(idex_tab(1,x)))
        comp(2)=wx_ox(1,x)*dreal(gcp(idex_tab(1,x)))-wx_ox(2,x)*dreal(gc(idex_tab(1,x)))
        sig_mod(q,idex_tab(1,x),idex_tab(2,x))=-datan2(comp(1),comp(2))
        sig_mod(q,idex_tab(1,x),-idex_tab(2,x))=sig_mod(q,idex_tab(1,x),idex_tab(2,x))
      end do
      if ((rank.eq.0).and.(look.eqv..true.)) then
        write(11,'(A,I4,E25.12)') "m,α(m)",idex_tab(2,x),alpha_PRA86(ivar,idex_tab(2,x))
        write(11,'(A,E25.12)') "Z         ",Zp(1)
        write(11,'(A,E25.12)') "momentum  ",momentum
        write(11,'(A,E25.12)') "Z/momentum",Zp(1)/momentum
        write(11,'(A)') "j,m,δ(non-Coulombic)"
        write(11,'(A)') "--------------------"
        do x=1,xmax_tab
          write(11,'(2I5,E25.12)') idex_tab(1,x),idex_tab(2,x),sig_mod(q,idex_tab(1,x),idex_tab(2,x))
        end do
        write(11,*) 
      end if
      deallocate(wx_in,wx_ox,idex_tab)
  end subroutine modified_coulomb_function

subroutine modified_coulomb_function_tools(kx,x,momentum)
      implicit real*8 (a-h,o-z)
      integer*4                          :: k,kx,npts,x
      real*8                             :: lagint,momentum,nrm_num
      real*8, dimension(:), allocatable  :: itr
      dimension comp(2)
      intent(in) kx,x,momentum
      external lagint
      epso_qp2=momentum**2.d0/2.d0
      h3=ei(3)-ei(1)
      npts=ngrid(2)/2
      period=2.d0*pi/momentum
      allocate(ui(1:npts),vi(1:npts),wi(1:npts))
      call stromgren_parameters
      call common_rk4(idex_tab(1,x),npts,idex_tab(2,x),nrm_num)
      do k=1,ngpx(kx)
        xx=tx(kx,k)
        comp(1)=lagint(xx,ui,vi,npts,3)
        comp(2)=dsqrt(pi*momentum)/(momentum*xx)
        hx_in(k,x)=product(comp)
      end do
      wx_in(1,x)=lagint(rc(1),ui,vi,npts,3)
      wx_in(2,x)=lagint(rc(1),ui,wi,npts,3)
      wx_in(:,x)=wx_in(:,x)*dsqrt(pi*momentum)
      deallocate(ui,vi,wi)
  end subroutine modified_coulomb_function_tools

subroutine common_rk4(j,npts,m,nrm_num)
      implicit real*8 (a-h,o-z)
      integer*4                          :: i,j,npts,idex,ngpr,m
      real*8                             :: k,kc,nrm_num,lagint
      real*8, dimension(:), allocatable  :: itr
      dimension k(4),g(4),kc(2),a(2),extrema(2)
      intent(in) j,npts,m
      intent(out) nrm_num
      external lagint
      x=0.d0
      dx=1.d0
      idex=1
      ui(:)=0.d0
      vi(:)=0.d0
      if (modified_coulomb.eqv..true.) wi(:)=0.d0
      do i=1,ngrid(2),2
        k(1)=dx;                  call potential_for_rk4(i,j,m,x,g(1))
        k(2)=dx+(h3/2.d0)*g(1);   call potential_for_rk4(i+1,j,m,x+(h3/2.d0)*k(1),g(2))
        k(3)=dx+(h3/2.d0)*g(2);   call potential_for_rk4(i+1,j,m,x+(h3/2.d0)*k(2),g(3))
        k(4)=dx+h3*g(3);          call potential_for_rk4(i+2,j,m,x+h3*k(3),g(4))
        x=x+(h3/6.d0)*(k(1)+2.d0*(k(2)+k(3))+k(4))
        dx=dx+(h3/6.d0)*(g(1)+2.d0*(g(2)+g(3))+g(4))
        ui(idex)=ei(i)
        vi(idex)=x
        if (modified_coulomb.eqv..true.) wi(idex)=dx
        idex=idex+1
      end do
      ngpr=nquad
      allocate(t(1:ngpr),wt(1:ngpr),itr(1:ngpr))      
      itr(:)=0.d0
      call cgqf(ngpr,1,0.d0,0.d0,rc(1),rc(2),t,wt)  
      do i=1,ngpr
        call local_wave_number(t(i),j,m,itr(i))
      end do
      zeta=dot_product(itr,wt)
      deallocate(t,wt,itr)
      do i=1,2
        call local_wave_number(rc(i),j,m,kc(i))
        extrema(i)=lagint(rc(i),ui,vi,npts,3)
        a(i)=dsqrt(pi*kc(i))*extrema(i)
      end do
      nrm_num=dsqrt((a(1)**2.d0+a(2)**2.d0-2.d0*product(a)*dcos(zeta))/dsin(zeta)**2.d0) 
      vi(:)=vi(:)/nrm_num
      if (modified_coulomb.eqv..true.) wi(:)=wi(:)/nrm_num
  end subroutine common_rk4

subroutine H_polarization
      implicit real*8 (a-h,o-z)
      integer*4  :: k,kmax,n,l,m,nvar,lvar,ivar
      dimension good_tab(1:2),tab(1:2)
      if (state_mod.eq."is") then
        nvar=np; lvar=lp; ivar=1
      else if (state_mod.eq."fs") then
        nvar=nh; lvar=lh; ivar=2
      end if
      kmax=0
      open(unit=1,file=trim(path(1))//trim("H_polarization"),form="formatted",iostat=ierr)
      do while (ierr==0)
        read(1,*,iostat=ierr)
        if (ierr==0) kmax=kmax+1
      end do
      close(1)
      open(unit=2,file=trim(path(1))//trim("H_polarization"),form="formatted",iostat=ierr)
      n=1; l=0
      do k=1,kmax
        read(2,*) tab(1),tab(2)
        if ((nvar.eq.n).and.(lvar.eq.l)) then
            good_tab(:)=tab(:)
            if (state_mod.eq."is") good_tab(:)=8.d0*good_tab(:)
        end if  
        if (l.eq.(n-1)) then
          n=n+1; l=0
        else 
          l=l+1
        end if  
      end do
      close(2)
      do m=-lvar,lvar
        alpha_PRA86(ivar,m)=good_tab(1)
        if (lvar.ne.0) alpha_PRA86(ivar,m)=alpha_PRA86(ivar,m)+good_tab(2)*(3.d0*dfloat(m)**2.d0-dfloat(lvar*(lvar+1)))/dfloat(lvar*(2*lvar-1))       
        if (ivar.eq.1) coeff=1.0d0
        if (ivar.eq.2) coeff=0.5d0
        d_PRA86(ivar,m)=(coeff*alpha_PRA86(ivar,m))**(1.d0/4.d0)
      end do  
  end subroutine H_polarization

subroutine stromgren_parameters
      rc(1)=600.d0
      gap=40.d0/pi
      rc(2)=rc(1)+gap*period
      nquad=4000
  end subroutine stromgren_parameters

subroutine local_wave_number(r,l,m,res)
      implicit real*8 (a-h,o-z)
      integer*4 :: k,l,m,ivar
      real*8    :: lagint
      intent(in) r,l,m
      intent(out) res
      parameter (h=1.d-16)
      dimension comp(3),dx(3)
      coef=dfloat(l*(l+1))
      if (compute_qp2.eqv..false.) then
        if ((mode.eq."gbar").or.((mode.eq."chem").and.(form.eq."cba"))) then 
          U=coef/(2.d0*r**2.d0)+Zp(1)/r
          dU=-coef/(r**3.d0)-Zp(1)/r**2.d0
          dU2=3.d0*coef/(r**4.d0)+2*Zp(1)/r**3.d0
        else if ((mode.eq."chem").and.(form.eq."cdw")) then 
          do k=1,3
            x=r+dfloat(k-2)*h
            dx(k)=coef/(2.d0*x**2.d0)+lagint(x,ei,fi(Zp(2),:),ngrid(2),3)
          end do
          U=dx(2)
          dU=(dx(3)-dx(1))/(2.d0*h)
          dU2=(dx(3)-2.d0*dx(2)+dx(1))/h**2.d0
          end if
      else if (compute_qp2.eqv..true.) then 
        do k=1,3
          x=r+dfloat(k-2)*h
          dx(k)=coef/(2.d0*x**2.d0)+lagint(x,ei,zi,ngrid(2),3)
        end do
        U=dx(2)
        dU=(dx(3)-dx(1))/(2.d0*h)
        dU2=(dx(3)-2.d0*dx(2)+dx(1))/h**2.d0
      end if
      if (state_mod.eq."is") ivar=1
      if (state_mod.eq."fs") ivar=2
      d=d_PRA86(ivar,m)
      U=U-alpha_PRA86(ivar,m)/(2.d0*(r**2.d0+d**2.d0)**2.d0)
      dU=dU+2.d0*alpha_PRA86(ivar,m)*r/((r**2.d0+d**2.d0)**3.d0)
      dU2=dU2+2.d0*alpha_PRA86(ivar,m)*(1.d0-6.d0*r**2.d0/(r**2.d0+d**2.d0))/(r**2.d0+d**2.d0)**3.d0
      A=2.d0*(epso_qp2-U)
      comp(1)=A
      comp(2)=-dU2/A
      comp(3)=(5.d0/16.d0)*(dU/A)**2.d0
      res=dsqrt(sum(comp))
  end subroutine local_wave_number

subroutine potential_for_rk4(i,l,m,x,res)
      implicit real*8 (a-h,o-z)
      integer*4 :: i,l,m,ivar
      real*8    :: x
      intent(in) i,l,m,x
      intent(out) res
      dimension tab(2)
      if (compute_qp2.eqv..false.) then
        if ((mode.eq."gbar").or.((mode.eq."chem").and.(form.eq."cba"))) then 
          arg=Zp(1)/ei(i)
        else if ((mode.eq."chem").and.(form.eq."cdw")) then 
          arg=fi(Zp(2),i)
        end if
      else if (compute_qp2.eqv..true.) then 
        arg=zi(i)
      end if  
      if (state_mod.eq."is") ivar=1
      if (state_mod.eq."fs") ivar=2
      d=d_PRA86(ivar,m)
      coef=dfloat(l*(l+1))
      U=coef/(2.d0*ei(i)**2.d0)+arg-alpha_PRA86(ivar,m)/(2.d0*(ei(i)**2.d0+d**2.d0)**2.d0)
      tab(1)=2.d0*(U-epso_qp2)
      tab(2)=x
      res=product(tab)
  end subroutine potential_for_rk4
  
subroutine hydrogenic_atoms_ionization
      implicit real*8 (a-h,o-z)
      integer*4                            :: n,k,lmax,x,l,m,ngpr
      character(len=2)                     :: state
      real*8                               :: mu,momentum
      real*8, dimension(:), allocatable    :: ax
      real*8, dimension(:,:), allocatable  :: itr
      data ngpr/160/,lmax/10/
      common lmax
      dimension fc(0:lmax),tab(1:4)
      external radial,d3j
      state="fs"
      omega=300.d0/eh
      if (state.eq."is") mu=0.5d0; if (state.eq."fs") mu=1.d0
      open(unit=1,file="res",form="formatted",iostat=ierr)
      do n=0,5
        eps=0.5d0*mu/dfloat(n)**2.d0; if (omega.le.eps) cycle
        do l=0,n-1
          momentum=dsqrt(2.d0*mu*(omega-eps))
          beta=-mu/momentum
          allocate(t(1:ngpr),wt(1:ngpr),itr(0:lmax,1:ngpr))
          t(:)=0.d0; wt(:)=0.d0; itr(:,:)=0.d0
          call cgqf(ngpr,5,2.d0,0.d0,0.d0,mu/dfloat(n),t,wt)
          do k=1,ngpr
            rho=momentum*t(k)
            call coulomb_function(rho,beta,lmax,fc)
            itr(:,k)=fc(:)/momentum*radial(state,1.d0,"norm",n,l,t(k))
          end do
          accu=0.d0
          allocate(ax(iabs(l-1):l+1))
          do m=-l,l
            ax(:)=0.d0
            do x=iabs(l-1),l+1
              if (mod((1+l+x),2).ne.0) cycle
              tab(1)=dot_product(itr(x,:),wt)
              tab(2)=dsqrt(dfloat(2*x+1))
              tab(3)=d3j(l,1,x,0,0,0)
              tab(4)=d3j(l,1,x,m,0,-m)
              ax(x)=product(tab)
              ax(x)=ax(x)**2.d0
            end do
            accu=accu+sum(ax)
          end do
          deallocate(ax)
          cs=8.d0*pi/1.37035999174d+2*omega*momentum*accu
          write(1,100) n,l,cs,"[ua]"
          100 format (I5,I5,E25.12,x,A)
          deallocate(t,wt,itr)
        end do
      end do
      close(1)
  end subroutine hydrogenic_atoms_ionization

subroutine test
  implicit real*8 (a-h,o-z)
  integer*4                         :: k,npts
  double precision                  :: mu,lagint
  real*8, dimension(:), allocatable :: tab
  external lagint,radial
  dimension comp(1:3)
  npts=0
  open(unit=1,file="fort.10",form="formatted",iostat=ierr)
  do while (ierr==0)
    read(1,*,iostat=ierr)
    if (ierr==0) npts=npts+1
  end do
  close(1)
  allocate(xi(1:npts),yi(1:npts),zi(1:npts))
  open(unit=2,file="fort.10",form="formatted",iostat=ierr)
  do k=1,npts
    read(2,*) xi(k),yi(k),zi(k)
  end do
  close(2)
  allocate(tab(1:ngpt),t(ngpt),wt(ngpt))
  call cgqf(ngpt,5,1.d0,0.d0,0.d0,1.d0,t,wt)
  do k=1,ngpt
    comp(1)=lagint(t(k),xi,yi,npts,3)
    comp(2)=lagint(t(k),xi,zi,npts,3)
    !comp(1)=t(k)*radial("fs",1.d0,"exp-",1,0,t(k))
    !comp(2)=t(k)*radial("fs",1.d0,"exp-",2,1,t(k))
    comp(3)=dexp(t(k))
    tab(k)=product(comp)
  end do
  mu=dot_product(tab,wt)
  write(6,'(A,F13.6)') "mu  (au)",mu
  omega=0.08550927d0
  !omega=0.375d0
  comp(1)=1.d0/mu**2.d0
  comp(2)=1.d0/omega**3.d0
  comp(3)=0.04668532d0
  write(6,'(A,F13.6)') "Γ-1 (ns)",3.d0*product(comp)
  deallocate(tab,t,wt)
  deallocate(xi,yi,zi)
  end subroutine test
  
subroutine data_biraben(eps,xi2)
  implicit real*8 (a-h,o-z)
      integer*4         :: k,q
      character(len=3)  :: eps
      intent(in) eps,xi2
      rad(:,:)=0.d0
      rad(1,3)=0.626831503513086d0 
      rad(1,5)=0.167343810813369d0
      rad(1,8)=6.822378722478062d-2
      rad(1,12)=3.43940468319939d-2
      rad(1,17)=1.97390879210772d-2
      rad(2,5)=2.246050608223220d-2
      rad(2,8)=9.673325671498229d-3
      rad(2,12)=4.95101045938208d-3
      rad(2,17)=2.85987957933002d-3
      rad(3,3)=-rad(1,3)
      rad(3,4)=6.317017335627779d-3
      rad(3,6)=6.468625751682888d-2
      rad(3,7)=2.579553512399500d-3
      rad(3,9)=2.063642809919630d-2
      rad(3,11)=1.28932564046406d-3
      rad(3,13)=9.43049611310878d-3
      rad(3,16)=7.35397606113433d-4
      rad(3,18)=5.14778324279409d-3
      rad(4,4)=-rad(3,4)
      rad(4,8)=3.066791029581214d-3
      rad(4,12)=1.63860566856981d-3
      rad(4,17)=9.55608614603801d-3
      rad(5,5)=-rad(1,5)-rad(2,5)
      rad(5,7)=1.836424181367483d-3
      rad(5,9)=7.041437097787818d-3
      rad(5,11)=9.05183007199363d-4
      rad(5,13)=3.39331414930273d-3
      rad(5,16)=5.07444556216594d-4
      rad(5,18)=1.87880298500025d-3
      rad(6,6)=-rad(3,6)
      rad(6,8)=3.477252887796412d-4
      rad(6,10)=1.37954685997476d-2
      rad(6,12)=1.49617025983364d-4
      rad(6,14)=4.54461716424474d-3
      rad(6,17)=7.82834577083434d-5
      rad(6,19)=2.14720341142885d-3
      rad(7,7)=-rad(5,7)-rad(3,7)
      rad(7,12)=7.37587457685501d-4
      rad(7,17)=4.45863326554335d-4
      rad(8,8)=-rad(4,8)-rad(6,8)-rad(2,8)-rad(1,8)
      rad(8,11)=6.45424233702572d-4
      rad(8,13)=1.48659334750723d-3
      rad(8,16)=3.58434347594779d-4
      rad(8,18)=8.62660070164897d-4
      rad(9,9)=-rad(3,9)-rad(5,9)
      rad(9,12)=1.88560985771725d-4
      rad(9,14)=2.58581890240769d-3
      rad(9,17)=9.42194152487095d-5
      rad(9,19)=1.28768086643420d-3
      rad(10,10)=-rad(6,10)
      rad(10,13)=5.0504275437649d-5
      rad(10,15)=4.2564920204242d-3
      rad(10,18)=2.1461347773902d-5
      rad(10,20)=1.3735262575298d-3
      rad(11,11)=-rad(3,11)-rad(5,11)-rad(8,11)
      rad(12,12)=-rad(1,12)-rad(2,12)-rad(4,12)-rad(6,12)-rad(7,12)-rad(9,12)
      rad(12,16)=2.6831866486691d-4
      rad(12,18)=4.4973020585264d-4
      rad(11,13)=-rad(3,13)-rad(5,13)-rad(8,13)-rad(10,13)
      rad(11,17)=9.5985266524577d-5
      rad(11,19)=7.2364983211232d-4
      rad(14,14)=-rad(6,14)-rad(9,14) 
      rad(14,18)=3.9100866384280d-5
      rad(14,20)=1.1062943182514d-3    
      rad(15,15)=-rad(10,15)
      rad(15,19)=1.1379027273443d-5
      rad(15,21)=1.6457270850021d-3
      rad(16,16)=-rad(3,16)-rad(5,16)-rad(8,16)-rad(12,16)   
      rad(17,17)=-rad(1,17)-rad(2,17)-rad(4,17)-rad(6,17)-rad(7,17)-rad(9,17)-rad(11,17)-rad(13,17)  
      rad(18,18)=-rad(3,18)-rad(5,18)-rad(8,18)-rad(10,18)-rad(12,18)-rad(14,18)
      rad(19,19)=-rad(6,19)-rad(9,19)-rad(13,19)-rad(15,19)     
      rad(20,20)=-rad(10,20)-rad(14,20)       
      rad(21,21)=-rad(15,21)
      sig3b_1s(:)=0.d0; sig4b_1s(:)=0.d0
      sig3b_3d(:)=0.d0; sig4b_3d(:)=0.d0
      sig3b_5f(:)=0.d0; sig4b_5f(:)=0.d0
      sig3b_3d_on(:)=0.d0; sig4b_3d_on(:)=0.d0
      sig3b_var(:)=0.d0; sig4b_var(:)=0.d0
      sig_phH_mines(:)=0.d0; sig_phH_plus(:)=0.d0
      gam_phH_mines(:)=0.d0; gam_phH_plus(:)=0.d0
      phi_mines=9.863626487558427d-10*2.d0*dsqrt(xi2)
      sig_phH_mines(4)=0.209159137219877d0
      sig_phH_mines(5)=0.153926005189575d0
      sig_phH_mines(6)=5.353030813815243d-2
      sig_phH_mines(7)=8.965384936180133d-2
      sig_phH_mines(8)=6.111326704203619d-2
      sig_phH_mines(9)=2.321007101722131d-2
      sig_phH_mines(10)=3.490310476719109d-3
      sig_phH_mines(11)=4.622627877943461d-2
      sig_phH_mines(12)=3.055316452247830d-2
      sig_phH_mines(13)=1.192309440375870d-2
      sig_phH_mines(14)=2.370819084660545d-3
      sig_phH_mines(15)=1.828101139894302d-4
      sig_phH_mines(16)=2.685098619300538d-2
      sig_phH_mines(17)=1.747063103702553d-2
      sig_phH_mines(18)=6.902406071344539d-3 
      sig_phH_mines(19)=1.528107479332327d-3
      sig_phH_mines(20)=1.720835532805863d-4
      sig_phH_mines(21)=7.644316512306385d-6
      sig3b_1s(1)=8.18497108454365d0
      sig3b_3d(1)=5.944429692054420d-3
      sig3b_3d(2)=7.58483003806793d0
      sig3b_3d(3)=16.1397405070357d0
      sig3b_3d(4)=203.087537998136d0
      sig3b_3d(5)=335.589649959747d0 
      sig3b_3d(6)=260.942081393704d0 
      sig3b_3d(7)=88.9943695233920d0
      sig3b_3d(8)=488.776204863298d0 
      sig3b_3d(9)=907.144357173008d0 
      sig3b_3d(10)=959.566248496551d0  
      sig3b_3d(11)=86.5791215634508d0
      sig3b_3d(12)=169.689768166038d0 
      sig3b_3d(13)=325.911277374396d0 
      sig3b_3d(14)=708.255461773418d0  
      sig3b_3d(15)=1251.27429886612d0 
      sig3b_3d(16)=39.2146270280700d0 
      sig3b_3d(17)=78.7477549808034d0 
      sig3b_3d(18)=151.280902498787d0 
      sig3b_3d(19)=303.529546346550d0
      sig3b_3d(20)=477.653031933305d0 
      sig3b_3d(21)=788.823724950675d0
      sig3b_5f(1)=9.932639114848475d-2
      sig3b_5f(2)=0.268275381055307d0
      sig3b_5f(3)=0.447743443289190d0
      sig3b_5f(4)=17.0138301228596d0
      sig3b_5f(5)=34.9131890512655d0
      sig3b_5f(6)=21.3737761430434d0
      sig3b_5f(7)=52.3967506999277d0
      sig3b_5f(8)=129.428643175551d0
      sig3b_5f(9)=149.397398371094d0
      sig3b_5f(10)=121.255138313674d0
      sig3b_5f(11)=14.6774871253760d0
      sig3b_5f(12)=97.5051131719035d0
      sig3b_5f(13)=124.215169058992d0
      sig3b_5f(14)=224.854050944841d0
      sig3b_5f(15)=272.633661436098d0
      sig3b_5f(16)=16.8834548079995d0
      sig3b_5f(17)=48.6833916017663d0
      sig3b_5f(18)=79.4594939929574d0
      sig3b_5f(19)=154.128128273877d0
      sig3b_5f(20)=146.302800397644d0
      sig3b_5f(21)=448.330522562092d0
      sig4b_1s(2)=18.1187162188501d0
      sig4b_1s(3)=1.98305823391468d0
      sig4b_1s(4)=3.850789480928098d-3
      sig4b_1s(5)=0.334327031336996d0
      sig4b_1s(6)=8.164498752917893d-2
      sig4b_1s(7)=5.002926251413185d-2
      sig4b_1s(8)=0.120269286066231d0
      sig4b_1s(9)=3.462499187563040d-2 
      sig4b_1s(10)=1.457873297232210d-3
      sig4b_1s(11)=2.845237993536368d-2
      sig4b_1s(12)=5.719434105517220d-2 
      sig4b_1s(13)=1.746172944010604d-2
      sig4b_1s(14)=1.026068141095243d-3 
      sig4b_1s(15)=1.595100014936771d-5 
      sig4b_1s(16)=1.688639472033986d-2
      sig4b_1s(17)=3.188031531402362d-2
      sig4b_1s(18)=9.965603684534703d-3
      sig4b_1s(19)=6.714047151537174d-4
      sig4b_1s(20)=1.573172582565272d-5
      sig4b_1s(21)=1.180029207963794d-7
      sig4b_3d(1)=9.84031173307966d0
      sig4b_3d(2)=1.028491793261377d-2
      sig4b_3d(3)=6.194449193966713d-3
      sig4b_3d(4)=2.214207216656121d-4
      sig4b_3d(5)=1.224553573711674d-3
      sig4b_3d(6)=2.860554694289900d-4
      sig4b_3d(7)=1.273456789393682d-4
      sig4b_3d(8)=4.521332791024839d-4
      sig4b_3d(9)=1.313862130688725d-4 
      sig4b_3d(10)=9.366472122295655d-6   
      sig4b_3d(11)=6.429390935703768d-5
      sig4b_3d(12)=2.164491289860785d-4
      sig4b_3d(13)=6.849449524017536d-5
      sig4b_3d(14)=6.870280244604520d-6
      sig4b_3d(15)=2.604876111040890d-7
      sig4b_3d(16)=3.635549658598484d-5
      sig4b_3d(17)=1.204794149392767d-4
      sig4b_3d(18)=3.960990268138516d-5
      sig4b_3d(19)=4.539923541147042d-6
      sig4b_3d(20)=2.540338694813929d-7
      sig4b_3d(21)=5.387858570912466d-9
      sig4b_5f(1)=0.613204995360397d0
      sig4b_5f(2)=6.898071780820790d-5
      sig4b_5f(3)=1.096863224222369d-4
      sig4b_5f(4)=3.514973108746946d-6
      sig4b_5f(5)=1.964462262673227d-5
      sig4b_5f(6)=5.083510504380160d-6
      sig4b_5f(7)=1.393634176897550d-6
      sig4b_5f(8)=7.014091656402230d-6
      sig4b_5f(9)=2.271075583338200d-6
      sig4b_5f(10)=2.030922864684926d-7
      sig4b_3d_on(:)=sig4b_3d(:)
      if (eps.eq."-pr") then
        phi_plus=7.256171906746860d-9
        sig_phH_plus(16)=1.41534216392928d0
        sig_phH_plus(17)=1.46262378848558d0
        sig_phH_plus(18)=1.41484819509030d0 
        sig_phH_plus(19)=1.11887640188592d0 
        sig_phH_plus(20)=0.61339594693644d0
        sig_phH_plus(21)=0.17019981975472d0   
        sig3b_3d_on(1)=sig3b_3d(1)+1.637388468750365d-3
        sig3b_3d_on(2)=sig3b_3d(2)+3.26520855946066d0
        sig3b_3d_on(3)=sig3b_3d(3)+7.62599112923746d0
        sig3b_3d_on(4)=sig3b_3d(4)+14.0995792114971d0
        sig3b_3d_on(5)=sig3b_3d(5)+40.2300672412648d0
        sig3b_3d_on(6)=sig3b_3d(6)+29.9960839025352d0
        sig3b_3d_on(7)=sig3b_3d(7)+23.4560286038795d0
        sig3b_3d_on(8)=sig3b_3d(8)+89.8512773330619d0
        sig3b_3d_on(9)=sig3b_3d(9)+116.181777401531d0
        sig3b_3d_on(10)=sig3b_3d(10)+60.7909566356536d0
        sig3b_3d_on(11)=sig3b_3d(11)+19.1671530498983d0
        sig3b_3d_on(12)=sig3b_3d(12)+51.8353089419221d0
        sig3b_3d_on(13)=sig3b_3d(13)+144.944460084737d0
        sig3b_3d_on(14)=sig3b_3d(14)+196.295618505686d0
        sig3b_3d_on(15)=sig3b_3d(15)+128.383259235759d0
        sig3b_3d_on(16)=sig3b_3d(16)+8.16763341564437d0
        sig3b_3d_on(17)=sig3b_3d(17)+72.3072260513752d0
        sig3b_3d_on(18)=sig3b_3d(18)+62.1441416182942d0
        sig3b_3d_on(19)=sig3b_3d(19)+102.228342865127d0 
        sig3b_3d_on(20)=sig3b_3d(20)+149.500687629912d0
        sig3b_3d_on(21)=sig3b_3d(21)+160.606494845558d0
        sig4b_3d_on(1)=sig4b_3d(1)+0.926157302704593d0
      else if (eps.eq."-25") then
        phi_plus=7.257508380139326d-9
        sig_phH_plus(16)=1.41588799181201d0
        sig_phH_plus(17)=1.46321483161529d0
        sig_phH_plus(18)=1.41548878871472d0 
        sig_phH_plus(19)=1.11948298625736d0 
        sig_phH_plus(20)=0.61381161869158d0
        sig_phH_plus(21)=0.17034646920671d0  
        sig3b_3d_on(1)=sig3b_3d(1)+1.149174533160096d-3
        sig3b_3d_on(2)=sig3b_3d(2)+2.28985278628092d0
        sig3b_3d_on(3)=sig3b_3d(3)+5.34960861671352d0
        sig3b_3d_on(4)=sig3b_3d(4)+9.88245505792252d0
        sig3b_3d_on(5)=sig3b_3d(5)+28.2244211259583d0
        sig3b_3d_on(6)=sig3b_3d(6)+21.0438857391701d0
        sig3b_3d_on(7)=sig3b_3d(7)+16.3956370605635d0
        sig3b_3d_on(8)=sig3b_3d(8)+62.8853427976488d0
        sig3b_3d_on(9)=sig3b_3d(9)+81.3583022331057d0
        sig3b_3d_on(10)=sig3b_3d(10)+42.6048254193051d0
        sig3b_3d_on(11)=sig3b_3d(11)+13.4111466028202d0
        sig3b_3d_on(12)=sig3b_3d(12)+36.2234420609818d0
        sig3b_3d_on(13)=sig3b_3d(13)+101.401824028607d0
        sig3b_3d_on(14)=sig3b_3d(14)+137.389053734617d0
        sig3b_3d_on(15)=sig3b_3d(15)+89.8726141273626d0
        sig3b_3d_on(16)=sig3b_3d(16)+5.71084947958477d0
        sig3b_3d_on(17)=sig3b_3d(17)+50.5743732345582d0
        sig3b_3d_on(18)=sig3b_3d(18)+43.4632719901754d0
        sig3b_3d_on(19)=sig3b_3d(19)+71.4993977373954d0 
        sig3b_3d_on(20)=sig3b_3d(20)+104.595614875364d0
        sig3b_3d_on(21)=sig3b_3d(21)+112.391851400106d0
        sig4b_3d_on(1)=sig4b_3d(1)+0.650101353952386d0
      else if (eps.eq."-48") then
        phi_plus=7.258023910147738d-9
        sig_phH_plus(16)=1.41609856735472d0
        sig_phH_plus(17)=1.46344285234026d0
        sig_phH_plus(18)=1.41573593161607d0 
        sig_phH_plus(19)=1.11971702095929d0 
        sig_phH_plus(20)=0.61397200962629d0
        sig_phH_plus(21)=0.17040306286448d0      
        sig3b_3d_on(1)=sig3b_3d(1)+1.018036496738955d-3
        sig3b_3d_on(2)=sig3b_3d(2)+2.02793854489625d0
        sig3b_3d_on(3)=sig3b_3d(3)+4.73826080214828d0
        sig3b_3d_on(4)=sig3b_3d(4)+8.75025390687215d0
        sig3b_3d_on(5)=sig3b_3d(5)+25.0000537637077d0
        sig3b_3d_on(6)=sig3b_3d(6)+18.6396054232040d0
        sig3b_3d_on(7)=sig3b_3d(7)+14.5019601139205d0
        sig3b_3d_on(8)=sig3b_3d(8)+55.6494126281817d0
        sig3b_3d_on(9)=sig3b_3d(9)+72.0120591971182d0
        sig3b_3d_on(10)=sig3b_3d(10)+37.7224259223608d0
        sig3b_3d_on(11)=sig3b_3d(11)+11.8667507275763d0
        sig3b_3d_on(12)=sig3b_3d(12)+32.0365282529871d0
        sig3b_3d_on(13)=sig3b_3d(13)+89.7195233139553d0
        sig3b_3d_on(14)=sig3b_3d(14)+121.582073344996d0
        sig3b_3d_on(15)=sig3b_3d(15)+79.5380364561995d0
        sig3b_3d_on(16)=sig3b_3d(16)+5.05183774071789d0
        sig3b_3d_on(17)=sig3b_3d(17)+44.7440021483246d0
        sig3b_3d_on(18)=sig3b_3d(18)+38.4517902946678d0
        sig3b_3d_on(19)=sig3b_3d(19)+63.2557339386675d0 
        sig3b_3d_on(20)=sig3b_3d(20)+92.5475127542668d0
        sig3b_3d_on(21)=sig3b_3d(21)+97.8160037879611d0
        sig4b_3d_on(1)=sig4b_3d(1)+0.575946839290866d0
      end if
      sig3b_1s(:)=sig3b_1s(:)*0.88d-20; sig4b_1s(:)=sig4b_1s(:)*0.88d-20
      sig3b_3d(:)=sig3b_3d(:)*0.88d-20; sig4b_3d(:)=sig4b_3d(:)*0.88d-20
      sig3b_5f(:)=sig3b_5f(:)*0.88d-20; sig4b_5f(:)=sig4b_5f(:)*0.88d-20
      sig3b_3d_on(:)=sig3b_3d_on(:)*0.88d-20; sig4b_3d_on(:)=sig4b_3d_on(:)*0.88d-20
      gam_phH_mines(:)=sig_phH_mines(:)*phi_mines/2.41888432650516d-8
      gam_phH_plus(:)=sig_phH_plus(:)*phi_plus/2.41888432650516d-8
  end subroutine data_biraben
  
subroutine biraben
  implicit real*8 (a-h,o-z)
  integer*4          :: var,onoff,k,q,pdel,ldel,ldel2,npas,time,mtime,npts,qmin,qmax,ionisation,Ir,x
  real*8             :: i,npbar,k_1,k_2,k_3,k_4,m_1,m_2,m_3,m_4
  complex*16         :: ci,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore
  complex*16         :: kgg_1,kge_1,keg_1,kee_1,krr_1,kgr_1,krg_1,ker_1,kre_1
  complex*16         :: kgg_2,kge_2,keg_2,kee_2,krr_2,kgr_2,krg_2,ker_2,kre_2
  complex*16         :: kgg_3,kge_3,keg_3,kee_3,krr_3,kgr_3,krg_3,ker_3,kre_3
  complex*16         :: kgg_4,kge_4,keg_4,kee_4,krr_4,kgr_4,krg_4,ker_4,kre_4
  complex*16         :: agg,age,aeg,aee,arr,agr,arg,aer,are,drv
  character(len=3)   :: eps
  character(len=2)   :: sim
  real*8, dimension(:,:), allocatable  :: cnvp
  logical ver
  data mtime/-200/,time/500/
  dimension k_1(1:nexch),k_2(1:nexch),k_3(1:nexch),k_4(1:nexch),ver(1:2)
  common/positon/t_eimp
  common/gr1/t_limp_mines,t_limp_plus,onoff,ldel,ldel2
  common/gr2/vpbar,psd,npbar,gam_d,rhogg,rhoee,rhorr
  common/gr3/t_pimp,pdel
  common/gr4/tau,gam_e,gam_r,gam_phPs_e,gam_phPs_r,omega_plus,omega_mines,domega,ci
  external xilas,prodPs,pshape,drv
       open(5,file="entr_2",status='unknown')
        read(5,*) sim
        read(5,*) eps
        read(5,*) Em
        read(5,*) t_pimp
        read(5,*) acell,scell
        read(5,*) z_ps,z_pbar
        read(5,*) t_eimp
        read(5,*) Etau
        read(5,*) t_limp_mines
        read(5,*) pdel
        read(5,*) ldel
        read(5,*) t_limp_plus
        read(5,*) qmin,qmax
        read(5,*) ionisation
        read(5,*) onoff
        read(5,*) di
      close(5)
      ci=dcmplx(0.d0,1.d0)
      pi=4.d0*datan(1.d0)
      sol=299792458.d0
      vpbar=sqrt(2.d0*Em/938272.013d0)*sol
      tcross=scell/vpbar*(1.d9)
      psd=z_Ps/acell/scell
      npbar=z_pbar
      npas=idint(tcross)+1
      npts=int(npas/di)
      S=10.d-2
      Ir=7.06d+5
      xi=((Etau/(Ir*S*t_limp_mines*1.d-9))*2.d0*dsqrt(log(2.d0)/pi))**2.d0
      write(6,*) "sim",sim
      write(6,*) "eps",eps
      write(6,*) "ionisation",ionisation
      write(6,*) "onoff",onoff
      write(6,*) "-------------------------"
      write(6,*) "t_pimp",int(t_pimp)
      write(6,*) "t_limp_mines",int(t_limp_mines)
      write(6,*) "Etau",Etau
      write(6,*) "xi",xi
      write(6,*) "pdel",pdel
      write(6,*) "ldel",ldel
      write(6,*) "-------------------------"
      tau=142.d0
      gam_e=1.d0/31.d0  
      gam_g=xi2*gam_e
      omega_mines=dsqrt(gam_g*gam_e)
      gam_phPs_e=0.047d0*gam_e*dsqrt(xi2)
      gam_d=8.453675905785703d-10*2.d0*dsqrt(xi2)/2.41888432650516d-8
      gam_r=2.2723085821223720d-3
      omega_plus=1.4532614815233727d-5/2.41888432650516d-8
      domega=-1.675612552174332d-5/2.41888432650516d-8 
      gam_phPs_r=5.16641733d-1
      call data_biraben(eps,xi2)
      if (sim.eq."3b") then 
        if (ionisation.eq.0) then 
          gam_phH_mines(:)=0.d0; gam_phH_plus(:)=0.d0
        end if  
      else if (sim.eq."4b") then 
        if (ionisation.eq.0) then 
          gam_d=0.d0
        else if (ionisation.eq.-1) then 
          gam_d=0.d0; gam_phH_mines(:)=0.d0; gam_phH_plus(:)=0.d0
        end if
      end if
      do ldel2=qmin,qmax
      x=pdel+t_pimp
      allocate(cnvp(x:x,1:npts))
      cnvp(:,:)=0.d0
      do pdel=x,x
        rhogg=dcmplx(0.d0,0.d0)
        rhoge=dcmplx(0.d0,0.d0)
        rhoeg=dcmplx(0.d0,0.d0)
        rhoee=dcmplx(0.d0,0.d0)
        rhorr=dcmplx(0.d0,0.d0)
        rhogr=dcmplx(0.d0,0.d0)
        rhorg=dcmplx(0.d0,0.d0)
        rhoer=dcmplx(0.d0,0.d0)
        rhore=dcmplx(0.d0,0.d0)
        phb(:)=0.d0; phbp=0.d0
        var=0
        k=1
        i=dfloat(mtime)
        do while (var.eq.0)
          kgg_1=drv(1,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          kge_1=drv(2,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          keg_1=drv(3,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          kee_1=drv(4,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          krr_1=drv(5,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          kgr_1=drv(6,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          krg_1=drv(7,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          ker_1=drv(8,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          kre_1=drv(9,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
          agg=rhogg+di/2.d0*kgg_1
          age=rhoge+di/2.d0*kge_1
          aeg=rhoeg+di/2.d0*keg_1
          aee=rhoee+di/2.d0*kee_1
          arr=rhorr+di/2.d0*krr_1
          agr=rhogr+di/2.d0*kgr_1
          arg=rhorg+di/2.d0*krg_1
          aer=rhoer+di/2.d0*ker_1
          are=rhore+di/2.d0*kre_1
          kgg_2=drv(1,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kge_2=drv(2,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          keg_2=drv(3,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kee_2=drv(4,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          krr_2=drv(5,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kgr_2=drv(6,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          krg_2=drv(7,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          ker_2=drv(8,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kre_2=drv(9,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          agg=rhogg+di/2.d0*kgg_2
          age=rhoge+di/2.d0*kge_2
          aeg=rhoeg+di/2.d0*keg_2
          aee=rhoee+di/2.d0*kee_2
          arr=rhorr+di/2.d0*krr_2
          agr=rhogr+di/2.d0*kgr_2
          arg=rhorg+di/2.d0*krg_2
          aer=rhoer+di/2.d0*ker_2
          are=rhore+di/2.d0*kre_2
          kgg_3=drv(1,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kge_3=drv(2,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          keg_3=drv(3,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kee_3=drv(4,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          krr_3=drv(5,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kgr_3=drv(6,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          krg_3=drv(7,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          ker_3=drv(8,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          kre_3=drv(9,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di/2.d0)
          agg=rhogg+di*kgg_3
          age=rhoge+di*kge_3
          aeg=rhoeg+di*keg_3
          aee=rhoee+di*kee_3
          arr=rhorr+di*krr_3
          agr=rhogr+di*kgr_3
          arg=rhorg+di*krg_3
          aer=rhoer+di*ker_3
          are=rhore+di*kre_3
          kgg_4=drv(1,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          kge_4=drv(2,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          keg_4=drv(3,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          kee_4=drv(4,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          krr_4=drv(5,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          kgr_4=drv(6,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          krg_4=drv(7,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          ker_4=drv(8,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)
          kre_4=drv(9,agg,age,aeg,aee,arr,agr,arg,aer,are,i+di)      
          ver(1)=(i.ge.dfloat(pdel)).and.(i.le.dfloat(pdel+npas))
          if (ver(1).eqv..true.) then
            sig3b_var(:)=sig3b_3d(:)
            sig4b_var(:)=sig4b_3d(:)
            ver(2)=(i.ge.(dfloat(ldel2)-t_limp_plus/2.d0)).and.(i.le.(dfloat(ldel2)+t_limp_plus/2.d0))
            if ((ver(2).eqv..true.).and.(onoff.eq.1)) then
              sig3b_var(:)=sig3b_3d_on(:)
              sig4b_var(:)=sig4b_3d_on(:)
            end if
            k_1=prod_phb(i,phb)
            k_2=prod_phb(i+di/2.d0,phb+di/2.d0*k_1)
            k_3=prod_phb(i+di/2.d0,phb+di/2.d0*k_2)
            k_4=prod_phb(i+di,phb+di*k_3)
            m_1=prod_phbp(i,phb)
            m_2=prod_phbp(i+di/2.d0,phb+di/2.d0*m_1)
            m_3=prod_phbp(i+di/2.d0,phb+di/2.d0*m_2)
            m_4=prod_phbp(i+di,phb+di*m_3)
            phb(:)=phb(:)+di/6.d0*(k_1(:)+2.d0*(k_2(:)+k_3(:))+k_4(:))
            phbp=phbp+di/6.d0*(m_1+2.d0*(m_2+m_3)+m_4)
            if (sim.eq."3b") cnvp(pdel,k)=sum(phb)*pshape(i)*di
            if (sim.eq."4b") cnvp(pdel,k)=phbp*pshape(i)*di
            k=k+1
          end if
          rhogg=rhogg+di/6.d0*(kgg_1+2.d0*(kgg_2+kgg_3)+kgg_4)
          rhoge=rhoge+di/6.d0*(kge_1+2.d0*(kge_2+kge_3)+kge_4)
          rhoeg=rhoeg+di/6.d0*(keg_1+2.d0*(keg_2+keg_3)+keg_4)
          rhoee=rhoee+di/6.d0*(kee_1+2.d0*(kee_2+kee_3)+kee_4)
          rhorr=rhorr+di/6.d0*(krr_1+2.d0*(krr_2+krr_3)+krr_4)
          rhogr=rhogr+di/6.d0*(kgr_1+2.d0*(kgr_2+kgr_3)+kgr_4)
          rhorg=rhorg+di/6.d0*(krg_1+2.d0*(krg_2+krg_3)+krg_4)
          rhoer=rhoer+di/6.d0*(ker_1+2.d0*(ker_2+ker_3)+ker_4)
          rhore=rhore+di/6.d0*(kre_1+2.d0*(kre_2+kre_3)+kre_4)
          i=i+di
          if (i.gt.dfloat(time)) var=1
        end do
        write(14,*) pdel-t_pimp,ldel,sum(cnvp(pdel,:))
      end do
      deallocate(cnvp)
      end do 
  end subroutine biraben
  
real*8 function prod_phb(i,tab)
      implicit real*8 (a-h,o-z)
      real*8     :: npbar,i
      complex*16 :: rhogg,rhoee,rhorr
      dimension prod_phb(1:nexch),tab(1:nexch)
      external xilas
      common/gr2/vpbar,psd,npbar,gam_d,rhogg,rhoee,rhorr
      prod_phb(:)=npbar*psd*vpbar*1.d-9*(sig3b_1s(:)*dreal(rhogg)+sig3b_var(:)*dreal(rhoee)+sig3b_5f(:)*dreal(rhorr))
      prod_phb(:)=prod_phb(:)+matmul(rad,tab)-gam_phH_mines(:)*xilas(1,i)-gam_phH_plus(:)*xilas(2,i)
      end function prod_phb 
  
real*8 function prod_phbp(i,tab)
      implicit real*8 (a-h,o-z)
      real*8     :: npbar,i
      complex*16 :: rhogg,rhoee,rhorr
      dimension tab(1:nexch),ox(1:nexch)
      external xilas
      common/gr2/vpbar,psd,npbar,gam_d,rhogg,rhoee,rhorr
      ox(:)=psd*vpbar*1.d-9*(sig4b_1s(:)*dreal(rhogg)+sig4b_var(:)*dreal(rhoee)+sig4b_5f(:)*dreal(rhorr))
      prod_phbp=dot_product(tab,ox)-gam_d*xilas(1,i)
      end function prod_phbp  
end module ipcms  

complex*16 function drv(numb,rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,i)
      implicit real*8 (a-h,o-z)
      integer*4  :: numb
      real*8     :: i
      complex*16 :: rhogg,rhoge,rhoeg,rhoee,rhorr,rhogr,rhorg,rhoer,rhore,ci,ox
      external xilas,prodPs
      common/gr4/tau,gam_e,gam_r,gam_phPs_e,gam_phPs_r,omega_plus,omega_mines,domega,ci
      dimension ox(1:2)
      ox(:)=dcmplx(0.d0,0.d0)   
      if (numb.eq.1) then 
        ox(1)=+xilas(1,i)*(ci*omega_mines/2.d0)*(rhoge-rhoeg)+gam_e*rhoee-rhogg/tau+prodPs(i)
      else if (numb.eq.2) then 
        ox(1)=+xilas(1,i)*(ci*omega_mines/2.d0)*(rhogg-rhoee)-rhoge*(1.d0/tau+gam_e)/2.d0
        ox(2)=+xilas(2,i)*(ci*omega_plus/2.d0)*rhogr
      else if (numb.eq.3) then 
        ox(1)=-xilas(1,i)*(ci*omega_mines/2.d0)*(rhogg-rhoee)-rhoeg*(1.d0/tau+gam_e)/2.d0
        ox(2)=-xilas(2,i)*(ci*omega_plus/2.d0)*rhorg
      else if (numb.eq.4) then 
        ox(1)=-xilas(1,i)*(ci*omega_mines/2.d0)*(rhoge-rhoeg)-gam_e*rhoee-xilas(1,i)*gam_phPs_e*rhoee
        ox(2)=+xilas(2,i)*(ci*omega_plus/2.d0)*(rhoer-rhore)+gam_r*rhorr
      else if (numb.eq.5) then  
        ox(1)=-xilas(2,i)*(ci*omega_plus/2.d0)*(rhoer-rhore)-gam_r*rhorr-xilas(2,i)*gam_phPs_r*rhorr
      else if (numb.eq.6) then  
        ox(1)=-(gam_r/2.d0+ci*domega*xilas(2,i))*rhogr-ci/2.d0*(xilas(1,i)*omega_mines*rhoer-xilas(2,i)*omega_plus*rhoge)
      else if (numb.eq.7) then 
        ox(1)=-(gam_r/2.d0-ci*domega*xilas(2,i))*rhorg+ci/2.d0*(xilas(1,i)*omega_mines*rhore-xilas(2,i)*omega_plus*rhoeg)
      else if (numb.eq.8) then 
        ox(1)=-xilas(1,i)*(ci*omega_mines/2.d0)*rhogr-((gam_e+gam_r)/2.d0+ci*domega*xilas(2,i))*rhoer
        ox(2)=-xilas(2,i)*(ci*omega_plus/2.d0)*(rhorr-rhoee)
      else if (numb.eq.9) then 
        ox(1)=+xilas(1,i)*(ci*omega_mines/2.d0)*rhorg-((gam_e+gam_r)/2.d0-ci*domega*xilas(2,i))*rhore
        ox(2)=+xilas(2,i)*(ci*omega_plus/2.d0)*(rhorr-rhoee)
      end if
      drv=sum(ox)
      end function drv

real*8 function pshape(t)
      implicit real*8(a-h,o-z)
      integer*4 :: pdel
      common/gr3/t_pimp,pdel
      pi = 4.d0*datan(1.d0)
      rms=t_pimp/(4.d0/3.d0*dsqrt(2.d0*dlog(2.d0)))
      xa=1.d0/(2.d0*rms**2.d0)
      xnorm=4.d0*(xa**1.5d0)/dsqrt(pi)
      pshape=0.d0
      pshape=xnorm*((t-(dfloat(pdel)-t_pimp))**2.d0)*dexp(-xa*(t-(dfloat(pdel)-t_pimp))**2.d0)
      return
      end
      
real*8 function prodPs(t)
      implicit real*8(a-h,o-z)
      common/positon/t_eimp
      pi=4.d0*datan(1.d0)
      rms=t_eimp/(4.d0/3.d0*dsqrt(2.d0*dlog(2.d0)))
      xa=1.d0/(2.d0*rms**2.d0)
      xnorm=4.d0*(xa**1.5d0)/dsqrt(pi)
      prodPs=0.d0
      if (t.ge.0) prodPs=xnorm*(t**2.d0)*dexp(-xa*(t**2.d0))
      return
      end
      
real*8 function xilas(trans,t)
      implicit real*8(a-h,o-z)
      integer*4 :: trans,onoff,ldel,ldel2,var
      common/gr1/t_limp_mines,t_limp_plus,onoff,ldel,ldel2
      pi=4.d0*datan(1.d0)  
      if (trans.eq.1) then
        t_limp=t_limp_mines; var=ldel
      else if (trans.eq.2) then 
        t_limp=t_limp_plus; var=ldel2
      end if  
      xcentr=dfloat(var)+t_limp/2.d0
      rms=t_limp/(2.d0*dsqrt(2.d0*dlog(2.d0)))
      xilas=t_limp*dexp(-0.5d0*(t-xcentr)**2.d0/rms**2.d0)/dsqrt(2.d0*pi*rms**2.d0)
      if ((onoff.eq.0).and.(trans.eq.2)) xilas=0.d0
      return
      end
    
real*8 function radial(state,z,input,n,l,r)
      implicit real*8 (a-h,o-z)
      character(len=2)  :: state
      character(len=4)  :: input
      integer*4         :: n,l
      intent(in) state,z,input,n,l,r
      dimension tab(1:4)
      if (state.eq."fs") then 
        a0=1.d0
      else if (state.eq."is") then 
        a0=2.d0
      end if  
      rho=2.d0*z*r/(a0*dfloat(n))
      tab(1)=1.d0/fact(2*l+1) 
      tab(2)=dsqrt((rho/r)**3.d0*fact(n+l)/(dfloat(2*n)*fact(n-l-1)))
      tab(3)=rho**dfloat(l)
      a=dfloat(l+1-n); b=dfloat(2*l+2)
      call chgm(a,b,rho,tab(4)) 
      if (input.eq."norm") radial=product(tab)
      if (input.eq."exp-") radial=product(tab)*dexp(-rho/2.d0)
  end function radial 

real*8 function fact(n)
      implicit none
      integer*4  :: n,i
      intent(in) n
      fact=1
      if (n.lt.2) return
        do i=1,n
        fact=i*fact
        end do
  end function fact 

double precision function d3j(a1,a2,a3,b1,b2,b3)
      implicit none
      real*8 wig3j
      integer*4 a1,a2,a3,b1,b2,b3
         call gamaf
      d3j=wig3j(dfloat(a1),dfloat(a2),dfloat(a3),dfloat(b1),dfloat(b2),dfloat(b3))
      end   
      
double precision function d3j30(l1,l2,l3)
      implicit real*8 (a-b,d-h,o-z)
      implicit complex*16 (c)
      ip=(l1+l2+l3)/2
      c1=dcmplx(dfloat(l1+l2-l3+1),0.d0)
      c2=dcmplx(dfloat(l2+l3-l1+1),0.d0)
      c3=dcmplx(dfloat(l3+l1-l2+1),0.d0)
      c4=dcmplx(dfloat(l1+l2+l3+2),0.d0)
      c5=dcmplx(dfloat(ip-l1+1),0.d0)
      c6=dcmplx(dfloat(ip-l2+1),0.d0)
      c7=dcmplx(dfloat(ip-l3+1),0.d0)
      c8=dcmplx(dfloat(ip+1))
      cldel=clgam(c1)+clgam(c2)+clgam(c3)-clgam(c4)
      cl3j=0.5d0*cldel+clgam(c8)-clgam(c5)-clgam(c6)-clgam(c7)
      d3j30=((-1.d0)**ip)*dreal(cdexp(cl3j))
      return
      end
    
complex*16 function clgam(x)
      implicit real*8 (a-h,o-z)
      complex*16  x,y,z,pz,pcz,zlg,gam,sub
      dimension q(11)
      data q/8.3333333333333d-02,-2.7777777777778d-03, 7.9365079365079d-04,  &
            & -5.9523809523810d-04, 8.4175084175084d-04,-1.9175269175269d-03,&
            & 6.4102564102564d-03,-2.9550653594771d-02, 1.7964437236883d-01, &
            &-1.3924322169059d0    , 13.4028640441684d0/
      data hlp /9.18938533204673d-1/
      data pi/3.14159265358979d0/
      ineg=0
      z=x
      if(dreal(z).lt.0.d0) then
      ineg=1
      pz=pi*z
      pcz=pi/cdsin(pz)
      sub=cdlog(pcz)
      z=1.d0-z
      endif
      gam=dcmplx(0.d0,0.d0)
 100  if (dreal(z).lt.6.d0) then
      if (dreal(z).eq.0.d0) then
      yy=dimag(z)
      if (yy.gt.0.d0) then
      zlg=dcmplx(dlog(yy),0.5d0*pi)
      else if (yy.lt.0.d0) then
      zlg=dcmplx(dlog(-yy),-0.5d0*pi)
      else
      stop 'argurment is O or negative integer'
      end if
      else
      zlg=cdlog(z)
      end if
      gam=gam-zlg
      z=z+1.d0
      go to 100
      end if
      gam=gam+(z-.5d0)*cdlog(z)-z+hlp
      y=z
      do 200 i=1,11
      y=y/(z*z)
      gam=gam+q(i)*y
 200  continue
      clgam=gam
      if (ineg.ne.0) then
      clgam= sub-gam
      end if
      return
      end      
       
double precision function plgndr(l,m,x)
      implicit real*8 (a-h,o-z)
      pmm=1.d0
      if (m.gt.0) then
      somx2=dsqrt((1.d0-x)*(1.d0+x))
      fact=1.d0
      do 11 i=1,m
      pmm=-pmm*fact*somx2
      fact=fact+2.d0
 11   continue
      endif
      if(l.eq.m) then
      plgndr=pmm
      else
      pmmp1=x*(2*m+1)*pmm
      if (l.eq.m+1) then
      plgndr=pmmp1
      else
      do 12 ll=m+2,l
      pll=(x*(2*ll-1)*pmmp1-(ll+m-1)*pmm)/(ll-m)
      pmm=pmmp1
      pmmp1=pll
 12   continue
      plgndr=pll
      end if
      end if
      return
      end   
      
double precision function ylm(xcost,l,m)
      implicit real*8 (a-b,d-h,o-z)
      implicit complex*16 (c)
      pi=4.0*datan(1.d0)
      if (l.eq.0) then
      if (m.ne.0) then
      ylm=0.d0
      return
      else
      ylm=1.d0/dsqrt(4.d0*pi)
      return
      end if
      end if
      if ((l-m).gt.10.or.(l+m).gt.10) then
        cap=dcmplx(l+m+1.0,0.0)
        cap=clgam(cap)
        cam=dcmplx(l-m+1.d0,0.d0)
        cam=clgam(cam)
        cad=cap-cam
        am=dreal(cdexp(0.5d0*cad))
        ap=dreal(cdexp(-0.5d0*cad))
        else
        am=dsqrt(fact(l+m)/fact(l-m))
        ap=dsqrt(fact(l-m)/fact(l+m))
      end if
      if (m.lt.0) then
      ylm=((-1.d0)**m)*plgndr(l,-m,xcost)*am*dsqrt((2.d0*l+1)/(4.d0*pi))
      else if (m.eq.0) then
      ylm=plgndr(l,0,xcost)*dsqrt((2.d0*l+1)/(4.d0*pi))
      else if (m.gt.0) then
      ylm=plgndr(l,m,xcost)*ap*dsqrt((2.d0*l+1)/(4.d0*pi))
      end if
      end     
      
 function lagint(xx, xi, yi, ni, n)
implicit none
double precision lagint, xx
integer ni, n
double precision xi(ni), yi(ni)
double precision lambda(ni)
integer i, j, k, js, jl
double precision y
if (n > ni) n = ni
if (xx <= xi(1)) then
  lagint = yi(1)
  return
end if
if (xx >= xi(ni)) then
  lagint = yi(ni)
  return
end if
i = 1
j = ni
do while (j > i+1)
  k = (i+j)/2
  if (xx < xi(k)) then
    j = k
    else
    i = k
  end if
end do
i = i + 1 - n/2
if (i < 1) i=1
if (i + n > ni) i=ni-n+1
y = 0.0
do js=i,i+n-1
  lambda(js)=1.0
  do jl=i,i+n-1
    if(jl /= js) lambda(js)=lambda(js)*(xx-xi(jl))/(xi(js)-xi(jl))
  end do
  y = y + yi(js)*lambda(js)
end do
lagint = y
end function lagint

function rint (f,na,nb,nq,h)
      implicit double precision(a-h,o-z)
      dimension c(105),c1(25),c2(80),d(14),f(nb)
      equivalence (c1(1),c(1)),(c2(1),c(26))
      data c1/1.d0,2.d0,1.d0,23.d0,28.d0,9.d0,25.d0,20.d0,31.d0,8.d0,&
      1413.d0,1586.d0,1104.d0,1902.d0,475.d0,1456.d0,1333.d0,1746.d0,&
      944.d0,1982.d0,459.d0,119585.d0,130936.d0,89437.d0,177984.d0/
      data c2/54851.d0,176648.d0,36799.d0,122175.d0,111080.d0,156451.d0,&
      46912.d0,220509.d0,29336.d0,185153.d0,35584.d0,7200319.d0,&
      7783754.d0,5095890.d0,12489922.d0,-1020160.d0,16263486.d0,&
      261166.d0,11532470.d0,2082753.d0,7305728.d0,6767167.d0,9516362.d0,&
      1053138.d0,18554050.d0,-7084288.d0,20306238.d0,-1471442.d0,&
      11965622.d0,2034625.d0,952327935.d0,1021256716.d0,636547389.d0,&
      1942518504.d0,-1065220914.d0,3897945600.d0,-2145575886.d0,&
      3373884696.d0,-454944189.d0,1637546484.d0,262747265.d0,&
      963053825.d0,896771060.d0,1299041091.d0,-196805736.d0,&
      3609224754.d0,-3398609664.d0,6231334350.d0,-3812282136.d0,&
      4207237821.d0,-732728564.d0,1693103359.d0,257696640.d0,&
      5206230892907.d0,5551687979302.d0,3283609164916.d0,&
      12465244770050.d0,-13155015007785.d0,39022895874876.d0,&
      -41078125154304.d0,53315213499588.d0,-32865015189975.d0,&
      28323664941310.d0,-5605325192308.d0,9535909891802.d0,&
      1382741929621.d0,5252701747968.d0,4920175305323.d0,&
      7268021504806.d0,-3009613761932.d0,28198302087170.d0,&
      -41474518178601.d0,76782233435964.d0,-78837462715392.d0,&
      81634716670404.d0,-48598072507095.d0,34616887868158.d0,&
      -7321658717812.d0,9821965479386.d0,1360737653653.d0/
      data d/2.d0,2.d0,24.d0,24.d0,1440.d0,1440.d0,120960.d0,120960.d0,&
      7257600.d0,7257600.d0,958003200.d0,958003200.d0,5230697472000.d0,&
      5230697472000.d0/
      a=0.0
      l=na
      m=nb
      i=nq*(nq+1)/2
      do 1 j=1,nq
      a=a+c(i)*(f(l)+f(m))
      l=l+1
      m=m-1
    1 i=i-1
      a=a/d(nq)
      do 2 n=l,m
    2 a=a+f(n)
      rint=a*h
      return
      end



