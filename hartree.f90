module lcpmr
use ipcms
use hyp_2f1_module
use incomplete_gamma
implicit none
real*8                                    :: cstr,eo
integer                                   :: twork
integer*4                                 :: Zc,nbas,nspin,ivar,ivar_max,orb_max,nbas_mpi
integer*4, dimension(:), allocatable      :: nbas_orb
integer*4, dimension(:,:), allocatable    :: nr,ds
integer*4, dimension(:,:,:), allocatable  :: mr
real*8, dimension(:), allocatable         :: cmd_1,cmd_2,e_corr,eplus,rho
real*8, dimension(:,:), allocatable       :: turn_ps,turn_ms,sovr,sovr_bis,fck,cplus,vp,cwr
real*8, dimension(:,:), allocatable       :: fck_plus,emines,eg_hf,eg_cisd
real*8, dimension(:,:), allocatable       :: wf
real*8, dimension(:,:,:), allocatable     :: hcor,cmines,fck_mines,ar,dr,qmines
character(len=20)                         :: str,bas
character(len=1)                          :: positron,data_3b
character(len=3)                          :: expand
character(len=6)                          :: starter
character(50)                             :: kstr
dimension cstr(0:2),nspin(1:2)
common/GTO_para/cstr,str,ivar,Zc
  contains
  
subroutine master_hf
    implicit real*8 (a-h,o-z)
    open(1,file="entr_1",status='unknown')
    read(1,*) str
    read(1,*) bas
    read(1,*) expand
    read(1,*) data_3b
    close(1)
    if (rank.eq.0) then
      open(unit=16,file="info",form="formatted")
      write(16,*) "ATOM:"," ",str
      write(16,*) "BASIS:"," ",bas
      write(16,*) "EXPANSION:"," ",expand
      write(16,*) "----------------------"
    end if  
    call data_bas(0)  
    call data_bas(1)  
    call scf_loop
    call data_bas(2) 
    call data_bas(3)
  end subroutine master_hf
  
subroutine atom_str
    positron="n"
    if (str.eq."H") then 
      Zc=1; nspin(1)=1; nspin(2)=0
    else if (str.eq."Ps") then 
      Zc=0; nspin(1)=1; nspin(2)=0; positron="o"
    else if (str.eq."He") then 
      Zc=2; nspin(:)=1
    else if (str.eq."Li") then 
      Zc=3; nspin(1)=2; nspin(2)=1 
    else if (str.eq."Be") then 
      Zc=4; nspin(:)=2
    else if (str.eq."B") then 
      Zc=5; nspin(1)=3; nspin(2)=2 
    else if (str.eq."Ne") then 
      Zc=10; nspin(:)=5
    else if (str.eq."Cl") then 
      Zc=17; nspin(1)=9; nspin(2)=8
    else if (str.eq."Ar") then 
      Zc=18; nspin(:)=9
    else if (str.eq."Sc") then 
      Zc=21; nspin(1)=11; nspin(2)=10
    else if (str.eq."Kr") then 
      Zc=36; nspin(:)=18
    else if (str.eq."Xe") then 
      Zc=54; nspin(:)=27
    else if (str.eq."HPs") then
      Zc=1; nspin(:)=1; positron="o"
    else if (str.eq."LiPs") then 
      Zc=3; nspin(:)=2; positron="o"
    else if (str.eq."PsF") then 
      Zc=9; nspin(:)=5; positron="o"
    else if (str.eq."PsCl") then 
      Zc=17; nspin(:)=9; positron="o"
    end if
end subroutine atom_str
  
subroutine data_bas(mode)
    implicit real*8 (a-h,o-z)
    integer                            :: nx,count_4,count_5
    integer*4                          :: x,k,r,i,s,q,mode,orb,ctr,momentum,m,ngrid
    integer*4                          :: x_mpi,no,id,id_max
    integer*4                          :: km,im,incr,count_1,count_2,count_3
    integer*4                          :: imod,imod_max
    real*8                             :: numb
    character(len=1)                   :: basis
    character(len=20)                  :: str_bas
    real*8, dimension(:), allocatable  :: tab_bis,comp_bis
    parameter fmt2='(i5,x,f13.6,x,f13.6)',fmt3='(i5,x,f13.6)',fmt4='(" EHF [ua]:",f13.6)'
    intent(in) mode
    external radial
    dimension tab(1:5),dens(1:2),dens_mod(1:2),tab_avr(1:5),dens_avr(1:2)
    dimension comp(1:2),a(1),b(1),otp(1:2)
    call mpi_comm_size(mpi_comm_world,nproc,ierr) 
    call mpi_comm_rank(mpi_comm_world,rank,ierr)
    if (mode.eq.0) then 
      !Normalization coefficients
      cstr(0)=dsqrt(4.d0*pi)
      cstr(1)=4.d0*pi/dsqrt(6.d0)
      cstr(2)=(4.d0*pi)**(3.d0/2.d0)/(2.d0*dsqrt(15.d0))
      !Atomic parameters
      call atom_str
      !Atomic basis
      str_bas=trim(str)//trim(bas)
      allocate(nbas_orb(1:3)); nbas_orb(:)=0
      if (rank.eq.0) then
        nx=0
        open(unit=1,file=trim(path(1))//"/basis/"//trim(str_bas),form="formatted",iostat=ierr)
        do while (ierr==0)
          read(1,*,iostat=ierr)
          if (ierr==0) nx=nx+1
        end do
        close(1)
        i=1
        open(unit=2,file=trim(path(1))//"/basis/"//trim(str_bas),form="formatted"); open(unit=3,file="cmd2",form="formatted")
        do while (i.le.nx)
          read(2,*) basis,ctr
          if (basis.eq."S") then 
            momentum=0; nbas_orb(1)=nbas_orb(1)+1
          else if (basis.eq."P") then 
            momentum=1; nbas_orb(2)=nbas_orb(2)+1
          else if (basis.eq."D") then 
            momentum=2; nbas_orb(3)=nbas_orb(3)+1
          end if
          allocate(cmd_1(ctr),cmd_2(ctr))
          do q=1,ctr
            read(2,*) x,cmd_1(q),cmd_2(q)
          end do
          do m=-momentum,momentum
            write(3,*) basis,ctr
            do q=1,ctr
              write(3,*) cmd_1(q),cmd_2(q),m
            end do
          end do
          deallocate(cmd_1,cmd_2)
          i=i+1+ctr
        end do
        close(2); close(3)
        nbas_orb(2)=3*nbas_orb(2); nbas_orb(3)=5*nbas_orb(3)
        do iproc=1,nproc-1
          call mpi_send(nbas_orb,3,mpi_int,iproc,1,mpi_comm_world,ierr)
        end do
      end if
      do iproc=1,nproc-1
        if (iproc.eq.rank) call mpi_recv(nbas_orb,3,mpi_int,0,1,mpi_comm_world,mpi_status_ignore,ierr)
      end do  
      call mpi_barrier(mpi_comm_world,ierr)
      if (expand.eq."S-S") then
        orb_max=1; nbas=nbas_orb(1)
      else if (expand.eq."S-P") then 
        orb_max=2; nbas=nbas_orb(1)+nbas_orb(2)
      else if (expand.eq."S-D") then   
        orb_max=3; nbas=nbas_orb(1)+nbas_orb(2)+nbas_orb(3)
      end if  
      allocate(nr(1:orb_max,maxval(nbas_orb))); nr(:,:)=0.d0
      do iproc=0,nproc-1
        if (iproc.eq.rank) then
          nx=0
          open(unit=4,file="cmd2",form="formatted",iostat=ierr)
          do while (ierr==0)
            read(4,*,iostat=ierr)
            if (ierr==0) nx=nx+1
          end do
          close(4)
          count_1=1; count_2=1; count_3=1; i=1
          open(unit=10,file="cmd2",form="formatted")
          do while (i.le.nx)
            read(10,*) basis,ctr
            if ((orb_max.ge.1).and.(basis.eq."S")) then
              nr(1,count_1)=ctr; count_1=count_1+1
            else if ((orb_max.ge.2).and.(basis.eq."P")) then
              nr(2,count_2)=ctr; count_2=count_2+1
            else if ((orb_max.ge.3).and.(basis.eq."D")) then 
              nr(3,count_3)=ctr; count_3=count_3+1
            end if  
            do q=1,ctr
              read(10,*) 
            end do
            i=i+1+ctr
          end do
          close(10)
        end if
      end do  
      call mpi_barrier(mpi_comm_world,ierr)
    else if (mode.eq.1) then 
      allocate(turn_ps(nbas,nbas),turn_ms(nbas,nbas),sovr(nbas,nbas),sovr_bis(nbas,nbas),hcor(1:2,nbas,nbas),fck(nbas,nbas))
      allocate(emines(1:2,nbas),fck_mines(1:2,nbas,nbas),cmines(1:2,nbas,maxval(nspin)),qmines(1:2,nbas,maxval(nspin)))
      allocate(ar(0:orb_max-1,maxval(nbas_orb),maxval(nr)))
      allocate(dr(0:orb_max-1,maxval(nbas_orb),maxval(nr)))
      allocate(mr(0:orb_max-1,maxval(nbas_orb),maxval(nr)))
      ar(:,:,:)=0.d0; dr(:,:,:)=0.d0; mr(:,:,:)=0.d0
      do iproc=0,nproc-1
        if (iproc.eq.rank) then
          open(unit=11,file="cmd2",form="formatted")
          do orb=1,orb_max; do i=1,nbas_orb(orb)
            read(11,*)
            do q=1,nr(orb,i)
              read(11,*) ar(orb-1,i,q),dr(orb-1,i,q),mr(orb-1,i,q)
            end do
          end do; end do
          close(11)
        end if
      end do
      call mpi_barrier(mpi_comm_world,ierr)
      ivar_max=1
      if (positron.eq."o") then 
        ivar_max=2; allocate(fck_plus(nbas,nbas),cplus(nbas,nbas),eplus(nbas))
      end if  
    else if (mode.eq.2) then
      if (rank.eq.0) then 
        write(16,*) "WORK [s]:"," ",twork
        numb=0.d0
        do k=1,nbas; do i=1,nbas
           comp(1)=dot_product(cmines(1,k,:),cmines(1,i,:))
           comp(2)=dot_product(cmines(2,k,:),cmines(2,i,:))
           dens(1)=sum(comp); dens(2)=sovr_bis(k,i)
           numb=numb+product(dens)
        end do; end do
        open(unit=12,file="egv-",form="formatted")
        do i=1,nbas
          write(12,fmt2) i,emines(1,i),emines(2,i)
        end do
        close(12)
        if (positron.eq."o") then
          open(unit=13,file="egv+",form="formatted")
          do i=1,nbas
            write(13,fmt3) i,eplus(i)
          end do  
          close(13)
        end if 
      end if  
      if (data_3b.eq."o") then 
        call seconds(count_4)
        imod_max=1; id_max=4; hp=1.d-3
        if (positron.eq."o") then 
          ngrid=100000
        else if (positron.eq."n") then
          ngrid=40000
          imod_max=imod_max+1
          id_max=id_max+1
        end if  
        no=ngrid/nproc; nx=mod(ngrid,nproc)
        do iproc=0,nx-1
          if (iproc.eq.rank) no=no+1
        end do
        allocate(hx_in(0:id_max,1:ngrid),hx_ox(0:id_max,1:ngrid))
        hx_in(:,:)=0.d0; hx_ox(:,:)=0.d0
        allocate(wf(ngrid,nbas),vp(1:imod_max,ngrid),rho(ngrid),cwr(1:2,nbas))
        do x_mpi=1,no; x=(x_mpi-1)*nproc+rank+1
          vp(:,x)=0.d0; rho(x)=0.d0
          do orb=1,orb_max; call disp(orb,incr)
            do k=1,nbas_orb(orb); km=k+incr
              wf(x,km)=0.d0
              do r=1,nr(orb,k)
                tab(1)=cstr(orb-1)
                tab(2)=(2.d0*ar(orb-1,k,r)/pi)**(3.d0/4.d0+dfloat(orb-1)/2.d0)
                tab(3)=dr(orb-1,k,r)
                tab(4)=(dfloat(x)*hp)**dfloat(orb-1)
                tab(5)=dexp(-ar(orb-1,k,r)*(dfloat(x)*hp)**2.d0)
                wf(x,km)=wf(x,km)+product(tab)
              end do               
              do i=1,nbas_orb(orb); im=i+incr
                comp(1)=dot_product(cmines(1,km,:),cmines(1,im,:))
                comp(2)=dot_product(cmines(2,km,:),cmines(2,im,:))
                dens(1)=sum(comp)
                dens(2)=0.d0; dens_avr=dens
                do r=1,nr(orb,k); do s=1,nr(orb,i)
                  if (mr(orb-1,k,r).eq.mr(orb-1,i,s)) then
                    tab(:)=0.d0; tab_avr=tab
                    tab(1)=cstr(orb-1)*(2.d0*ar(orb-1,k,r)/pi)**(3.d0/4.d0+dfloat(orb-1)/2.d0)
                    tab(2)=cstr(orb-1)*(2.d0*ar(orb-1,i,s)/pi)**(3.d0/4.d0+dfloat(orb-1)/2.d0)
                    tab(3)=dr(orb-1,k,r)
                    tab(4)=dr(orb-1,i,s)
                    tab_avr=tab
                    a(1)=2.d0*dfloat(orb)
                    b(1)=ar(orb-1,k,r)+ar(orb-1,i,s)
                    comp(1)=(dfloat(x)*hp)**a(1)
                    comp(2)=dexp(-b(1)*(dfloat(x)*hp)**2.d0)
                    tab_avr(5)=product(comp)
                    dens_avr(2)=dens_avr(2)+product(tab_avr)
                    !---------------------------------------------------------!
                    allocate(comp_bis(1:2))
                    r_inp=b(1)*(dfloat(x)*hp)**2.d0
                    a_inp=(1.d0+a(1))/2.d0
                    allocate(tab_bis(1:3))
                    tab_bis(1)=(dfloat(x)*hp)**a(1)
                    tab_bis(2)=r_inp**(-a_inp)
                    call ingam(a_inp,0.d0,ans_1); call ingam(a_inp,r_inp,ans_2)
                    tab_bis(3)=ans_1-ans_2
                    comp_bis(1)=product(tab_bis)
                    deallocate(tab_bis)
                    a_inp=a(1)/2.d0
                    allocate(tab_bis(1:2))
                    tab_bis(1)=b(1)**(-a_inp)
                    call ingam(a_inp,r_inp,ans)
                    tab_bis(2)=ans
                    comp_bis(2)=product(tab_bis)
                    deallocate(tab_bis)
                    tab(5)=sum(comp_bis)/2.d0
                    deallocate(comp_bis)
                    !---------------------------------------------------------!
                    dens(2)=dens(2)+product(tab)
                  end if
                end do; end do 
                dens_mod(2)=dens(2)
                do imod=1,imod_max
                  if (positron.eq."o") then
                    comp(:)=0.d0
                  else if (positron.eq."n") then
                    comp(1)=cmines(1,km,imod); comp(2)=cmines(1,im,imod)
                  end if  
                  dens_mod(1)=dens(1)-product(comp)
                  vp(imod,x)=vp(imod,x)+product(dens_mod)
                end do
                rho(x)=rho(x)+product(dens_avr)
              end do
            end do
          end do 
          comp(1)=dfloat(x)*hp 
          if (positron.eq."o") then
            cwr(1,:)=cplus(:,1); cwr(2,:)=cplus(:,5)
          else if (positron.eq."n") then
            cwr(1,:)=cmines(1,:,1); cwr(2,:)=cmines(1,:,2)
          end if
          comp(2)=-dot_product(cwr(1,:),wf(x,:)); otp(1)=product(comp)
          comp(2)=-dot_product(cwr(2,:),wf(x,:)); otp(2)=product(comp)   
          !---------------------------------- Output ----------------------------------!
          hx_in(0,x)=comp(1)                         !Coordinates r
          hx_in(1,x)=rho(x)                          !r²ρ(r)
          hx_in(2,x)=otp(1)                          !(1s,1s+) radial HF wave function
          hx_in(3,x)=otp(2)                          !(2s,2s+) radial HF wave function
          hx_in(4,x)=vp(1,x)                         !Coulomb repulsion
          if (positron.eq."n") hx_in(5,x)=vp(2,x)
          !----------------------------------------------------------------------------!
        end do
        deallocate(wf,vp,rho,cwr)
        call mpi_barrier(mpi_comm_world,ierr)
        do id=0,id_max
          call mpi_reduce(hx_in(id,:),hx_ox(id,:),ngrid,mpi_real8,mpi_sum,0,mpi_comm_world,ierr) 
        end do
        call seconds(count_5)
        if (rank.eq.0) then
          open(unit=14,file=trim("fo")//trim(str),form="formatted")
          open(unit=15,file=trim("vp")//trim(str),form="formatted")
          do x=1,ngrid
           write(10,*) hx_ox(0,x),hx_ox(1,x) 
           write(14,*) hx_ox(0,x),hx_ox(2,x),hx_ox(3,x)
           if (positron.eq."o") write(15,*) hx_ox(0,x),hx_ox(4,x)
           if (positron.eq."n") write(15,*) hx_ox(0,x),hx_ox(4,x),hx_ox(5,x)
          end do 
          close(14); close(15)
          write(16,*) "DATA [s]:"," ",count_5-count_4
        end if 
        deallocate(hx_in,hx_ox)
      end if  
      if (rank.eq.0) then 
        write(16,*) "DENS_ELC:"," ",idnint(numb)
        write(16,fmt4) eo
        close(16)
      end if  
    else if (mode.eq.3) then
      deallocate(ar,dr,mr)
      deallocate(nbas_orb,nr)      
      deallocate(turn_ps,turn_ms,sovr,sovr_bis,hcor,fck)
      deallocate(emines,fck_mines,cmines,qmines)   
      if (positron.eq."o") deallocate(fck_plus,cplus,eplus)
    end if
  end subroutine data_bas
                      
subroutine data_mendeleiev(input)
    implicit real*8 (a-h,o-z)
    integer*4         :: kmax,k,q,input
    character(len=20) :: emt
    intent(in) input
    parameter (exponents=3.d0)
    dimension emt(2)
    if (input.eq.0) then
      open(unit=1,file=trim(path(3))//trim(target),form="formatted",iostat=ierr)
      kmax=0
      do while (ierr==0)
        read(1,*,iostat=ierr)
        if (ierr==0) kmax=kmax+1
      end do
      close(1)
      allocate(cmd_1(kmax),cmd_2(kmax),e_corr(kmax))
      open(unit=2,file=trim(path(3))//trim(target),form="formatted",iostat=ierr)
      do k=1,kmax
        read(2,*) cmd_1(k),cmd_2(k)
      end do
      close(2)
      e_corr(:)=cmd_2(:)-cmd_1(:)
      open(unit=3,file=trim(path(3))//trim(target)//"-INFO",form="formatted",iostat=ierr)
      do k=1,kmax
        write(3,*) cmd_1(k),cmd_2(k),e_corr(k)
      end do
      close(3)
      open(unit=4,file=trim(path(3))//trim(target)//"-INFO2",form="formatted",iostat=ierr)
      do k=2,kmax
        e_corr_lim=e_corr(k)*dfloat(k+1)**exponents-e_corr(k-1)*dfloat(k)**exponents
        e_corr_lim=e_corr_lim/(dfloat(k+1)**exponents-dfloat(k)**exponents)
        write(4,100) k+1,e_corr_lim
      end do
      100 format (I5,E25.12)
      close(4)
      deallocate(cmd_1,cmd_2,e_corr)
    else if (input.eq.1) then
      emt(1)="PsCl"
      emt(2)="Cl-"
      open(unit=1,file=trim(path(3))//trim(emt(1)),form="formatted",iostat=ierr)
      kmax=0
      do while (ierr==0)
        read(1,*,iostat=ierr)
        if (ierr==0) kmax=kmax+1
      end do
      close(1)
      allocate(eg_hf(kmax,2),eg_cisd(kmax,2))
      do q=1,2
        open(unit=2,file=trim(path(3))//trim(emt(q)),form="formatted",iostat=ierr)
        do k=1,kmax
          read(2,*) eg_hf(k,q),eg_cisd(k,q)
        end do  
        close(2)
      end do
      open(unit=3,file=trim(path(3))//trim(emt(1))//"-"//trim(emt(2))//"-DIFF",form="formatted",iostat=ierr)
        do k=1,kmax
          write(3,101) k+1,eg_hf(k,1)-eg_hf(k,2),eg_cisd(k,1)-eg_cisd(k,2)
        end do
        101 format (I5,E25.12,E25.12)
      close(3)
      deallocate(eg_hf,eg_cisd)
    end if
  end subroutine data_mendeleiev
  
subroutine ingam(a,x,ans_mod)
    implicit real*8 (a-h,o-z)
    integer     :: ind
    complex*16  :: a_cmp,clgam
    intent(in) a,x
    intent(out) ans_mod
    dimension tab(1:2)
    external clgam
    call gratio(a,x,ans,qans,ind); tab(1)=qans
    a_cmp=dcmplx(a,0.d0); tab(2)=dreal(cdexp(clgam(a_cmp)))
    ans_mod=product(tab)
  end subroutine ingam
  
subroutine disp(orb,incr)
    implicit integer*4 (a-z)
    intent(in) orb
    intent(out) incr
    if (orb.eq.1) incr=0
    if (orb.eq.2) incr=nbas_orb(1)
    if (orb.eq.3) incr=nbas_orb(1)+nbas_orb(2)
  end subroutine disp   

subroutine scf_loop
    implicit real*8 (a-h,o-z)
    integer                                :: nx,count_1,count_2,count_3
    integer*4                              :: i,k,j,r,s,l,m,p,q,id,id_max,status
    integer*4                              :: idex,idex_orb,incr,orb,x,x_mpi,no
    integer*4                              :: l1,l2,l3,l4,li,m1,m2,m3,m4,km,im
    integer*4                              :: incr1,orb1,incr2,orb2,incr3,orb3,incr4,orb4
    real*8                                 :: jk2
    logical                                :: ver
    real*8, dimension(:), allocatable      :: itr
    real*8, dimension(:,:,:), allocatable  :: cntr
    data eps/1.d-6/
    dimension egr(1:2),idex(1:2),idex_orb(1:4)
    dimension ver(1:2),tab(1:2),dens(1:2),tab_bis(1),dens_bis(1)
    dimension a(1:2),b(1:2),ox(1:4),rps(1:2)
    dimension computation(1:2),comp(1:4)
    dimension fck_ini(nbas,nbas),e(nbas),d(nbas)
    common/GTO_orb/orb
    external d3j
    call seconds(count_1)
    !Computation of the one-electron hamiltonian
    sovr(:,:)=0.d0; hcor(:,:,:)=0.d0
    do orb=1,orb_max
      call disp(orb,incr)
      do k=1,nbas_orb(orb); do i=1,nbas_orb(orb)
        km=k+incr; im=i+incr
        do r=1,nr(orb,k); do s=1,nr(orb,i)
          if (mr(orb-1,k,r).eq.mr(orb-1,i,s)) then
            sovr(km,im)=sovr(km,im)+GTO("overlap",k,i,r,s)
            do ivar=1,ivar_max
              hcor(ivar,km,im)=hcor(ivar,km,im)+GTO("one-elc",k,i,r,s)
            end do
          end if
        end do; end do 
      end do; end do
    end do  
    sovr_bis=sovr
    !Computation of the rotation matrix
    call lowdin
    nbas_mpi=int(dfloat(nbas*(nbas+1))/2.d0)
    nbas_mpi=nbas_mpi*(nbas)**2
    if (rank.eq.0) write(16,*) "NBAS_MPI:"," ",nbas_mpi
    !MPI linearization 
    allocate(ds(1:12,1:nbas_mpi))
    ds(:,:)=0; x=1
    do orb1=1,orb_max; do orb2=1,orb_max; call disp(orb1,incr1); call disp(orb2,incr2)
      do k=1,nbas_orb(orb1); do i=1,nbas_orb(orb2); if ((k+incr1).le.(i+incr2)) then
        do orb3=1,orb_max; do orb4=1,orb_max; call disp(orb3,incr3); call disp(orb4,incr4)
          do l=1,nbas_orb(orb3); do m=1,nbas_orb(orb4)
            ds(1,x)=orb1
            ds(2,x)=orb2
            ds(3,x)=incr1
            ds(4,x)=incr2
            ds(5,x)=k
            ds(6,x)=i
            ds(7,x)=orb3
            ds(8,x)=orb4
            ds(9,x)=incr3
            ds(10,x)=incr4
            ds(11,x)=l
            ds(12,x)=m
            x=x+1
          end do; end do
        end do; end do
      end if; end do; end do
    end do; end do 
    call mpi_comm_size(mpi_comm_world,nproc,ierr) 
    call mpi_comm_rank(mpi_comm_world,rank,ierr)
    no=nbas_mpi/nproc; nx=mod(nbas_mpi,nproc)
    do iproc=0,nx-1
      if (iproc.eq.rank) no=no+1
    end do
    write(kstr,'(i1)') rank    
    !Computation of bielectronic integrals
    open(unit=1,file=trim(kstr)//trim(".dat"),form="unformatted")
    do x_mpi=1,no; x=(x_mpi-1)*nproc+rank+1
      !-------------------------------------------------------------------------!
      do r=1,nr(ds(1,x),ds(5,x)); do p=1,nr(ds(7,x),ds(11,x))
        idex_orb(3)=ds(1,x); idex_orb(4)=ds(7,x) 
        l1=idex_orb(3)-1; m1=mr(l1,ds(5,x),r)
        l3=idex_orb(4)-1; m3=mr(l3,ds(11,x),p)
        do id=1,2
          if (id.eq.1) then 
            idex_orb(1)=ds(2,x); idex(1)=ds(6,x)
            idex_orb(2)=ds(8,x); idex(2)=ds(12,x)
          else if (id.eq.2) then
            idex_orb(1)=ds(8,x); idex(1)=ds(12,x)
            idex_orb(2)=ds(2,x); idex(2)=ds(6,x)
          end if 
          do s=1,nr(idex_orb(1),idex(1)); do q=1,nr(idex_orb(2),idex(2))
            l2=idex_orb(1)-1; m2=mr(l2,idex(1),s)
            l4=idex_orb(2)-1; m4=mr(l4,idex(2),q)
            ver(1)=mod((l1+l2-l3-l4),2).eq.0
            ver(2)=(m2-m1).eq.(m3-m4)
            if ((ver(1).eqv..true.).and.(ver(2).eqv..true.)) then    
              a(1)=dfloat(2+l1+l2); b(1)=ar(l1,ds(5,x),r)+ar(l2,idex(1),s)
              a(2)=dfloat(2+l3+l4); b(2)=ar(l3,ds(11,x),p)+ar(l4,idex(2),q)
              do li=max0(iabs(l1-l2),iabs(l3-l4)),min0(l1+l2,l3+l4)
                if ((mod((l1+l2+li),2).eq.0).and.(mod((l3+l4+li),2).eq.0)) then
                  call GTO2(li,a,b,jk2); write(1) jk2
                end if
              end do 
            end if
          end do; end do
        end do 
      end do; end do 
      !-----------------------------------------------------------------------!
    end do   
    close(1)
    call mpi_barrier(mpi_comm_world,ierr)
    call seconds(count_2)
    if (rank.eq.0) write(16,*) "J-K2 [s]:"," ",count_2-count_1
    !One-electron Guess from the core Hamiltonian
    qmines(:,:,:)=0.d0
    id_max=2; if (positron.eq."o") id_max=3
    do id=1,id_max
      if (id.le.2) fck_ini(:,:)=hcor(1,:,:)
      if (id.eq.3) fck_ini(:,:)=hcor(2,:,:)
      fck_ini=matmul(turn_ms,matmul(fck_ini,turn_ms))
      call tred2(fck_ini,nbas,nbas,d,e); call tqli(d,e,nbas,nbas,fck_ini)
      call eigsrt(d,fck_ini,nbas,nbas)
      if (id.le.2) then 
        do i=1,nspin(id)
          cmines(id,:,i)=matmul(turn_ms,fck_ini(:,i))
        end do  
      else if (id.eq.3) then 
        do i=1,nbas
          cplus(:,i)=matmul(turn_ms,fck_ini(:,i))
        end do  
      end if
    end do
    !SCF 
    status=0
    do while (status.eq.0)
      fck_mines(:,:,:)=0.d0
      if (positron.eq."o") fck_plus(:,:)=0.d0
      allocate(hx_in(1:id_max,1:nbas_mpi),hx_ox(1:id_max,1:nbas_mpi))
      hx_in(:,:)=0.d0; hx_ox(:,:)=0.d0
      !Upper triangular fock matrices
      open(unit=2,file=trim(kstr)//trim(".dat"),form="unformatted")
      do x_mpi=1,no; x=(x_mpi-1)*nproc+rank+1
        !---------------------------------------------------------------------------------------!
        tab(:)=0.d0; dens(:)=0.d0; tab_bis(1)=0.d0; dens_bis(1)=0.d0
        do r=1,nr(ds(1,x),ds(5,x)); do p=1,nr(ds(7,x),ds(11,x))
          idex_orb(3)=ds(1,x); idex_orb(4)=ds(7,x) 
          l1=idex_orb(3)-1; m1=mr(l1,ds(5,x),r)
          l3=idex_orb(4)-1; m3=mr(l3,ds(11,x),p)
          do id=1,2
            if (id.eq.1) then 
              idex_orb(1)=ds(2,x); idex(1)=ds(6,x)
              idex_orb(2)=ds(8,x); idex(2)=ds(12,x)
            else if (id.eq.2) then
              idex_orb(1)=ds(8,x); idex(1)=ds(12,x)
              idex_orb(2)=ds(2,x); idex(2)=ds(6,x)
            end if 
            do s=1,nr(idex_orb(1),idex(1)); do q=1,nr(idex_orb(2),idex(2))
              l2=idex_orb(1)-1; m2=mr(l2,idex(1),s)
              l4=idex_orb(2)-1; m4=mr(l4,idex(2),q)
              ver(1)=mod((l1+l2-l3-l4),2).eq.0
              ver(2)=(m2-m1).eq.(m3-m4)
              if ((ver(1).eqv..true.).and.(ver(2).eqv..true.)) then   
                allocate(itr(1:4))
                itr(1)=dr(l1,ds(5,x),r)
                itr(2)=dr(l2,idex(1),s)
                itr(3)=dr(l3,ds(11,x),p)
                itr(4)=dr(l4,idex(2),q)
                ox(1)=product(itr)
                itr(1)=cstr(l1)*(2.d0*ar(l1,ds(5,x),r)/pi)**(3.d0/4.d0+dfloat(l1)/2.d0)
                itr(2)=cstr(l2)*(2.d0*ar(l2,idex(1),s)/pi)**(3.d0/4.d0+dfloat(l2)/2.d0)
                itr(3)=cstr(l3)*(2.d0*ar(l3,ds(11,x),p)/pi)**(3.d0/4.d0+dfloat(l3)/2.d0)
                itr(4)=cstr(l4)*(2.d0*ar(l4,idex(2),q)/pi)**(3.d0/4.d0+dfloat(l4)/2.d0)
                ox(2)=product(itr)
                itr(1)=dfloat(2*l1+1)
                itr(2)=dfloat(2*l2+1)
                itr(3)=dfloat(2*l3+1)
                itr(4)=dfloat(2*l4+1)
                ox(3)=dsqrt(product(itr))
                deallocate(itr)
                ox(4)=0.d0
                do li=max0(iabs(l1-l2),iabs(l3-l4)),min0(l1+l2,l3+l4)
                  if ((mod((l1+l2+li),2).eq.0).and.(mod((l3+l4+li),2).eq.0)) then
                    read(2) rps(1)
                    rps(2)=0.d0
                    allocate(itr(1:5))
                    itr(1)=d3j(l1,l2,li,0,0,0)
                    itr(2)=d3j(l3,l4,li,0,0,0)
                    itr(3)=(-1.d0)**(dfloat(m2+m3))
                    itr(4)=d3j(l1,l2,li,-m1,m2,m1-m2)
                    itr(5)=d3j(l3,l4,li,-m3,m4,m3-m4)
                    rps(2)=rps(2)+product(itr)
                    deallocate(itr)
                    ox(4)=ox(4)+product(rps)
                  end if
                end do
                jk2=product(ox)
                if (id.eq.1) then
                  tab(1)=tab(1)+jk2
                  if (positron.eq."o") tab_bis(1)=tab_bis(1)-jk2
                end if  
                if (id.eq.2) tab(2)=tab(2)-jk2
              end if
            end do; end do
          end do 
        end do; end do
        do id=1,id_max
          if (id.le.2) then
            comp(:)=0.d0
            comp(1)=0.8*dot_product(cmines(1,ds(11,x)+ds(9,x),:),cmines(1,ds(12,x)+ds(10,x),:))
            comp(2)=0.8*dot_product(cmines(2,ds(11,x)+ds(9,x),:),cmines(2,ds(12,x)+ds(10,x),:))
            comp(3)=0.2*dot_product(qmines(1,ds(11,x)+ds(9,x),:),qmines(1,ds(12,x)+ds(10,x),:))
            comp(4)=0.2*dot_product(qmines(2,ds(11,x)+ds(9,x),:),qmines(2,ds(12,x)+ds(10,x),:))
            dens(1)=sum(comp)
            if (positron.eq."o") then
              if (id.eq.1) dens_bis(1)=dens(1)
              dens(1)=dens(1)-cplus(ds(11,x)+ds(9,x),1)*cplus(ds(12,x)+ds(10,x),1)
            end if
            comp(:)=0.d0
            comp(1)=0.8*dot_product(cmines(id,ds(11,x)+ds(9,x),:),cmines(id,ds(12,x)+ds(10,x),:))
            comp(2)=0.2*dot_product(qmines(id,ds(11,x)+ds(9,x),:),qmines(id,ds(12,x)+ds(10,x),:))
            dens(2)=sum(comp)
            agr=dot_product(tab,dens)
          else if (id.eq.3) then
            agr=dot_product(tab_bis,dens_bis)
          end if
          hx_in(id,x)=agr
        end do
        !---------------------------------------------------------------------------------------!
      end do        
      close(2)
      call mpi_barrier(mpi_comm_world,ierr)
      do id=1,id_max
        call mpi_allreduce(hx_in(id,:),hx_ox(id,:),nbas_mpi,mpi_real8,mpi_sum,mpi_comm_world,ierr) 
      end do
      allocate(cntr(1:id_max,nbas,nbas))
      cntr(:,:,:)=0.d0
      do x=1,nbas_mpi        
        do id=1,id_max
          km=ds(5,x)+ds(3,x); im=ds(6,x)+ds(4,x)
          cntr(id,km,im)=cntr(id,km,im)+hx_ox(id,x)
        end do
      end do
      do x=1,nbas_mpi
        km=ds(5,x)+ds(3,x); im=ds(6,x)+ds(4,x)
        do id=1,id_max
          if (id.le.2) fck_mines(id,km,im)=cntr(id,km,im)+hcor(1,km,im)
          if (id.eq.3) fck_plus(km,im)=cntr(id,km,im)+hcor(2,km,im)
        end do
      end do  
      deallocate(cntr,hx_in,hx_ox)  
      !Lower triangular fock matrices
      do k=1,nbas; do i=1,nbas; if (k.gt.i) then
        fck_mines(:,k,i)=fck_mines(:,i,k); if (positron.eq."o") fck_plus(k,i)=fck_plus(i,k)
      end if; end do; end do      
      !GSE
      egr(:)=0.d0
      do k=1,nbas; do i=1,nbas 
        do id=1,2
          tab(id)=0.8*dot_product(cmines(id,k,:),cmines(id,i,:))+0.2*dot_product(qmines(id,k,:),qmines(id,i,:))
        end do
        egr(1)=egr(1)+sum(tab)*hcor(1,k,i); if (positron.eq."o") egr(2)=egr(2)+cplus(k,1)*cplus(i,1)*hcor(2,k,i)
      end do; end do
      do id=1,2; do i=1,nspin(id)
          egr(1)=egr(1)+emines(id,i)
      end do; end do
      if (positron.eq."o") egr(2)=egr(2)+eplus(1); eo=sum(egr)/2.d0
      !Diagonalization of the fock matrices 
      qmines=cmines
      do id=1,id_max
        if (id.le.2) fck(:,:)=fck_mines(id,:,:)
        if (id.eq.3) fck(:,:)=fck_plus(:,:)
        fck=matmul(turn_ms,matmul(fck,turn_ms))
        call tred2(fck,nbas,nbas,d,e); call tqli(d,e,nbas,nbas,fck)
        call eigsrt(d,fck,nbas,nbas)
        if (id.le.2) then 
          do i=1,nspin(id)
            cmines(id,:,i)=matmul(turn_ms,fck(:,i))
          end do  
          emines(id,:)=d(:) 
        else if (id.eq.3) then 
          do i=1,nbas
            cplus(:,i)=matmul(turn_ms,fck(:,i)); eplus(i)=d(i)
          end do  
        end if
      end do
      if (dabs(eo_ver-eo).lt.eps) status=1
      eo_ver=eo
    end do
    call mpi_barrier(mpi_comm_world,ierr)
    deallocate(ds)
    if (rank.eq.0) then
      call system ("rm cmd2"); call system ("rm *.dat")
      if (str.eq."Ps") call cm_components
    end if  
    call seconds(count_3)
    twork=count_3-count_1
  end subroutine scf_loop
  
subroutine cm_components
    implicit real*8 (a-h,o-z)
    integer*4  :: k,i
    dimension comp(3),dens(2)
    accu=0.d0
    do k=1,nbas; do i=1,nbas
      comp(1)=cmines(1,k,1)
      comp(2)=cmines(1,i,1)
      comp(3)=hcor(1,k,i)
      dens(1)=product(comp)
      comp(1)=cplus(k,1)
      comp(2)=cplus(i,1)
      comp(3)=hcor(2,k,i)
      dens(2)=product(comp)
      accu=accu+sum(dens)
    end do; end do
    write(6,*) "cm_components",accu/2.d0
  end subroutine cm_components
  
subroutine lowdin 
    implicit real*8 (a-h,o-z)
    real*8, dimension(:,:), allocatable  :: sovr_sub
    real*8, dimension(:), allocatable    :: d_sub,e_sub
    integer*4                            :: i,k,km,im,incr,orb
    dimension rms(nbas,nbas),rps(nbas,nbas)
    rms(:,:)=0.d0; rps(:,:)=0.d0
    !Diagonalization of sub-overlap matrices
    do orb=1,orb_max
      call disp(orb,incr)
      allocate(sovr_sub(nbas_orb(orb),nbas_orb(orb)),d_sub(nbas_orb(orb)),e_sub(nbas_orb(orb)))
      sovr_sub(:,:)=0.d0
      do k=1,nbas_orb(orb); do i=1,nbas_orb(orb)
        km=k+incr; im=i+incr
        sovr_sub(k,i)=sovr(km,im)
      end do; end do
      call tred2(sovr_sub,nbas_orb(orb),nbas_orb(orb),d_sub,e_sub)
      call tqli(d_sub,e_sub,nbas_orb(orb),nbas_orb(orb),sovr_sub)
      call eigsrt(d_sub,sovr_sub,nbas_orb(orb),nbas_orb(orb))
      do i=1,nbas_orb(orb)
        rps(i+incr,i+incr)=dsqrt(d_sub(i)); rms(i+incr,i+incr)=1.d0/dsqrt(d_sub(i))
      end do 
      do k=1,nbas_orb(orb); do i=1,nbas_orb(orb)
        km=k+incr; im=i+incr
        sovr(km,im)=sovr_sub(k,i)
      end do; end do
      deallocate(sovr_sub,d_sub,e_sub)
    end do 
    turn_ps=matmul(sovr,matmul(rps,transpose(sovr)))
    turn_ms=matmul(sovr,matmul(rms,transpose(sovr)))
  end subroutine lowdin 
  
subroutine GTO2(li,a,b,jk2)
    implicit real*8 (a-h,o-z)
    integer*4                            :: li,x,y
    real*8                               :: jk2,itgr
    complex*16                           :: arg,clgam
    complex(pr)                          :: a_cmp,b_cmp,c_cmp,z_cmp,hyp_2f1
    real*8, dimension(:), allocatable    :: tab,comp,comp_bis,rcmp_1
    real*8, dimension(:,:), allocatable  :: rcmp_2
    intent(in) li,a,b
    intent(out) jk2
    dimension a(1:2),b(1:2),itgr(1:2)
    external clgam
    !Formula from mathematica
    itgr(:)=0.d0
    allocate(tab(1:5),comp(1:2),comp_bis(1:4))
    tab(1)=b(1)**(dfloat(li)/2.d0)
    tab(2)=b(2)**(a(1)/2.d0)
    tab(3)=a(1)-dfloat(li)
    arg=dcmplx((a(1)-dfloat(li))/2.d0,0.d0); tab(4)=cdexp(clgam(arg))
    arg=dcmplx((a(2)+dfloat(li+1))/2.d0,0.d0); tab(5)=cdexp(clgam(arg))
    comp(1)=product(tab)
    !
    tab(1)=-2.d0
    tab(2)=b(2)**(dfloat(li)/2.d0)
    tab(3)=b(1)**(a(1)/2.d0)
    arg=dcmplx((1.d0+sum(a))/2.d0,0.d0); tab(4)=cdexp(clgam(arg))
    a_cmp=arg
    b_cmp=dcmplx((a(1)-dfloat(li))/2.d0,0.d0)
    c_cmp=b_cmp+dcmplx(1.d0,0.d0)
    z_cmp=dcmplx(-b(1)/b(2),0.d0)
    tab(5)=abs(hyp_2f1(a_cmp,b_cmp,c_cmp,z_cmp))
    comp(2)=product(tab)
    !
    comp_bis(1)=sum(comp)
    comp_bis(2)=b(1)**(-a(1)/2.d0)
    comp_bis(3)=b(2)**(-(sum(a)+dfloat(li+1))/2.d0)
    comp_bis(4)=2.5d-1/(a(1)-dfloat(li))
    itgr(1)=product(comp_bis)
    deallocate(tab,comp,comp_bis)
    !
    allocate(comp_bis(1:4))
    comp_bis(1)=5.d-1/(a(1)+dfloat(li+1))
    comp_bis(2)=b(2)**(-(1.d0+sum(a))/2.d0)
    arg=dcmplx((1.d0+sum(a))/2.d0,0.d0); comp_bis(3)=cdexp(clgam(arg))
    a_cmp=arg
    b_cmp=dcmplx((a(1)+dfloat(li+1))/2.d0,0.d0)
    c_cmp=b_cmp+dcmplx(1.d0,0.d0)
    z_cmp=dcmplx(-b(1)/b(2),0.d0)
    comp_bis(4)=abs(hyp_2f1(a_cmp,b_cmp,c_cmp,z_cmp))
    itgr(2)=product(comp_bis)
    deallocate(comp_bis)
    jk2=sum(itgr)
end subroutine GTO2

real*8 function GTO(mode,k,i,r,s)
    implicit real*8 (a-h,o-z)
    real*8, dimension(:), allocatable  :: ox
    character(len=7)                   :: mode
    character(len=20)                  :: str
    integer*4                          :: k,i,r,s,ivar,orb,l,Zc
    complex*16                         :: clgam,arg
    dimension cstr(0:2),tab(1:2)
    external clgam
    common/GTO_para/cstr,str,ivar,Zc
    common/GTO_orb/orb
    l=orb-1
    allocate(ox(1:4))
    sumr=ar(l,k,r)+ar(l,i,s); prodr=ar(l,k,r)*ar(l,i,s)
    ox(1)=dr(l,k,r)*dr(l,i,s)
    ox(2)=(2.d0*dsqrt(prodr)/pi)**(3.d0/2.d0+dfloat(l))
    ox(3)=cstr(l)**2.d0
    if (mode.eq."overlap") then 
      arg=dcmplx(3.d0/2.d0+dfloat(l),0.d0)
      ox(4)=dreal(cdexp(clgam(arg)))/(2.d0*sumr**(3.d0/2.d0+dfloat(l)))
    else if (mode.eq."one-elc") then
      if (ivar.eq.1) sgn=-1.d0
      if (ivar.eq.2) sgn=+1.d0
      arg=dcmplx(5.d0/2.d0+dfloat(l),0.d0)
      tab(1)=2.d0*prodr*dreal(cdexp(clgam(arg)))
      arg=dcmplx(dfloat(l+1),0.d0)
      tab(2)=sgn*dfloat(Zc)*dreal(cdexp(clgam(arg)))*sumr**(3.d0/2.d0)
      ox(4)=sum(tab)/(2.d0*sumr**(5.d0/2.d0+dfloat(l)))
    end if
      GTO=product(ox)
      deallocate(ox)
  end function GTO
end module lcpmr
