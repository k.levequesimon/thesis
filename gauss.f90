module gauss_quadrature


public         :: cdgqf,cgqf
contains


subroutine cdgqf ( nt, kind, alpha, beta, t, wts )
!*****************************************************************************80
!
!! cdgqf computes a gauss quadrature formula with default a, b and simple knots.
!
!  discussion:
!
!    this routine computes all the knots and weights of a gauss quadrature
!    formula with a classical weight function with default values for a and b,
!    and only simple knots.
!
!    there are no moments checks and no printing is done.
!
!    use routine eiqfs to evaluate a quadrature computed by cgqfs.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    04 january 2010
!
!  author:
!
!    original fortran77 version by sylvan elhay, jaroslav kautsky.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    sylvan elhay, jaroslav kautsky,
!    algorithm 655: iqpack, fortran subroutines for the weights of
!    interpolatory quadrature,
!    acm transactions on mathematical software,
!    volume 13, number 4, december 1987, pages 399-415.
!
!  parameters:
!
!    input, integer ( kind = 4 ) nt, the number of knots.
!
!    input, integer ( kind = 4 ) kind, the rule.
!    1, legendre,             (a,b)       1.0
!    2, chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
!    3, gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
!    4, jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
!    5, generalized laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
!    6, generalized hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
!    7, exponential,          (a,b)       |x-(a+b)/2.0|^alpha
!    8, rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
!
!    input, real ( kind = 8 ) alpha, the value of alpha, if needed.
!
!    input, real ( kind = 8 ) beta, the value of beta, if needed.
!
!    output, real ( kind = 8 ) t(nt), the knots.
!
!    output, real ( kind = 8 ) wts(nt), the weights.
!
  implicit none

  integer ( kind = 4 ) nt

  real    ( kind = 8 ) aj(nt)
  real    ( kind = 8 ) alpha
  real    ( kind = 8 ) beta
  real    ( kind = 8 ) bj(nt)
  integer ( kind = 4 ) kind
  real    ( kind = 8 ) t(nt)
  real    ( kind = 8 ) wts(nt)
  real    ( kind = 8 ) zemu

  call parchk ( kind, 2 * nt, alpha, beta )
!
!  get the jacobi matrix and zero-th moment.
!
  call class_matrix ( kind, nt, alpha, beta, aj, bj, zemu )
!
!  compute the knots and weights.
!
  call sgqf ( nt, aj, bj, zemu, t, wts )

  return
end subroutine


function ch_eqi ( c1, c2 )

!*****************************************************************************80
!
!! ch_eqi is a case insensitive comparison of two characters for equality.
!
!  example:
!
!    ch_eqi ( 'a', 'a' ) is .true.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    28 july 2000
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    input, character c1, c2, the characters to compare.
!
!    output, logical ch_eqi, the result of the comparison.
!
  implicit none

  logical   ch_eqi
  character c1
  character c1_cap
  character c2
  character c2_cap

  c1_cap = c1
  c2_cap = c2

  call ch_cap ( c1_cap )
  call ch_cap ( c2_cap )

  if ( c1_cap == c2_cap ) then
    ch_eqi = .true.
  else
    ch_eqi = .false.
  end if

end function

function r8_gamma ( x )

!*****************************************************************************80
!
!! r8_gamma evaluates gamma(x) for a real argument.
!
!  discussion:
!
!    this routine calculates the gamma function for a real argument x.
!
!    computation is based on an algorithm outlined in reference 1.
!    the program uses rational functions that approximate the gamma
!    function to at least 20 significant decimal digits.  coefficients
!    for the approximation over the interval (1,2) are unpublished.
!    those for the approximation for 12 <= x are from reference 2.
!
!  modified:
!
!    11 february 2008
!
!  author:
!
!    original fortran77 version by william cody, laura stoltz.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    william cody,
!    an overview of software development for special functions,
!    in numerical analysis dundee, 1975,
!    edited by ga watson,
!    lecture notes in mathematics 506,
!    springer, 1976.
!
!    john hart, ward cheney, charles lawson, hans maehly,
!    charles mesztenyi, john rice, henry thatcher,
!    christoph witzgall,
!    computer approximations,
!    wiley, 1968,
!    lc: qa297.c64.
!
!  parameters:
!
!    input, real ( kind = 8 ) x, the argument of the function.
!
!    output, real ( kind = 8 ) r8_gamma, the value of the function.
!
  implicit none

  real    ( kind = 8 ), dimension ( 7 ) :: c = (/ &
   -1.910444077728d-03, &
    8.4171387781295d-04, &
   -5.952379913043012d-04, &
    7.93650793500350248d-04, &
   -2.777777777777681622553d-03, &
    8.333333333333333331554247d-02, &
    5.7083835261d-03 /)
  real    ( kind = 8 ), parameter :: eps = 2.22d-16
  real    ( kind = 8 ) fact
  integer ( kind = 4 ) i
  integer ( kind = 4 ) n
  real    ( kind = 8 ), dimension ( 8 ) :: p = (/ &
    -1.71618513886549492533811d+00, &
     2.47656508055759199108314d+01, &
    -3.79804256470945635097577d+02, &
     6.29331155312818442661052d+02, &
     8.66966202790413211295064d+02, &
    -3.14512729688483675254357d+04, &
    -3.61444134186911729807069d+04, &
     6.64561438202405440627855d+04 /)
  logical parity
  real    ( kind = 8 ), parameter :: pi = 3.1415926535897932384626434d+00
  real    ( kind = 8 ), dimension ( 8 ) :: q = (/ &
    -3.08402300119738975254353d+01, &
     3.15350626979604161529144d+02, &
    -1.01515636749021914166146d+03, &
    -3.10777167157231109440444d+03, &
     2.25381184209801510330112d+04, &
     4.75584627752788110767815d+03, &
    -1.34659959864969306392456d+05, &
    -1.15132259675553483497211d+05 /)
  real    ( kind = 8 ) r8_gamma
  real    ( kind = 8 ) res
  real    ( kind = 8 ), parameter :: sqrtpi = 0.9189385332046727417803297d+00
  real    ( kind = 8 ) sum
  real    ( kind = 8 ) x
  real    ( kind = 8 ), parameter :: xbig = 171.624d+00
  real    ( kind = 8 ) xden
  real    ( kind = 8 ), parameter :: xinf = 1.0d+30
  real    ( kind = 8 ), parameter :: xminin = 2.23d-308
  real    ( kind = 8 ) xnum
  real    ( kind = 8 ) y
  real    ( kind = 8 ) y1
  real    ( kind = 8 ) ysq
  real    ( kind = 8 ) z

  parity = .false.
  fact = 1.0d+00
  n = 0
  y = x
!
!  argument is negative.
!
  if ( y <= 0.0d+00 ) then

    y = - x
    y1 = aint ( y )
    res = y - y1

    if ( res /= 0.0d+00 ) then

      if ( y1 /= aint ( y1 * 0.5d+00 ) * 2.0d+00 ) then
        parity = .true.
      end if

      fact = - pi / sin ( pi * res )
      y = y + 1.0d+00

    else

      res = xinf
      r8_gamma = res
      return

    end if

  end if
!
!  argument is positive.
!
  if ( y < eps ) then
!
!  argument < eps.
!
    if ( xminin <= y ) then
      res = 1.0d+00 / y
    else
      res = xinf
      r8_gamma = res
      return
    end if

  else if ( y < 12.0d+00 ) then

    y1 = y
!
!  0.0 < argument < 1.0.
!
    if ( y < 1.0d+00 ) then

      z = y
      y = y + 1.0d+00
!
!  1.0 < argument < 12.0.
!  reduce argument if necessary.
!
    else

      n = int ( y ) - 1
      y = y - real ( n, kind = 8 )
      z = y - 1.0d+00

    end if
!
!  evaluate approximation for 1.0 < argument < 2.0.
!
    xnum = 0.0d+00
    xden = 1.0d+00
    do i = 1, 8
      xnum = ( xnum + p(i) ) * z
      xden = xden * z + q(i)
    end do

    res = xnum / xden + 1.0d+00
!
!  adjust result for case  0.0 < argument < 1.0.
!
    if ( y1 < y ) then

      res = res / y1
!
!  adjust result for case 2.0 < argument < 12.0.
!
    else if ( y < y1 ) then

      do i = 1, n
        res = res * y
        y = y + 1.0d+00
      end do

    end if

  else
!
!  evaluate for 12.0 <= argument.
!
    if ( y <= xbig ) then

      ysq = y * y
      sum = c(7)
      do i = 1, 6
        sum = sum / ysq + c(i)
      end do
      sum = sum / y - y + sqrtpi
      sum = sum + ( y - 0.5d+00 ) * log ( y )
      res = exp ( sum )

    else

      res = xinf
      r8_gamma = res
      return

    end if

  end if
!
!  final adjustments and return.
!
  if ( parity ) then
    res = - res
  end if

  if ( fact /= 1.0d+00 ) then
    res = fact / res
  end if

  r8_gamma = res


end function


subroutine cgqf ( nt, kind, alpha, beta, a, b, t, wts )

!*****************************************************************************80
!
!! cgqf computes knots and weights of a gauss quadrature formula.
!
!  discussion:
!
!    the user may specify the interval (a,b).
!
!    only simple knots are produced.
!
!    use routine eiqfs to evaluate this quadrature formula.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    16 february 2010
!
!  author:
!
!    original fortran77 version by sylvan elhay, jaroslav kautsky.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    sylvan elhay, jaroslav kautsky,
!    algorithm 655: iqpack, fortran subroutines for the weights of
!    interpolatory quadrature,
!    acm transactions on mathematical software,
!    volume 13, number 4, december 1987, pages 399-415.
!
!  parameters:
!
!    input, integer ( kind = 4 ) nt, the number of knots.
!
!    input, integer ( kind = 4 ) kind, the rule.
!    1, legendre,             (a,b)       1.0
!    2, chebyshev type 1,     (a,b)       ((b-x)*(x-a))^-0.5)
!    3, gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
!    4, jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
!    5, generalized laguerre, (a,+oo)     (x-a)^alpha*exp(-b*(x-a))
!    6, generalized hermite,  (-oo,+oo)   |x-a|^alpha*exp(-b*(x-a)^2)
!    7, exponential,          (a,b)       |x-(a+b)/2.0|^alpha
!    8, rational,             (a,+oo)     (x-a)^alpha*(x+b)^beta
!    9, chebyshev type 2,     (a,b)       ((b-x)*(x-a))^(+0.5)
!
!    input, real ( kind = 8 ) alpha, the value of alpha, if needed.
!
!    input, real ( kind = 8 ) beta, the value of beta, if needed.
!
!    input, real ( kind = 8 ) a, b, the interval endpoints, or
!    other parameters.
!
!    output, real ( kind = 8 ) t(nt), the knots.
!
!    output, real ( kind = 8 ) wts(nt), the weights.
!
  implicit none

  integer ( kind = 4 ) nt

  real    ( kind = 8 ) a
  real    ( kind = 8 ) alpha
  real    ( kind = 8 ) b
  real    ( kind = 8 ) beta
  integer ( kind = 4 ) i
  integer ( kind = 4 ) kind
  integer ( kind = 4 ), allocatable :: mlt(:)
  integer ( kind = 4 ), allocatable :: ndx(:)
  real    ( kind = 8 ) t(nt)
  real    ( kind = 8 ) wts(nt)
!
!  compute the gauss quadrature formula for default values of a and b.
!
  call cdgqf ( nt, kind, alpha, beta, t, wts )
!
!  prepare to scale the quadrature formula to other weight function with
!  valid a and b.
!
  allocate ( mlt(1:nt) )

  mlt(1:nt) = 1

  allocate ( ndx(1:nt) )

  do i = 1, nt
    ndx(i) = i
  end do

  call scqf ( nt, t, mlt, wts, nt, ndx, wts, t, kind, alpha, beta, a, b )

  deallocate ( mlt )
  deallocate ( ndx )

  return
end subroutine
subroutine ch_cap ( c )

!*****************************************************************************80
!
!! ch_cap capitalizes a single character.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    19 july 1998
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    input/output, character c, the character to capitalize.
!
  implicit none

  character              c
  integer   ( kind = 4 ) itemp

  itemp = ichar ( c )

  if ( 97 <= itemp .and. itemp <= 122 ) then
    c = char ( itemp - 32 )
  end if

  return
end subroutine

subroutine ch_to_digit ( c, digit )

!*****************************************************************************80
!
!! ch_to_digit returns the integer value of a base 10 digit.
!
!  example:
!
!     c   digit
!    ---  -----
!    '0'    0
!    '1'    1
!    ...  ...
!    '9'    9
!    ' '    0
!    'x'   -1
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    04 august 1999
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    input, character c, the decimal digit, '0' through '9' or blank
!    are legal.
!
!    output, integer ( kind = 4 ) digit, the corresponding integer value.
!    if c was 'illegal', then digit is -1.
!
  implicit none

  character              c
  integer   ( kind = 4 ) digit

  if ( lge ( c, '0' ) .and. lle ( c, '9' ) ) then

    digit = ichar ( c ) - 48

  else if ( c == ' ' ) then

    digit = 0

  else

    digit = -1

  end if

  return
end subroutine
subroutine class_matrix ( kind, m, alpha, beta, aj, bj, zemu )

!*****************************************************************************80
!
!! class_matrix computes the jacobi matrix for a quadrature rule.
!
!  discussion:
!
!    this routine computes the diagonal aj and sub-diagonal bj
!    elements of the order m tridiagonal symmetric jacobi matrix
!    associated with the polynomials orthogonal with respect to
!    the weight function specified by kind.
!
!    for weight functions 1-7, m elements are defined in bj even
!    though only m-1 are needed.  for weight function 8, bj(m) is
!    set to zero.
!
!    the zero-th moment of the weight function is returned in zemu.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    27 december 2009
!
!  author:
!
!    original fortran77 version by sylvan elhay, jaroslav kautsky.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    sylvan elhay, jaroslav kautsky,
!    algorithm 655: iqpack, fortran subroutines for the weights of
!    interpolatory quadrature,
!    acm transactions on mathematical software,
!    volume 13, number 4, december 1987, pages 399-415.
!
!  parameters:
!
!    input, integer ( kind = 4 ) kind, the rule.
!    1, legendre,             (a,b)       1.0
!    2, chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
!    3, gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
!    4, jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
!    5, generalized laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
!    6, generalized hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
!    7, exponential,          (a,b)       |x-(a+b)/2.0|^alpha
!    8, rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
!
!    input, integer ( kind = 4 ) m, the order of the jacobi matrix.
!
!    input, real ( kind = 8 ) alpha, the value of alpha, if needed.
!
!    input, real ( kind = 8 ) beta, the value of beta, if needed.
!
!    output, real ( kind = 8 ) aj(m), bj(m), the diagonal and subdiagonal
!    of the jacobi matrix.
!
!    output, real ( kind = 8 ) zemu, the zero-th moment.
!
  implicit none

  integer ( kind = 4 ) m

  real    ( kind = 8 ) a2b2
  real    ( kind = 8 ) ab
  real    ( kind = 8 ) aba
  real    ( kind = 8 ) abi
  real    ( kind = 8 ) abj
  real    ( kind = 8 ) abti
  real    ( kind = 8 ) aj(m)
  real    ( kind = 8 ) alpha
  real    ( kind = 8 ) apone
  real    ( kind = 8 ) beta
  real    ( kind = 8 ) bj(m)
  integer ( kind = 4 ) i
  integer ( kind = 4 ) kind
  real    ( kind = 8 ), parameter :: pi = 3.14159265358979323846264338327950d+00
  !real    ( kind = 8 ) r8_gamma
  real    ( kind = 8 ) temp
  real    ( kind = 8 ) temp2
  real    ( kind = 8 ) zemu

  temp = epsilon ( temp )

  call parchk ( kind, 2 * m - 1, alpha, beta )

  temp2 = 0.5d+00

  if ( 500.0d+00 * temp < abs ( ( r8_gamma ( temp2 ) )**2 - pi ) ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'class_matrix - fatal error!'
    write ( *, '(a)' ) '  gamma function does not match machine parameters.'
    stop
  end if

  if ( kind == 1 ) then

    ab = 0.0d+00

    zemu = 2.0d+00 / ( ab + 1.0d+00 )

    aj(1:m) = 0.0d+00

    do i = 1, m
      abi = i + ab * mod ( i, 2 )
      abj = 2 * i + ab
      bj(i) = abi * abi / ( abj * abj - 1.0d+00 )
    end do
    bj(1:m) =  sqrt ( bj(1:m) )

  else if ( kind == 2 ) then

    zemu = pi

    aj(1:m) = 0.0d+00

    bj(1) =  sqrt ( 0.5d+00 )
    bj(2:m) = 0.5d+00

  else if ( kind == 3 ) then

    ab = alpha * 2.0d+00
    zemu = 2.0d+00**( ab + 1.0d+00 ) * r8_gamma ( alpha + 1.0d+00 )**2 &
      / r8_gamma ( ab + 2.0d+00 )

    aj(1:m) = 0.0d+00
    bj(1) = 1.0d+00 / ( 2.0d+00 * alpha + 3.0d+00 )
    do i = 2, m
      bj(i) = i * ( i + ab ) / ( 4.0d+00 * ( i + alpha )**2 - 1.0d+00 )
    end do
    bj(1:m) =  sqrt ( bj(1:m) )

  else if ( kind == 4 ) then

    ab = alpha + beta
    abi = 2.0d+00 + ab
    zemu = 2.0d+00**( ab + 1.0d+00 ) * r8_gamma ( alpha + 1.0d+00 ) &
      * r8_gamma ( beta + 1.0d+00 ) / r8_gamma ( abi )
    aj(1) = ( beta - alpha ) / abi
    bj(1) = 4.0d+00 * ( 1.0 + alpha ) * ( 1.0d+00 + beta ) &
      / ( ( abi + 1.0d+00 ) * abi * abi )
    a2b2 = beta * beta - alpha * alpha

    do i = 2, m
      abi = 2.0d+00 * i + ab
      aj(i) = a2b2 / ( ( abi - 2.0d+00 ) * abi )
      abi = abi**2
      bj(i) = 4.0d+00 * i * ( i + alpha ) * ( i + beta ) * ( i + ab ) &
        / ( ( abi - 1.0d+00 ) * abi )
    end do
    bj(1:m) =  sqrt ( bj(1:m) )

  else if ( kind == 5 ) then

    zemu = r8_gamma ( alpha + 1.0d+00 )

    do i = 1, m
      aj(i) = 2.0d+00 * i - 1.0d+00 + alpha
      bj(i) = i * ( i + alpha )
    end do
    bj(1:m) =  sqrt ( bj(1:m) )

  else if ( kind == 6 ) then

    zemu = r8_gamma ( ( alpha + 1.0d+00 ) / 2.0d+00 )

    aj(1:m) = 0.0d+00

    do i = 1, m
      bj(i) = ( i + alpha * mod ( i, 2 ) ) / 2.0d+00
    end do
    bj(1:m) =  sqrt ( bj(1:m) )

  else if ( kind == 7 ) then

    ab = alpha
    zemu = 2.0d+00 / ( ab + 1.0d+00 )

    aj(1:m) = 0.0d+00

    do i = 1, m
      abi = i + ab * mod ( i, 2 )
      abj = 2 * i + ab
      bj(i) = abi * abi / ( abj * abj - 1.0d+00 )
    end do
    bj(1:m) =  sqrt ( bj(1:m) )

  else if ( kind == 8 ) then

    ab = alpha + beta
    zemu = r8_gamma ( alpha + 1.0d+00 ) * r8_gamma ( - ( ab + 1.0d+00 ) ) &
      / r8_gamma ( - beta )
    apone = alpha + 1.0d+00
    aba = ab * apone
    aj(1) = - apone / ( ab + 2.0d+00 )
    bj(1) = - aj(1) * ( beta + 1.0d+00 ) / ( ab + 2.0d+00 ) / ( ab + 3.0d+00 )
    do i = 2, m
      abti = ab + 2.0d+00 * i
      aj(i) = aba + 2.0d+00 * ( ab + i ) * ( i - 1 )
      aj(i) = - aj(i) / abti / ( abti - 2.0d+00 )
    end do

    do i = 2, m - 1
      abti = ab + 2.0d+00 * i
      bj(i) = i * ( alpha + i ) / ( abti - 1.0d+00 ) * ( beta + i ) &
        / ( abti**2 ) * ( ab + i ) / ( abti + 1.0d+00 )
    end do

    bj(m) = 0.0d+00
    bj(1:m) =  sqrt ( bj(1:m) )

  end if

  return
end subroutine
subroutine get_unit ( iunit )

!*****************************************************************************80
!
!! get_unit returns a free fortran unit number.
!
!  discussion:
!
!    a "free" fortran unit number is an integer between 1 and 99 which
!    is not currently associated with an i/o device.  a free fortran unit
!    number is needed in order to open a file with the open command.
!
!    if iunit = 0, then no free fortran unit could be found, although
!    all 99 units were checked (except for units 5, 6 and 9, which
!    are commonly reserved for console i/o).
!
!    otherwise, iunit is an integer between 1 and 99, representing a
!    free fortran unit.  note that get_unit assumes that units 5 and 6
!    are special, and will never return those values.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    18 september 2005
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    output, integer ( kind = 4 ) iunit, the free unit number.
!
  implicit none

  integer ( kind = 4 ) i
  integer ( kind = 4 ) ios
  integer ( kind = 4 ) iunit
  logical              lopen

  iunit = 0

  do i = 1, 99

    if ( i /= 5 .and. i /= 6 .and. i /= 9 ) then

      inquire ( unit = i, opened = lopen, iostat = ios )

      if ( ios == 0 ) then
        if ( .not. lopen ) then
          iunit = i
          return
        end if
      end if

    end if

  end do

  return
end subroutine
subroutine imtqlx ( n, d, e, z )

!*****************************************************************************80
!
!! imtqlx diagonalizes a symmetric tridiagonal matrix.
!
!  discussion:
!
!    this routine is a slightly modified version of the eispack routine to
!    perform the implicit ql algorithm on a symmetric tridiagonal matrix.
!
!    the authors thank the authors of eispack for permission to use this
!    routine.
!
!    it has been modified to produce the product q' * z, where z is an input
!    vector and q is the orthogonal matrix diagonalizing the input matrix.
!    the changes consist (essentialy) of applying the orthogonal transformations
!    directly to z as they are generated.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    27 december 2009
!
!  author:
!
!    original fortran77 version by sylvan elhay, jaroslav kautsky.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    sylvan elhay, jaroslav kautsky,
!    algorithm 655: iqpack, fortran subroutines for the weights of
!    interpolatory quadrature,
!    acm transactions on mathematical software,
!    volume 13, number 4, december 1987, pages 399-415.
!
!    roger martin, james wilkinson,
!    the implicit ql algorithm,
!    numerische mathematik,
!    volume 12, number 5, december 1968, pages 377-383.
!
!  parameters:
!
!    input, integer ( kind = 4 ) n, the order of the matrix.
!
!    input/output, real ( kind = 8 ) d(n), the diagonal entries of the matrix.
!    on output, the information in d has been overwritten.
!
!    input/output, real ( kind = 8 ) e(n), the subdiagonal entries of the
!    matrix, in entries e(1) through e(n-1).  on output, the information in
!    e has been overwritten.
!
!    input/output, real ( kind = 8 ) z(n).  on input, a vector.  on output,
!    the value of q' * z, where q is the matrix that diagonalizes the
!    input symmetric tridiagonal matrix.
!
  implicit none

  integer ( kind = 4 ) n

  real    ( kind = 8 ) b
  real    ( kind = 8 ) c
  real    ( kind = 8 ) d(n)
  real    ( kind = 8 ) e(n)
  real    ( kind = 8 ) f
  real    ( kind = 8 ) g
  integer ( kind = 4 ) i
  integer ( kind = 4 ) ii
  integer ( kind = 4 ), parameter :: itn = 30
  integer ( kind = 4 ) j
  integer ( kind = 4 ) k
  integer ( kind = 4 ) l
  integer ( kind = 4 ) m
  integer ( kind = 4 ) mml
  real    ( kind = 8 ) p
  real    ( kind = 8 ) prec
  real    ( kind = 8 ) r
  real    ( kind = 8 ) s
  real    ( kind = 8 ) z(n)

  prec = epsilon ( prec )

  if ( n == 1 ) then
    return
  end if

  e(n) = 0.0d+00

  do l = 1, n

    j = 0

    do

      do m = l, n

        if ( m == n ) then
          exit
        end if

        if ( abs ( e(m) ) <= prec * ( abs ( d(m) ) + abs ( d(m+1) ) ) ) then
          exit
        end if

      end do

      p = d(l)

      if ( m == l ) then
        exit
      end if

      if ( itn <= j ) then
        write ( *, '(a)' ) ' '
        write ( *, '(a)' ) 'imtqlx - fatal error!'
        write ( *, '(a)' ) '  iteration limit exceeded.'
        write ( *, '(a,i8)' ) '  j = ', j
        write ( *, '(a,i8)' ) '  l = ', l
        write ( *, '(a,i8)' ) '  m = ', m
        write ( *, '(a,i8)' ) '  n = ', n
        stop
      end if

      j = j + 1
      g = ( d(l+1) - p ) / ( 2.0d+00 * e(l) )
      r =  sqrt ( g * g + 1.0d+00 )
      g = d(m) - p + e(l) / ( g + sign ( r, g ) )
      s = 1.0d+00
      c = 1.0d+00
      p = 0.0d+00
      mml = m - l

      do ii = 1, mml

        i = m - ii
        f = s * e(i)
        b = c * e(i)

        if ( abs ( g ) <= abs ( f ) ) then
          c = g / f
          r =  sqrt ( c * c + 1.0d+00 )
          e(i+1) = f * r
          s = 1.0d+00 / r
          c = c * s
        else
          s = f / g
          r =  sqrt ( s * s + 1.0d+00 )
          e(i+1) = g * r
          c = 1.0d+00 / r
          s = s * c
        end if

        g = d(i+1) - p
        r = ( d(i) - g ) * s + 2.0d+00 * c * b
        p = s * r
        d(i+1) = g + p
        g = c * r - b
        f = z(i+1)
        z(i+1) = s * z(i) + c * f
        z(i) = c * z(i) - s * f

      end do

      d(l) = d(l) - p
      e(l) = g
      e(m) = 0.0d+00

    end do

  end do
!
!  sorting.
!
  do ii = 2, n

    i = ii - 1
    k = i
    p = d(i)

    do j = ii, n
      if ( d(j) < p ) then
        k = j
        p = d(j)
      end if
    end do

    if ( k /= i ) then
      d(k) = d(i)
      d(i) = p
      p = z(i)
      z(i) = z(k)
      z(k) = p
    end if

  end do

  return
end subroutine
subroutine parchk ( kind, m, alpha, beta )

!*****************************************************************************80
!
!! parchk checks parameters alpha and beta for classical weight functions.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    27 december 2009
!
!  author:
!
!    original fortran77 version by sylvan elhay, jaroslav kautsky.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    sylvan elhay, jaroslav kautsky,
!    algorithm 655: iqpack, fortran subroutines for the weights of
!    interpolatory quadrature,
!    acm transactions on mathematical software,
!    volume 13, number 4, december 1987, pages 399-415.
!
!  parameters:
!
!    input, integer ( kind = 4 ) kind, the rule.
!    1, legendre,             (a,b)       1.0
!    2, chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
!    3, gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
!    4, jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
!    5, generalized laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
!    6, generalized hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
!    7, exponential,          (a,b)       |x-(a+b)/2.0|^alpha
!    8, rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
!
!    input, integer ( kind = 4 ) m, the order of the highest moment to
!    be calculated.  this value is only needed when kind = 8.
!
!    input, real ( kind = 8 ) alpha, beta, the parameters, if required
!    by the value of kind.
!
  implicit none

  real    ( kind = 8 ) alpha
  real    ( kind = 8 ) beta
  integer ( kind = 4 ) kind
  integer ( kind = 4 ) m
  real    ( kind = 8 ) tmp

  if ( kind <= 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'parchk - fatal error!'
    write ( *, '(a)' ) '  kind <= 0.'
    stop
  end if
!
!  check alpha for gegenbauer, jacobi, laguerre, hermite, exponential.
!
  if ( 3 <= kind .and. alpha <= -1.0d+00 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'parchk - fatal error!'
    write ( *, '(a)' ) '  3 <= kind and alpha <= -1.'
    stop
  end if
!
!  check beta for jacobi.
!
  if ( kind == 4 .and. beta <= -1.0d+00 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'parchk - fatal error!'
    write ( *, '(a)' ) '  kind == 4 and beta <= -1.0.'
    stop
  end if
!
!  check alpha and beta for rational.
!
  if ( kind == 8 ) then
    tmp = alpha + beta + m + 1.0d+00
    if ( 0.0d+00 <= tmp .or. tmp <= beta ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'parchk - fatal error!'
      write ( *, '(a)' ) '  kind == 8 but condition on alpha and beta fails.'
      stop
    end if
  end if

  return
end  subroutine
function r8_epsilon ( )

!*****************************************************************************80
!
!! r8_epsilon returns the r8 roundoff unit.
!
!  discussion:
!
!    the roundoff unit is a number r which is a power of 2 with the
!    property that, to the precision of the computer's arithmetic,
!      1 < 1 + r
!    but
!      1 = ( 1 + r / 2 )
!
!    fortran90 provides the superior library routine
!
!      epsilon ( x )
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    15 april 2004
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    output, real ( kind = 8 ) r8_epsilon, the r8 round-off unit.
!
  implicit none

  real    ( kind = 8 ) d
  real    ( kind = 8 ) d_test
  real    ( kind = 8 ) r8_epsilon

  d = 1.0d+00
  d_test = 1.0d+00 + d / 2.0d+00

  do while ( 1.0d+00 < d_test )
    d = d / 2.0d+00
    d_test = 1.0d+00 + d / 2.0d+00
  end do

  r8_epsilon = d

  return
end function

function r8_huge ( )

!*****************************************************************************80
!
!! r8_huge returns a very large r8.
!
!  discussion:
!
!    the value returned by this function is not required to be the
!    maximum representable r8.  this value varies from machine to machine,
!    from compiler to compiler, and may cause problems when being printed.
!    we simply want a "very large" but non-infinite number.
!
!    fortran90 provides a built-in routine huge ( x ) that
!    can return the maximum representable number of the same datatype
!    as x, if that is what is really desired.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    12 october 2007
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    output, real ( kind = 8 ) r8_huge, a "huge" value.
!
  implicit none

  real    ( kind = 8 ) r8_huge

  r8_huge = 1.0d+30

  return
end  function
subroutine r8mat_write ( output_filename, m, n, table )

!*****************************************************************************80
!
!! r8mat_write writes an r8mat file.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    31 may 2009
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    input, character ( len = * ) output_filename, the output file name.
!
!    input, integer ( kind = 4 ) m, the spatial dimension.
!
!    input, integer ( kind = 4 ) n, the number of points.
!
!    input, real ( kind = 8 ) table(m,n), the table data.
!
  implicit none

  integer   ( kind = 4 ) m
  integer   ( kind = 4 ) n

  integer   ( kind = 4 ) j
  character ( len = * )  output_filename
  integer   ( kind = 4 ) output_status
  integer   ( kind = 4 ) output_unit
  character ( len = 30 ) string
  real      ( kind = 8 ) table(m,n)
!
!  open the file.
!
  call get_unit ( output_unit )

  open ( unit = output_unit, file = output_filename, &
    status = 'replace', iostat = output_status )

  if ( output_status /= 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'r8mat_write - fatal error!'
    write ( *, '(a,i8)' ) '  could not open the output file "' // &
      trim ( output_filename ) // '" on unit ', output_unit
    output_unit = -1
    stop
  end if
!
!  create a format string.
!
!  for less precision in the output file, try:
!
!                                            '(', m, 'g', 14, '.', 6, ')'
!
  if ( 0 < m .and. 0 < n ) then

    write ( string, '(a1,i8,a1,i8,a1,i8,a1)' ) '(', m, 'g', 24, '.', 16, ')'
!
!  write the data.
!
    do j = 1, n
      write ( output_unit, string ) table(1:m,j)
    end do

  end if
!
!  close the file.
!
  close ( unit = output_unit )

  return
end subroutine
subroutine rule_write ( order, x, w, r, filename )

!*****************************************************************************80
!
!! rule_write writes a quadrature rule to a file.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    18 february 2010
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    input, integer ( kind = 4 ) order, the order of the rule.
!
!    input, real ( kind = 8 ) x(order), the abscissas.
!
!    input, real ( kind = 8 ) w(order), the weights.
!
!    input, real ( kind = 8 ) r(2), defines the region.
!
!    input, character ( len = * ) filename, specifies the output.
!    'filename_w.txt', 'filename_x.txt', 'filename_r.txt' defining weights,
!    abscissas, and region.
!
  implicit none

  integer   ( kind = 4 )  order

  character ( len = * )   filename
  character ( len = 255 ) filename_r
  character ( len = 255 ) filename_w
  character ( len = 255 ) filename_x
  integer   ( kind = 4 )  i
  integer   ( kind = 4 )  kind
  real      ( kind = 8 )  r(2)
  real      ( kind = 8 )  w(order)
  real      ( kind = 8 )  x(order)

  filename_w = trim ( filename ) // '_w.txt'
  filename_x = trim ( filename ) // '_x.txt'
  filename_r = trim ( filename ) // '_r.txt'

  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) '  creating quadrature files.'
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) '  "root" file name is   "' // trim ( filename ) // '".'
  write ( *, '(a)' ) ' '
  write ( *, '(a)' ) '  weight file will be   "' // trim ( filename_w ) // '".'
  write ( *, '(a)' ) '  abscissa file will be "' // trim ( filename_x ) // '".'
  write ( *, '(a)' ) '  region file will be   "' // trim ( filename_r ) // '".'

  call r8mat_write ( filename_w, 1, order, w )
  call r8mat_write ( filename_x, 1, order, x )
  call r8mat_write ( filename_r, 1, 2,     r )

  return
end subroutine
subroutine s_to_i4 ( s, ival, ierror, length )

!*****************************************************************************80
!
!! s_to_i4 reads an i4 from a string.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    28 june 2000
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    input, character ( len = * ) s, a string to be examined.
!
!    output, integer ( kind = 4 ) ival, the integer value read from the string.
!    if the string is blank, then ival will be returned 0.
!
!    output, integer ( kind = 4 ) ierror, an error flag.
!    0, no error.
!    1, an error occurred.
!
!    output, integer ( kind = 4 ) length, the number of characters of s
!    used to make ival.
!
  implicit none

  character              c
  integer   ( kind = 4 ) i
  integer   ( kind = 4 ) ierror
  integer   ( kind = 4 ) isgn
  integer   ( kind = 4 ) istate
  integer   ( kind = 4 ) ival
  integer   ( kind = 4 ) length
  character ( len = * )  s

  ierror = 0
  istate = 0
  isgn = 1
  ival = 0

  do i = 1, len_trim ( s )

    c = s(i:i)
!
!  haven't read anything.
!
    if ( istate == 0 ) then

      if ( c == ' ' ) then

      else if ( c == '-' ) then
        istate = 1
        isgn = -1
      else if ( c == '+' ) then
        istate = 1
        isgn = + 1
      else if ( lle ( '0', c ) .and. lle ( c, '9' ) ) then
        istate = 2
        ival = ichar ( c ) - ichar ( '0' )
      else
        ierror = 1
        return
      end if
!
!  have read the sign, expecting digits.
!
    else if ( istate == 1 ) then

      if ( c == ' ' ) then

      else if ( lle ( '0', c ) .and. lle ( c, '9' ) ) then
        istate = 2
        ival = ichar ( c ) - ichar ( '0' )
      else
        ierror = 1
        return
      end if
!
!  have read at least one digit, expecting more.
!
    else if ( istate == 2 ) then

      if ( lle ( '0', c ) .and. lle ( c, '9' ) ) then
        ival = 10 * ival + ichar ( c ) - ichar ( '0' )
      else
        ival = isgn * ival
        length = i - 1
        return
      end if

    end if

  end do
!
!  if we read all the characters in the string, see if we're ok.
!
  if ( istate == 2 ) then
    ival = isgn * ival
    length = len_trim ( s )
  else
    ierror = 1
    length = 0
  end if

  return
end subroutine
subroutine s_to_r8 ( s, dval, ierror, length )

!*****************************************************************************80
!
!! s_to_r8 reads an r8 from a string.
!
!  discussion:
!
!    the routine will read as many characters as possible until it reaches
!    the end of the string, or encounters a character which cannot be
!    part of the number.
!
!    legal input is:
!
!       1 blanks,
!       2 '+' or '-' sign,
!       2.5 blanks
!       3 integer part,
!       4 decimal point,
!       5 fraction part,
!       6 'e' or 'e' or 'd' or 'd', exponent marker,
!       7 exponent sign,
!       8 exponent integer part,
!       9 exponent decimal point,
!      10 exponent fraction part,
!      11 blanks,
!      12 final comma or semicolon,
!
!    with most quantities optional.
!
!  example:
!
!    s                 dval
!
!    '1'               1.0
!    '     1   '       1.0
!    '1a'              1.0
!    '12,34,56'        12.0
!    '  34 7'          34.0
!    '-1e2abcd'        -100.0
!    '-1x2abcd'        -1.0
!    ' 2e-1'           0.2
!    '23.45'           23.45
!    '-4.2e+2'         -420.0
!    '17d2'            1700.0
!    '-14e-2'         -0.14
!    'e2'              100.0
!    '-12.73e-9.23'   -12.73 * 10.0**(-9.23)
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    07 september 2004
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    input, character ( len = * ) s, the string containing the
!    data to be read.  reading will begin at position 1 and
!    terminate at the end of the string, or when no more
!    characters can be read to form a legal real.  blanks,
!    commas, or other nonnumeric data will, in particular,
!    cause the conversion to halt.
!
!    output, real ( kind = 8 ) dval, the value read from the string.
!
!    output, integer ( kind = 4 ) ierror, error flag.
!    0, no errors occurred.
!    1, 2, 6 or 7, the input number was garbled.  the
!    value of ierror is the last type of input successfully
!    read.  for instance, 1 means initial blanks, 2 means
!    a plus or minus sign, and so on.
!
!    output, integer ( kind = 4 ) length, the number of characters read
!    to form the number, including any terminating
!    characters such as a trailing comma or blanks.
!
  implicit none

  character              c
  !logical                ch_eqi
  real      ( kind = 8 ) dval
  integer   ( kind = 4 ) ierror
  integer   ( kind = 4 ) ihave
  integer   ( kind = 4 ) isgn
  integer   ( kind = 4 ) iterm
  integer   ( kind = 4 ) jbot
  integer   ( kind = 4 ) jsgn
  integer   ( kind = 4 ) jtop
  integer   ( kind = 4 ) length
  integer   ( kind = 4 ) nchar
  integer   ( kind = 4 ) ndig
  real      ( kind = 8 ) rbot
  real      ( kind = 8 ) rexp
  real      ( kind = 8 ) rtop
  character ( len = * )  s

  nchar = len_trim ( s )

  ierror = 0
  dval = 0.0d+00
  length = -1
  isgn = 1
  rtop = 0
  rbot = 1
  jsgn = 1
  jtop = 0
  jbot = 1
  ihave = 1
  iterm = 0

  do

    length = length + 1

    if ( nchar < length+1 ) then
      exit
    end if

    c = s(length+1:length+1)
!
!  blank character.
!
    if ( c == ' ' ) then

      if ( ihave == 2 ) then

      else if ( ihave == 6 .or. ihave == 7 ) then
        iterm = 1
      else if ( 1 < ihave ) then
        ihave = 11
      end if
!
!  comma.
!
    else if ( c == ',' .or. c == ';' ) then

      if ( ihave /= 1 ) then
        iterm = 1
        ihave = 12
        length = length + 1
      end if
!
!  minus sign.
!
    else if ( c == '-' ) then

      if ( ihave == 1 ) then
        ihave = 2
        isgn = -1
      else if ( ihave == 6 ) then
        ihave = 7
        jsgn = -1
      else
        iterm = 1
      end if
!
!  plus sign.
!
    else if ( c == '+' ) then

      if ( ihave == 1 ) then
        ihave = 2
      else if ( ihave == 6 ) then
        ihave = 7
      else
        iterm = 1
      end if
!
!  decimal point.
!
    else if ( c == '.' ) then

      if ( ihave < 4 ) then
        ihave = 4
      else if ( 6 <= ihave .and. ihave <= 8 ) then
        ihave = 9
      else
        iterm = 1
      end if
!
!  scientific notation exponent marker.
!
    else if ( ch_eqi ( c, 'e' ) .or. ch_eqi ( c, 'd' ) ) then

      if ( ihave < 6 ) then
        ihave = 6
      else
        iterm = 1
      end if
!
!  digit.
!
    else if (  ihave < 11 .and. lle ( '0', c ) .and. lle ( c, '9' ) ) then

      if ( ihave <= 2 ) then
        ihave = 3
      else if ( ihave == 4 ) then
        ihave = 5
      else if ( ihave == 6 .or. ihave == 7 ) then
        ihave = 8
      else if ( ihave == 9 ) then
        ihave = 10
      end if

      call ch_to_digit ( c, ndig )

      if ( ihave == 3 ) then
        rtop = 10.0d+00 * rtop + real ( ndig, kind = 8 )
      else if ( ihave == 5 ) then
        rtop = 10.0d+00 * rtop + real ( ndig, kind = 8 )
        rbot = 10.0d+00 * rbot
      else if ( ihave == 8 ) then
        jtop = 10 * jtop + ndig
      else if ( ihave == 10 ) then
        jtop = 10 * jtop + ndig
        jbot = 10 * jbot
      end if
!
!  anything else is regarded as a terminator.
!
    else
      iterm = 1
    end if
!
!  if we haven't seen a terminator, and we haven't examined the
!  entire string, go get the next character.
!
    if ( iterm == 1 ) then
      exit
    end if

  end do
!
!  if we haven't seen a terminator, and we have examined the
!  entire string, then we're done, and length is equal to nchar.
!
  if ( iterm /= 1 .and. length+1 == nchar ) then
    length = nchar
  end if
!
!  number seems to have terminated.  have we got a legal number?
!  not if we terminated in states 1, 2, 6 or 7!
!
  if ( ihave == 1 .or. ihave == 2 .or. ihave == 6 .or. ihave == 7 ) then
    ierror = ihave
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 's_to_r8 - serious error!'
    write ( *, '(a)' ) '  illegal or nonnumeric input:'
    write ( *, '(a)' ) '    ' // trim ( s )
    return
  end if
!
!  number seems ok.  form it.
!
  if ( jtop == 0 ) then
    rexp = 1.0d+00
  else
    if ( jbot == 1 ) then
      rexp = 10.0d+00 ** ( jsgn * jtop )
    else
      rexp = 10.0d+00 ** ( real ( jsgn * jtop, kind = 8 ) &
        / real ( jbot, kind = 8 ) )
    end if
  end if

  dval = real ( isgn, kind = 8 ) * rexp * rtop / rbot

  return
end subroutine
subroutine scqf ( nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, &
  b )

!*****************************************************************************80
!
!! scqf scales a quadrature formula to a nonstandard interval.
!
!  discussion:
!
!    the arrays wts and swts may coincide.
!
!    the arrays t and st may coincide.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    27 december 2009
!
!  author:
!
!    original fortran77 version by sylvan elhay, jaroslav kautsky.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    sylvan elhay, jaroslav kautsky,
!    algorithm 655: iqpack, fortran subroutines for the weights of
!    interpolatory quadrature,
!    acm transactions on mathematical software,
!    volume 13, number 4, december 1987, pages 399-415.
!
!  parameters:
!
!    input, integer ( kind = 4 ) nt, the number of knots.
!
!    input, real ( kind = 8 ) t(nt), the original knots.
!
!    input, integer ( kind = 4 ) mlt(nt), the multiplicity of the knots.
!
!    input, real ( kind = 8 ) wts(nwts), the weights.
!
!    input, integer ( kind = 4 ) nwts, the number of weights.
!
!    input, integer ( kind = 4 ) ndx(nt), used to index the array wts.
!    for more details see the comments in cawiq.
!
!    output, real ( kind = 8 ) swts(nwts), the scaled weights.
!
!    output, real ( kind = 8 ) st(nt), the scaled knots.
!
!    input, integer ( kind = 4 ) kind, the rule.
!    1, legendre,             (a,b)       1.0
!    2, chebyshev type 1,     (a,b)       ((b-x)*(x-a))^(-0.5)
!    3, gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
!    4, jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
!    5, generalized laguerre, (a,+oo)     (x-a)^alpha*exp(-b*(x-a))
!    6, generalized hermite,  (-oo,+oo)   |x-a|^alpha*exp(-b*(x-a)^2)
!    7, exponential,          (a,b)       |x-(a+b)/2.0|^alpha
!    8, rational,             (a,+oo)     (x-a)^alpha*(x+b)^beta
!    9, chebyshev type 2,     (a,b)       ((b-x)*(x-a))^(+0.5)
!
!    input, real ( kind = 8 ) alpha, the value of alpha, if needed.
!
!    input, real ( kind = 8 ) beta, the value of beta, if needed.
!
!    input, real ( kind = 8 ) a, b, the interval endpoints.
!
  implicit none

  integer ( kind = 4 ) nt
  integer ( kind = 4 ) nwts

  real    ( kind = 8 ) a
  real    ( kind = 8 ) al
  real    ( kind = 8 ) alpha
  real    ( kind = 8 ) b
  real    ( kind = 8 ) be
  real    ( kind = 8 ) beta
  integer ( kind = 4 ) i
  integer ( kind = 4 ) k
  integer ( kind = 4 ) kind
  integer ( kind = 4 ) l
  integer ( kind = 4 ) mlt(nt)
  integer ( kind = 4 ) ndx(nt)
  real    ( kind = 8 ) p
  real    ( kind = 8 ) shft
  real    ( kind = 8 ) slp
  real    ( kind = 8 ) st(nt)
  real    ( kind = 8 ) swts(nwts)
  real    ( kind = 8 ) t(nt)
  real    ( kind = 8 ) temp
  real    ( kind = 8 ) tmp
  real    ( kind = 8 ) wts(nwts)

  temp = epsilon ( temp )

  call parchk ( kind, 1, alpha, beta )

  if ( kind == 1 ) then

    al = 0.0d+00
    be = 0.0d+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  |b - a| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0d+00
    slp = ( b - a ) / 2.0d+00

  else if ( kind == 2 ) then

    al = -0.5d+00
    be = -0.5d+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  |b - a| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0d+00
    slp = ( b - a ) / 2.0d+00

  else if ( kind == 3 ) then

    al = alpha
    be = alpha

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  |b - a| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0d+00
    slp = ( b - a ) / 2.0d+00

  else if ( kind == 4 ) then

    al = alpha
    be = beta

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  |b - a| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0d+00
    slp = ( b - a ) / 2.0d+00

  else if ( kind == 5 ) then

    if ( b <= 0.0d+00 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  b <= 0'
      stop
    end if

    shft = a
    slp = 1.0d+00 / b
    al = alpha
    be = 0.0d+00

  else if ( kind == 6 ) then

    if ( b <= 0.0d+00 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  b <= 0.'
      stop
    end if

    shft = a
    slp = 1.0d+00 / sqrt ( b )
    al = alpha
    be = 0.0d+00

  else if ( kind == 7 ) then

    al = alpha
    be = 0.0d+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  |b - a| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0d+00
    slp = ( b - a ) / 2.0d+00

  else if ( kind == 8 ) then

    if ( a + b <= 0.0d+00 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  a + b <= 0.'
      stop
    end if

    shft = a
    slp = a + b
    al = alpha
    be = beta

  else if ( kind == 9 ) then

    al = 0.5d+00
    be = 0.5d+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'scqf - fatal error!'
      write ( *, '(a)' ) '  |b - a| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0d+00
    slp = ( b - a ) / 2.0d+00

  end if

  p = slp**( al + be + 1.0d+00 )

  do k = 1, nt

    st(k) = shft + slp * t(k)
    l = abs ( ndx(k) )

    if ( l /= 0 ) then
      tmp = p
      do i = l, l + mlt(k) - 1
        swts(i) = wts(i) * tmp
        tmp = tmp * slp
      end do
    end if

  end do

  return
end subroutine
subroutine sgqf ( nt, aj, bj, zemu, t, wts )

!*****************************************************************************80
!
!! sgqf computes knots and weights of a gauss quadrature formula.
!
!  discussion:
!
!    this routine computes all the knots and weights of a gauss quadrature
!    formula with simple knots from the jacobi matrix and the zero-th
!    moment of the weight function, using the golub-welsch technique.
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    04 january 2010
!
!  author:
!
!    original fortran77 version by sylvan elhay, jaroslav kautsky.
!    fortran90 version by john burkardt.
!
!  reference:
!
!    sylvan elhay, jaroslav kautsky,
!    algorithm 655: iqpack, fortran subroutines for the weights of
!    interpolatory quadrature,
!    acm transactions on mathematical software,
!    volume 13, number 4, december 1987, pages 399-415.
!
!  parameters:
!
!    input, integer ( kind = 4 ) nt, the number of knots.
!
!    input, real ( kind = 8 ) aj(nt), the diagonal of the jacobi matrix.
!
!    input/output, real ( kind = 8 ) bj(nt), the subdiagonal of the jacobi
!    matrix, in entries 1 through nt-1.  on output, bj has been overwritten.
!
!    input, real ( kind = 8 ) zemu, the zero-th moment of the weight function.
!
!    output, real ( kind = 8 ) t(nt), the knots.
!
!    output, real ( kind = 8 ) wts(nt), the weights.
!
  implicit none

  integer ( kind = 4 ) nt

  real    ( kind = 8 ) aj(nt)
  real    ( kind = 8 ) bj(nt)
  integer ( kind = 4 ) i
  real    ( kind = 8 ) t(nt)
  real    ( kind = 8 ) wts(nt)
  real    ( kind = 8 ) zemu
!
!  exit if the zero-th moment is not positive.
!
  if ( zemu <= 0.0d+00 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'sgqf - fatal error!'
    write ( *, '(a)' ) '  zemu <= 0.'
    stop
  end if
!
!  set up vectors for imtqlx.
!
  t(1:nt) = aj(1:nt)

  wts(1) = sqrt ( zemu )
  wts(2:nt) = 0.0d+00
!
!  diagonalize the jacobi matrix.
!
  call imtqlx ( nt, t, bj, wts )

  wts(1:nt) = wts(1:nt)**2

  return
end subroutine
subroutine timestamp ( )

!*****************************************************************************80
!
!! timestamp prints the current ymdhms date as a time stamp.
!
!  example:
!
!    may 31 2001   9:45:54.872 am
!
!  licensing:
!
!    this code is distributed under the gnu lgpl license.
!
!  modified:
!
!    31 may 2001
!
!  author:
!
!    john burkardt
!
!  parameters:
!
!    none
!
  implicit none

  character ( len = 8 )  ampm
  integer   ( kind = 4 ) d
  character ( len = 8 )  date
  integer   ( kind = 4 ) h
  integer   ( kind = 4 ) m
  integer   ( kind = 4 ) mm
  character ( len = 9 ), parameter, dimension(12) :: month = (/ &
    'january  ', 'february ', 'march    ', 'april    ', &
    'may      ', 'june     ', 'july     ', 'august   ', &
    'september', 'october  ', 'november ', 'december ' /)
  integer   ( kind = 4 ) n
  integer   ( kind = 4 ) s
  character ( len = 10 ) time
  integer   ( kind = 4 ) values(8)
  integer   ( kind = 4 ) y
  character ( len = 5 )  zone

  call date_and_time ( date, time, zone, values )

  y = values(1)
  m = values(2)
  d = values(3)
  h = values(5)
  n = values(6)
  s = values(7)
  mm = values(8)

  if ( h < 12 ) then
    ampm = 'am'
  else if ( h == 12 ) then
    if ( n == 0 .and. s == 0 ) then
      ampm = 'noon'
    else
      ampm = 'pm'
    end if
  else
    h = h - 12
    if ( h < 12 ) then
      ampm = 'pm'
    else if ( h == 12 ) then
      if ( n == 0 .and. s == 0 ) then
        ampm = 'midnight'
      else
        ampm = 'am'
      end if
    end if
  end if

  write ( *, '(a,1x,i2,1x,i4,2x,i2,a1,i2.2,a1,i2.2,a1,i3.3,1x,a)' ) &
    trim ( month(m) ), d, y, h, ':', n, ':', s, '.', mm, trim ( ampm )

  return
end subroutine

 end module gauss_quadrature



