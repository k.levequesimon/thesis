       real*8 function wig3j(a1,a2,a3,b1,b2,b3)                                                                   
       implicit real*8(a-c,e-h,o-z),                                            
     1          real*16(d)                                                      
       parameter(ngam=400)                                                      
       dimension k(3), l(2)                                                     
       common /gamma_coeff/ gam(ngam),jgam(ngam)                                      
       data zero,   eps1,  eps2,   half,  one,   sc,i/                          
     1     0.0d0,1.0d-20,1.0d-3,0.501d0,1.0d0,1.0d3,1/                          
       data done,  dsc,  dsc1/                                                  
     1     1.0q0,1.0d3,1.0d-3/                                                  
       call delta2(a1,a2,a3,t,j)                                                
       if(t-eps1)1,1,2                                                          
    1  wig3j=zero                                                               
       return                                                                   
    2  ab1=dabs(b1)                                                             
       if(a1-ab1+eps2)1,3,3                                                     
    3  ab2=dabs(b2)                                                             
       if(a2-ab2+eps2)1,4,4                                                     
    4  if(a3-dabs(b3)+eps2)1,5,5                                                
    5  n=dabs(b1+b2+b3)+one-eps2                                                
       if(n)1,6,1                                                               
    6  n1=a1+b1+eps2                                                            
       n11=a1+b1+half                                                           
       n3=a2+b2+eps2                                                            
       n31=a2+b2+half                                                           
       n5=a3+b3+eps2                                                            
       n51=a3+b3+half                                                           
       ng2=n1+n3+n5                                                             
       if(ng2-n11-n31-n51)1,7,1                                                 
    7  if(ab1+ab2)31,31,30                                                      
   30  n2=a1-b1+eps2                                                            
       n4=a2-b2+eps2                                                            
       n6=a3-b3+eps2                                                            
       k(1)=n2                                                                  
       k(2)=n3                                                                  
       k(3)=n1+n3-n6                                                            
       c1=a3-a2+b1                                                              
       if(c1)8,9,9                                                              
    8  l(1)=c1-eps2                                                             
       go to 10                                                                 
    9  l(1)=c1+eps2                                                             
   10  c1=a3-a1-b2                                                              
       if(c1)11,12,12                                                           
   11  l(2)=c1-eps2                                                             
       go to 13                                                                 
   12  l(2)=c1+eps2                                                             
   13  nmin=0                                                                   
       do 16 n=1,2                                                              
       if(l(n))14,16,16                                                         
   14  if(l(n)+nmin)15,16,16                                                    
   15  nmin=-l(n)                                                               
   16  continue                                                                 
       nmax=k(1)                                                                
       do 18 n=2,3                                                              
       if(k(n)-nmax)17,18,18                                                    
   17  nmax=k(n)                                                                
   18  continue                                                                 
       t=t*gam(n1+i)*gam(n2+i)*gam(n3+i)*gam(n4+i)*gam(n5+i)*gam(n6+i)          
       t=dsqrt(t)                                                               
       n=n1-n4+nmin                                                             
       if(2*(n/2)-n)19,20,19                                                    
   19  t=-t                                                                     
   20  j=j+jgam(n1+i)+jgam(n2+i)+jgam(n3+i)+jgam(n4+i)+jgam(n5+i)               
     1 +jgam(n6+i)                                                              
       n1=k(1)-nmin                                                             
       n2=l(1)+nmin                                                             
       n3=k(2)-nmin                                                             
       n4=l(2)+nmin                                                             
       n5=k(3)-nmin                                                             
       t=t/(gam(nmin+i)*gam(n1+i)*gam(n2+i)*gam(n3+i)*gam(n4+i)                 
     1 *gam(n5+i))                                                              
       m1=jgam(nmin+i)+jgam(n1+i)+jgam(n2+i)+jgam(n3+i)+jgam(n4+i)              
     1 +jgam(n5+i)                                                              
       if(nmax-nmin)21,21,22                                                    
   21  wig3j=t*sc**(j-m1-m1)                                                    
       return                                                                   
   22  d1=n1+i                                                                  
       d2=n2                                                                    
       d3=n3+i                                                                  
       d4=n4                                                                    
       d5=n5+i                                                                  
       d6=nmin                                                                  
       dt=done                                                                  
       dsum=done                                                                
       nmax1=nmax-nmin                                                          
       dsc2=dsc*dsc                                                             
       do 23 n=1,nmax1                                                          
       d1=d1-done                                                               
       d2=d2+done                                                               
       d3=d3-done                                                               
       d4=d4+done                                                               
       d5=d5-done                                                               
       d6=d6+done                                                               
       dt=-dt*d1*d3*d5/(d2*d4*d6)                                               
       dsum=dsum+dt                                                             
   24  if(dsum*dsum-dsc2)23,23,25                                               
   25  dsum=dsum*dsc1                                                           
       dt=dt*dsc1                                                               
       j=j+1                                                                    
       go to 24                                                                 
   23  continue                                                                 
       sum=dsum                                                                 
       t=t*sum                                                                  
       go to 21                                                                 
   31  ng=ng2/2                                                                 
       ng1=(ng2+1)/2                                                            
       if(ng-ng1)1,32,32                                                        
   32  n2=ng-n1                                                                 
       n4=ng-n3                                                                 
       n6=ng-n5                                                                 
       t=dsqrt(t)*gam(ng+i)/(gam(n2+i)*gam(n4+i)*gam(n6+i))                     
       m1=jgam(n2+i)+jgam(n4+i)+jgam(n6+i)-jgam(ng+i)                           
       if(2*(ng/2)-ng)33,21,33                                                  
   33  t=-t                                                                     
       go to 21                                                                 
       end  
      
       subroutine gamaf                                                                  
       implicit real*8(a-h,o-z)                                                 
       parameter(ngam=400)                                                      
       common /gamma_coeff/ gam(ngam),jgam(ngam)                                      
       data sc1,sc2,one/1.0d6,1.0d-6,1.0d0/                                     
       gam(1)=one                                                               
       jgam(1)=0                                                                
       do 4 n=2,ngam                                                            
       n1=n-1                                                                   
       x=n1                                                                     
       x1=x*gam(n1)                                                             
       j1=jgam(n1)                                                              
    1  if(x1-sc1)3,2,2                                                          
    2  x1=sc2*x1                                                                
       j1=j1+1                                                                  
       go to 1                                                                  
    3  gam(n)=x1                                                                
       jgam(n)=j1                                                               
    4  continue                                                                 
       return                                                                   
       end
       
       subroutine delta2(a,b,c,t,j)                                             
       implicit real*8(a-h,o-z)                                                 
       parameter(ngam=400)                                                      
       common /gamma_coeff/ gam(ngam),jgam(ngam)                                      
       data zero,   eps,   oneh,i/                                              
     1     0.0d0,1.0d-3,1.501d0,1/                                              
       a1=a+b-c+eps                                                             
       if(a1)5,1,1                                                              
    1  a2=a-b+c+eps                                                             
       if(a2)5,2,2                                                              
    2  a3=b-a+c+eps                                                             
       if(a3)5,3,3                                                              
    3  j1=a1                                                                    
       j2=a2                                                                    
       j3=a3                                                                    
       j4=j1+j2+j3+i                                                            
       j5=a1+a2+a3+oneh                                                         
       if(j4-j5)5,4,5                                                           
    4  t=gam(j1+i)*gam(j2+i)*gam(j3+i)/gam(j4+i)                                
       j=jgam(j1+i)+jgam(j2+i)+jgam(j3+i)-jgam(j4+i)                            
       return                                                                   
    5  t=zero                                                                   
       j=0                                                                      
       return                                                                   
       end        

       subroutine seconds(secs)
       integer secs
       integer mins,i
       integer days(11)
       integer date_time(8)
       character*8 date
       character*10 time
       character*5 zone
       call date_and_time(date,time,zone,date_time)
       days(1)=31
       days(2)=28
       if(date_time(1).eq.1996)days(2)=29
       days(3)=31
       days(4)=30
       days(5)=31
       days(6)=30
       days(7)=31
       days(8)=31
       days(9)=30
       days(10)=31
       days(11)=30
       mins=0
       do i=1,date_time(2)-1
       mins=mins+days(i)*24*60
       enddo
       mins=mins+(date_time(3)-1)*24*60
       mins=mins+(date_time(5)-1)*60
       mins=mins+date_time(6)
       secs=mins*60+date_time(7)
       return
       end       