program principal
      use ipcms
      use lcpmr
      use lsi
      call mpi_init(ierr)
      call mpi_comm_size(mpi_comm_world,nproc,ierr)
      call mpi_comm_rank(mpi_comm_world,rank,ierr)      
      open(unit=1,file="para",form="formatted")
        read(1,*) nonp
        read(1,*) ngpt
        read(1,*) ngpx(1)
        read(1,*) ngpu
        read(1,*) dgam(1)
        read(1,*) dgam(2)
        read(1,*) par
        read(1,"(A)") id
        read(1,*) np
        read(1,*) lp
        read(1,*) nh
        read(1,*) lh
        read(1,*) 
        read(1,*) pmin
        read(1,"(A)") acs
        read(1,"(A)") ais
        read(1,"(A)") afs
        read(1,*)
        read(1,*)
        read(1,*)
        read(1,*)
        read(1,*)
        read(1,*)
        read(1,*) form
        read(1,*) mode
        read(1,*) sens
        read(1,*) target
        read(1,*) approx
        read(1,*) basis
        read(1,*) method
      close(1)
      call acces
      solved_in_mp_mh=.false.
      use_Ps_energy=.false.
      PsX_target=.false.
      if ((target=="PsCl").or.(target=="PsH")) PsX_target=.true.
      modified_coulomb=.true.
      compute_qp2=.false.
      if (par.le.1) call section("incidence")
      if (par.eq.2) call section("resonance")
      if (par.eq.3) call data_add
      if (par.eq.4) then
        modified_coulomb=.false.
        compute_qp2=.true.
        behavior="+"
        call acces
        if (compute_qp2.eqv..true.) then
          call section("incidence")
        else 
          call hydrogenic_atoms_ionization
        end if  
      end if
      if (par.eq.5) call para_inp
      if (par.eq.6) call biraben
      if (par.eq.7) call master_hf
      if (par.eq.8) call data_mendeleiev(0)
      if (par.eq.9) call data_abinit
      if (par.eq.10) call test  
      call mpi_finalize(ierr)
end program principal
