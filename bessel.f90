      function bessj(n,x)
      implicit none
      integer, parameter :: iacc = 40
      real*8, parameter :: bigno = 1.d10, bigni = 1.d-10
      integer m, n, j, jsum
      real *8 x,bessj,bessj0,bessj1,tox,bjm,bj,bjp,sum
      if (n.eq.0) then
      bessj = bessj0(x)
      return
      endif
      if (n.eq.1) then
      bessj = bessj1(x)
      return
      endif
      if (x.eq.0.) then
      bessj = 0.
      return
      endif
      tox = 2./x
      if (x.gt.float(n)) then
      bjm = bessj0(x)
      bj  = bessj1(x)
      do 11 j = 1,n-1
      bjp = j*tox*bj-bjm
      bjm = bj
      bj  = bjp
   11 continue
      bessj = bj
      else
      m = 2*((n+int(sqrt(float(iacc*n))))/2)
      bessj = 0.
      jsum = 0
      sum = 0.
      bjp = 0.
      bj  = 1.
      do 12 j = m,1,-1
      bjm = j*tox*bj-bjp
      bjp = bj
      bj  = bjm
      if (abs(bj).gt.bigno) then
      bj  = bj*bigni
      bjp = bjp*bigni
      bessj = bessj*bigni
      sum = sum*bigni
      endif
      if (jsum.ne.0) sum = sum+bj
      jsum = 1-jsum
      if (j.eq.n) bessj = bjp
   12 continue
      sum = 2.*sum-bj
      bessj = bessj/sum
      endif
      return
      end

      function bessj0 (x)
      implicit none
      real *8 x,bessj0,ax,fr,fs,z,fp,fq,xx
      real *8 y,p1,p2,p3,p4,p5,r1,r2,r3,r4,r5,r6  &
               ,q1,q2,q3,q4,q5,s1,s2,s3,s4,s5,s6
      data p1,p2,p3,p4,p5 /1.d0,-.1098628627d-2,.2734510407d-4, &
      -.2073370639d-5,.2093887211d-6 /
      data q1,q2,q3,q4,q5 /-.1562499995d-1,.1430488765d-3, &
      -.6911147651d-5,.7621095161d-6,-.9349451520d-7 /
      data r1,r2,r3,r4,r5,r6 /57568490574.d0,-13362590354.d0, &
      651619640.7d0,-11214424.18d0,77392.33017d0,-184.9052456d0 /
      data s1,s2,s3,s4,s5,s6 /57568490411.d0,1029532985.d0, &
      9494680.718d0,59272.64853d0,267.8532712d0,1.d0 /
      if(x.eq.0.d0) go to 1
      ax = abs (x)
      if (ax.lt.8.) then
      y = x*x
      fr = r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6))))
      fs = s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6))))
      bessj0 = fr/fs
      else
      z = 8./ax
      y = z*z
      xx = ax-.785398164
      fp = p1+y*(p2+y*(p3+y*(p4+y*p5)))
      fq = q1+y*(q2+y*(q3+y*(q4+y*q5)))
      bessj0 = sqrt(.636619772/ax)*(fp*cos(xx)-z*fq*sin(xx))
      endif
      return
    1 bessj0 = 1.d0
      return
      end

      function bessj1 (x)
      implicit none
      real *8 x,bessj1,ax,fr,fs,z,fp,fq,xx
      real *8 y,p1,p2,p3,p4,p5,p6,r1,r2,r3,r4,r5,r6  &
               ,q1,q2,q3,q4,q5,s1,s2,s3,s4,s5,s6
      data p1,p2,p3,p4,p5 /1.d0,.183105d-2,-.3516396496d-4,  &
      .2457520174d-5,-.240337019d-6 /,p6 /.636619772d0 /
      data q1,q2,q3,q4,q5 /.04687499995d0,-.2002690873d-3,   &
      .8449199096d-5,-.88228987d-6,.105787412d-6 /
      data r1,r2,r3,r4,r5,r6 /72362614232.d0,-7895059235.d0, & 
      242396853.1d0,-2972611.439d0,15704.48260d0,-30.16036606d0 /
      data s1,s2,s3,s4,s5,s6 /144725228442.d0,2300535178.d0, &
      18583304.74d0,99447.43394d0,376.9991397d0,1.d0 /
      ax = abs(x)
      if (ax.lt.8.) then
      y = x*x
      fr = r1+y*(r2+y*(r3+y*(r4+y*(r5+y*r6))))
      fs = s1+y*(s2+y*(s3+y*(s4+y*(s5+y*s6))))
      bessj1 = x*(fr/fs)
      else
      z = 8./ax
      y = z*z
      xx = ax-2.35619491
      fp = p1+y*(p2+y*(p3+y*(p4+y*p5)))
      fq = q1+y*(q2+y*(q3+y*(q4+y*q5)))
      bessj1 = sqrt(p6/ax)*(cos(xx)*fp-z*sin(xx)*fq)*sign(s6,x)
      endif
      return
      end
      