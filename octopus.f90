module lsi
use ipcms
  contains

subroutine data_octopus
      implicit real*8 (a-h,o-z)
      real*8, dimension(:,:), allocatable :: u,v,interp
      integer*4                           :: k,kmax,i,j,k_mpi,no
      integer                             :: nr
      double precision                    :: lagint
      character(len=50)                   :: filename
      logical                             :: average
      dimension conductivity(2),ft(2)
      external lagint
      path(4)="/home/kevin/Desktop/jean-zay/transient_absorption_nk8_etsf/"
      filename="800"
      average=.false.
      kmax=0
      open(unit=1,file=trim(path(4))//"conductivity_"//trim(filename),form="formatted",iostat=ierr)
        do while (ierr==0)
          read(1,*,iostat=ierr)
          if (ierr==0) kmax=kmax+1
        end do
      close(1)
      allocate(u(1:3,1:kmax))
      open(unit=2,file=trim(path(4))//"conductivity_"//trim(filename),form="formatted",iostat=ierr)
      do k=1,kmax
        read(2,*) u(1,k),u(2,k),u(3,k)
      end do
      close(2)
      allocate(v(2:3,1:kmax))
      if (average.eqv..true.) then
        tau=1.65365d+3
        allocate(tu(1:ngpu),wtu(1:ngpu))      
        call cgqf(ngpu,1,0.d0,0.d0,-1.d0,1.d0,tu,wtu)    
        call mpi_comm_size(mpi_comm_world,nproc,ierr) 
        call mpi_comm_rank(mpi_comm_world,rank,ierr)
        no=kmax/nproc; nr=mod(kmax,nproc)
        do iproc=0,nr-1
          if (iproc.eq.rank) no=no+1
        end do
        allocate(interp(1:2,1:ngpu))  
        do k_mpi=1,no
          k=(k_mpi-1)*nproc+rank+1
          do i=1,ngpu
            ft(1)=dsin(tu(i))
            ft(2)=1.d0-dcos(tu(i))
            ft(:)=ft(:)*(2.d0*pi**2.d0)/(tu(i)*(4.d0*pi**2.d0-tu(i)**2.d0))
            x=u(1,k)+tu(i)/tau
            do j=1,2
             conductivity(j)=lagint(x,u(1,:),u(j+1,:),kmax,3)
            end do
            interp(1,i)=conductivity(1)*ft(1)-conductivity(2)*ft(2)
            interp(2,i)=conductivity(2)*ft(1)+conductivity(1)*ft(2)
          end do
          do j=1,2
            v(j+1,k)=dot_product(interp(j,:),wtu)
          end do
        end do
        deallocate(interp)   
        call mpi_barrier(mpi_comm_world,ierr)
        do j=1,2
          call mpi_allreduce(v(j+1,:),v(j+1,:),kmax,mpi_real8,mpi_sum,mpi_comm_world,ierr) 
        end do
        deallocate(tu,wtu)
      else
        do j=1,2
          v(j+1,:)=u(j+1,:)
        end do
      end if
      open(unit=3,file=trim(path(4))//"dielectric_function_"//trim(filename),form="formatted",iostat=ierr)
      do k=1,kmax
        write(3,100) u(1,k)*eh/2.081064553496308784d0,1.d0-v(3,k)/u(1,k)*4.d0*pi*137.035999710d0/0.01d0,v(2,k)/u(1,k)*4.d0*pi*137.035999710d0/0.01d0
      end do
      close(3)
      100 format (F13.8,2E25.12)
      deallocate(u,v)
end subroutine data_octopus

subroutine data_abinit
      implicit real*8 (a-h,o-z)
      real*8, dimension(:,:), allocatable :: c
      integer*4                           :: kmax,k,i
      dimension acell(3),rprim(3)
      data acell/24.0346124050d0,12.0173062025d0,11.09269240d0/
      data rprim/0.970234900d0,0.2421656d0,0.000000000000d0/
      open(unit=1,file="fort.10",form="formatted",iostat=ierr)
        do while (ierr==0)
          read(1,*,iostat=ierr)
          if (ierr==0) kmax=kmax+1
        end do
      close(1)
      allocate(c(1:3,1:kmax))
      open(unit=2,file="fort.10",form="formatted",iostat=ierr)
      do k=1,kmax
        read(2,*) (c(i,k),i=1,3)
      end do  
      close(2) 
      do i=1,3 
        c(i,:)=c(i,:)+acell(i)*rprim(i)
      end do 
      open(unit=3,file="fort.11",form="formatted",iostat=ierr)
      do k=1,kmax
        write(3,'(3E18.10)') (c(i,k),i=1,3)
      end do  
      close(3) 
      deallocate(c)
end subroutine data_abinit 
end module lsi
