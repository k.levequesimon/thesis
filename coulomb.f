      subroutine coulcc(xx,eta1,zlmin,nl, fc,gc,fcp,gcp, sig,           
     x                  mode1,kfn,ifail)      
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                                                                      c
c  complex coulomb wavefunction program using steed's method           c
c                                                                      c
c  a. r. barnett           manchester  march   1981                    c
c  modified i.j. thompson  daresbury, sept. 1983 for complex functions c
c                                                                      c
c  original program  rcwfn       in    cpc  8 (1974) 377-395           c
c                 +  rcwff       in    cpc 11 (1976) 141-142           c
c                 +  coulfg      in    cpc 27 (1982) 147-166           c
c  description of real algorithm in    cpc 21 (1981) 297-314           c
c  description of complex algorithm    jcp xx (1985) yyy-zzz           c
c  this version written up       in    cpc xx (1985) yyy-zzz           c
c                                                                      c
c  coulcc returns f,g,g',g',sig for complex xx, eta1, and zlmin,       c
c   for nl integer-spaced lambda values zlmin to zlmin+nl-1 inclusive, c
c   thus giving  complex-energy solutions to the coulomb schrodinger   c
c   equation,to the klein-gordon equation and to suitable forms of     c
c   the dirac equation ,also spherical & cylindrical bessel equations  c
c                                                                      c
c  if /mode1/= 1  get f,g,f',g'   for integer-spaced lambda values     c
c            = 2      f,g      unused arrays must be dimensioned in    c
c            = 3      f,  f'          call to at least length (1)      c
c            = 4      f                                                c
c            = 11 get f,h+,f',h+' ) if kfn=0, h+ = g + i.f        )    c
c            = 12     f,h+        )       >0, h+ = j + i.y = h(1) ) in c
c            = 21 get f,h-,f',h-' ) if kfn=0, h- = g - i.f        ) gc c
c            = 22     f,h-        )       >0, h- = j - i.y = h(2) )    c
c                                                                      c
c     if mode1<0 then the values returned are scaled by an exponential c
c                factor (dependent only on xx) to bring nearer unity   c
c                the functions for large /xx/, small eta & /zl/ < /xx/ c
c        define scale = (  0        if mode1 > 0                       c
c                       (  imag(xx) if mode1 < 0  &  kfn < 3           c
c                       (  dble(xx) if mode1 < 0  &  kfn = 3           c
c        then fc = exp(-abs(scale)) * ( f, j, j, or i)                 c
c         and gc = exp(-abs(scale)) * ( g, y, or y )                   c
c               or exp(scale)       * ( h+, h(1), or k)                c
c               or exp(-scale)      * ( h- or h(2) )                   c
c                                                                      c
c  if  kfn  =  0,-1  complex coulomb functions are returned   f & g    c
c           =  1   spherical bessel      "      "     "       j & y    c
c           =  2 cylindrical bessel      "      "     "       j & y    c
c           =  3 modified cyl. bessel    "      "     "       i & k    c
c                                                                      c
c          and where coulomb phase shifts put in sig if kfn=0 (not -1) c
c                                                                      c
c  the use of mode and kfn is independent                              c
c    (except that for kfn=3,  h(1) & h(2) are not given)               c
c                                                                      c
c  with negative orders lambda, coulcc can still be used but with      c
c    reduced accuracy as cf1 becomes unstable. the user is thus        c
c    strongly advised to use reflection formulae based on              c
c    h+-(zl,,) = h+-(-zl-1,,) * exp +-i(sig(zl)-sig(-zl-1)-(zl+1/2)pi) c
c                                                                      c
c  precision:  results to within 2-3 decimals of 'machine accuracy',   c
c               but if cf1a fails because x too small or eta too large c
c               the f solution  is less accurate if it decreases with  c
c               decreasing lambda (e.g. for lambda.le.-1 & eta.ne.0)   c
c              rerr in common/steed/ traces the main roundoff errors.  c
c                                                                      c
c   coulcc is coded for real*8 on ibm or equivalent  accur >= 10**-14  c
c          with a section of doubled real*16 for less roundoff errors. c
c          (if no doubled precision available, increase jmax to eg 100)c
c   use implicit complex*32 & real*16 on vs compiler accur >= 10**-32  c
c   for single precision cdc (48 bits) reassign real*8=real etc.       c
c                                                                      c
c   ifail  on input   = 0 : no printing of error messages              c
c                    ne 0 : print error messages on file 6             c
c   ifail  in output = -2 : argument out of range                      c
c                    = -1 : one of the continued fractions failed,     c
c                           or arithmetic check before final recursion c
c                    =  0 : all calculations satisfactory              c
c                    ge 0 : results available for orders up to & at    c
c                             position nl-ifail in the output arrays.  c
c                    = -3 : values at zlmin not found as over/underflowc
c                    = -4 : roundoff errors make results meaningless   c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c                                                                      c
c     machine dependent constants :                                    c
c                                                                      c
c     accur    target bound on relative error (except near 0 crossings)c
c               (accur should be at least 100 * acc8)                  c
c     acc8     smallest number with 1+acc8 .ne.1 in real*8  arithmetic c
c     acc16    smallest number with 1+acc16.ne.1 in real*16 arithmetic c
c     fpmax    magnitude of largest floating point number * acc8       c
c     fpmin    magnitude of smallest floating point number / acc8      c
c     fplmax   log(fpmax)                                              c
c     fplmin   log(fpmin)                                              c
c                                                                      c
c     routines called :       logam/clogam/cdigam,                     c
c                             f20, cf1a, rcf, cf1c, cf2, f11, cf1r     c
c     intrinsic functions :   min, max, sqrt, real, imag, abs, log, exp,
c      (generic names)        nint, mod, atan, atan2, cos, sin, dcmplx,
c                             sign, conjg, int, tanh                   c
c     note: statement fntn.   nintc = integer nearest to a complex no. c
c                                                                      c
c     parameters determining region of calculations :                  c
c                                                                      c
c        r20      estimate of (2f0 iterations)/(cf2 iterations)        c
c        asym     minimum x/(eta**2+l) for cf1a to converge easily     c
c        xnear    minimum abs(x) for cf2 to converge accurately        c
c        limit    maximum no. iterations for cf1, cf2, and 1f1 series  c
c        jmax     size of work arrays for pade accelerations           c
c        ndrop    number of successive decrements to define instabilityc
c                                                                      c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      implicit complex*16 (a-h,o-z)
      parameter(jmax=50)
      dimension fc(nl),gc(nl),fcp(nl),gcp(nl),sig(nl),xrcf(jmax,4)
      logical pr,etane0,ifcp,rlel,donem,unstab,zlneg,axial,nocf2,npint
      real*8 err,rerr,absc,accur,acct,acc8,acch,acc16,accb, xnear,cf1r,
     x       zero,one,two,half,hpi,tlog,fpmax,fpmin,fplmin,fplmax,
     x       paccq,eps,off,scale,sf,sfsh,ta,rk,omega,r20,asym,absx
      complex*16 logam, ztmp

c
      common       /csteed/ rerr,nfp,n11,npq(2),n20,kas(2)
c***  common blocks are for information & storage only.
c     (they are not essential to working of the code)
c      common /rcfcm1/ pk,ek,clgaa,clgab,clgbb,dsig,tpk1,w,rl,fcl1,q,gam,
c     x                hcl,hpl,fcm,hcl1,alpha,beta,pl
c      equivalence            (pk,xrcf(1,1))
c
      data zero,one,two,limit /0.0d+0, 1.0d+0, 2.0d+0, 20000 /,
     x     half, ci / 0.5d+0, (0d+0, 1d+0) /,
     x     fpmax,fpmin,fplmax,fplmin / 1d+60,1d-60 ,140d+0, -140d+0 /,
     x     r20,asym,xnear,ndrop / 3., 3., .5, 5 /,
     x     accur, acc8, acc16 / 1d-14, 2d-16, 3d-33 /
      nintc(w) = nint(real(real(w)))
      absc(w) = abs(dble(w)) + abs(imag(w))
      npint(w,accb) = absc(nintc(w)-w).lt.accb .and. dble(w).lt.half
c
      mode = mod(abs(mode1),10)
      ifcp = mod(mode,2).eq.1
      pr = ifail.ne.0
      ifail = -2

      kase   = 0
      nocf2  = .false.
      omega  = 0.0
      paccq  = 0.0
      sfsh   = 0.0
      cll    = zero
      f20v   = zero
      first  = zero
      hcl    = zero
      hpl    = zero
      pq1    = zero
      pq2    = zero
      q      = zero
      thetam = zero

      n11   = 0
      nfp   = 0
      kas(1)   = 0
      kas(2)   = 0
      npq(1)   = 0
      npq(2)   = 0
      n20 = 0
      hpi = two*atan(one)
      tlog = log(two)
      accur = max(accur, 50*acc8)
      acct = accur * .5
c                       initialise the log-gamma function :
      ztmp = logam(acc8)
      acch  = sqrt(accur)
      accb  = sqrt(acch)
      rerr = acct
      
      sig = (0.0, 0.0)
c
      cik = one
         if(kfn.ge.3) cik = ci * sign(one,acc8-imag(xx))
      x     = xx * cik
      eta   = eta1
      if(kfn .gt. 0) eta = zero
         etane0  = absc(eta).gt.acc8
         etai = eta*ci
      dell  = zero
      if(kfn .ge. 2)  dell = half
      zm1   = zlmin - dell
      scale = zero
      if(mode1.lt.0) scale = imag(x)
c
      m1 = 1
      l1  = m1 + nl - 1
      rlel = abs(imag(eta)) + abs(imag(zm1)) .lt. acc8
      absx = abs(x)
      axial = rlel .and. abs(imag(x)) .lt. acc8 * absx
      if(mode.le.2 .and. absx.lt.fpmin) go to 310
      xi  = one/x
      xlog = log(x)
c            log with cut along the negative real axis] see also omega
      id = 1
      donem = .false.
         unstab = .false.
      lf = m1
      ifail = -1
   10    zlm = zm1 + lf - m1
         zll = zm1 + l1 - m1
c
c ***       zll  is final lambda value, or 0.5 smaller for j,y bessels
c
              z11 = zll
              if(id.lt.0) z11 = zlm
              p11 = ci*sign(one,acc8-imag(eta))
      last = l1
c
c ***       find phase shifts and gamow factor at lambda = zll
c
      pk = zll + one
      aa = pk - etai
      ab = pk + etai
      bb = two*pk
         zlneg = npint(bb,accb)
                     clgaa = clogam(aa)
                     clgab = clgaa
         if(etane0.and..not.rlel)  clgab = clogam(ab)
         if(etane0.and.     rlel)  clgab = conjg(clgaa)
         sigma = (clgaa - clgab) * ci*half
         if(kfn.eq.0) sig(l1) = sigma
         if(.not.zlneg) cll = zll*tlog- hpi*eta - clogam(bb)
     x                                          + (clgaa+clgab)*half
              theta  = x - eta*(xlog+tlog) - zll*hpi + sigma
c
        ta = (imag(aa)**2+imag(ab)**2+abs(dble(aa))+abs(dble(ab)))*half
      if(id.gt.0 .and. absx .lt. ta*asym .and. .not.zlneg) go to 20
c
c ***         use cf1 instead of cf1a, if predicted to converge faster,
c                 (otherwise using cf1a as it treats negative lambda &
c                  recurrence-unstable cases properly)
c
           rk = sign(one, dble(x) + acc8)
           p =  theta
           if(rk.lt.0) p = -x + eta*(log(-x)+tlog)-zll*hpi-sigma
      f = rk * cf1a(x*rk,eta*rk,zll,p,acct,jmax,nfp,fest,err,fpmax,xrcf,
     x                                      xrcf(1,3), xrcf(1,4))
      fesl = log(fest) + abs(imag(x))
         nfp = - nfp
      if(nfp.lt.0   .or.(unstab.and.err.lt.accb)) go to 40
      if(.not.zlneg .or. unstab.and.err.gt.accb)  go to 20
c         if(pr) write(6,1060) '-l',err
         if(err.gt.accb) go to 280
         go to 40
c
c ***    evaluate cf1  =  f   =  f'(zll,eta,x)/f(zll,eta,x)
c
   20 if(axial) then
c                                                        real version
      f = cf1r(real(x),real(eta),real(zll),acc8,sf ,rk,
     x         etane0,limit,err,nfp, acch,fpmin,fpmax,pr,'coulcc')
          fcl = sf
          tpk1= rk
         else
c                                                        complex version
      f = cf1c(x,eta,zll,acc8,fcl,tpk1,etane0,limit,err,nfp,
     x         acch,fpmin,fpmax,pr,'coulcc')
         endif
      if(err.gt.one) go to 390
c
c ***  make a simple check for cf1 being badly unstable:
c
      if(id.lt.0) go to 30
      unstab = dble((one-eta*xi)*ci*imag(theta)/f).gt.zero
     x .and..not.axial .and. abs(imag(theta)).gt.-log(acc8)*.5
     x .and. absc(eta)+absc(zll).lt.absc(x)
      if(unstab) go to 60
c
c *** compare accumulated phase fcl with asymptotic phase for g(k+1) :
c     to determine estimate of f(zll) (with correct sign) to start recur
c
   30 w   =  x*x  *(half/tpk1 + one/tpk1**2) + eta*(eta-two*x)/tpk1
      fesl   = (zll+one) * xlog + cll - w - log(fcl)
   40 fesl = fesl - abs(scale)
          rk   =        max(dble(fesl), fplmin*half)
          fesl = dcmplx(min(rk,   fplmax*half ) , imag(fesl))
      fest= exp(fesl)
c
           rerr = max(rerr, err, acc8 * abs(dble(theta)) )
c
      fcl = fest
      fpl = fcl*f
      if(ifcp) fcp(l1) = fpl
               fc (l1) = fcl
c
c *** downward recurrence to lambda = zlm. array gc,if present,stores rl
c
      i  = max(-id, 0)
      zl  = zll + i
         mono = 0
        off = abs(fcl)
         ta = absc(sigma)
      do 70  l  = l1-id,lf,-id
         if(etane0) then
               if(rlel) then
                    dsig = atan2(dble(eta),dble(zl))
                    rl = sqrt(dble(zl)**2 + dble(eta)**2)
                  else
                    aa = zl - etai
                    bb = zl + etai
                    if(absc(aa).lt.acch.or.absc(bb).lt.acch) goto 50
                    dsig = (log(aa) - log(bb)) * ci*half
                    rl = aa * exp(ci*dsig)
                 endif
             if(absc(sigma).lt.ta*half) then
c               re-calculate sigma because of accumulating roundoffs:
                sl =(clogam(zl+i-etai)-clogam(zl+i+etai))*ci*half
                rl = (zl - etai) * exp(ci*id*(sigma - sl))
                sigma = sl
                ta = zero
              else
                sigma = sigma - dsig * id
              endif
                ta = max(ta, absc(sigma))
             sl    =  eta  + zl*zl*xi
                pl = zero
                if(absc(zl).gt.acch) pl = (sl*sl - rl*rl)/zl
             fcl1  = (fcl *sl + id*zl*fpl)/rl
              sf = abs(fcl1)
                       if(sf.gt.fpmax) go to 350
             fpl   = (fpl *sl + id*pl*fcl)/rl
             if(mode .le. 1) gcp(l+id)= pl * id
        else
c                               eta = 0, including bessels.  nb rl==sl
           rl = zl* xi
           fcl1 = fcl * rl + fpl*id
              sf = abs(fcl1)
                      if(sf.gt.fpmax) go to 350
           fpl  =(fcl1* rl - fcl) * id
        endif
c             if(absc(fcl1).lt.absc(fcl)) then
              if(sf.lt.off) then
                 mono = mono + 1
                else
                 mono = 0
                endif
         fcl   =  fcl1
           off = sf
         fc(l) =  fcl
         if(ifcp) fcp(l)  = fpl
           if(kfn.eq.0) sig(l) = sigma
           if(mode .le. 2) gc(l+id) = rl
      zl = zl - id
      if(mono.lt.ndrop) go to 70
      if(axial .or. dble(zlm)*id.gt.-ndrop.and..not.etane0) go to 70
         unstab = .true.
c
c ***    take action if cannot or should not recur below this zl:
   50    zlm = zl
         lf = l
            if(id.lt.0) go to 380
         if(.not.unstab) lf = l + 1
         if(l+mono.lt.l1-2 .or. id.lt.0 .or. .not.unstab) go to 80
c             otherwise, all l values (for stability) should be done
c                        in the reverse direction:
         go to 60
   70 continue
      go to 80
   60       id = -1
            lf = l1
            l1 = m1
            rerr = acct
            go to 10
   80 if(fcl .eq. zero) fcl = + acc8
      f  = fpl/fcl
c
c *** check, if second time around, that the 'f' values agree]
c
      if(id.gt.0) first = f
      if(donem) rerr = max(rerr, absc(f-first)/absc(f))
      if(donem) go to 90
c
       nocf2 = .false.
      thetam  = x - eta*(xlog+tlog) - zlm*hpi + sigma
c
c *** on left x-plane, determine omega by requiring cut on -x axis
c     on right x-plane, choose omega (using estimate based on thetam)
c       so h(omega) is smaller and recurs upwards accurately.
c     (x-plane boundary is shifted to give cf2(lh) a chance to converge)
c
                           omega = sign(one,imag(x)+acc8)
      if(dble(x).ge.xnear) omega = sign(one,imag(thetam)+acc8)
c     correction from erratum.
      if (axial) omega = one
c
         sfsh = exp(omega*scale - abs(scale))
         off=exp(min(two * max(abs(imag(x)),abs(imag(thetam)),
     x                         abs(imag(zlm))*3 ) , fplmax) )
          eps = max(acc8 , acct * half / off)
c
c ***    try first estimated omega, then its opposite,
c        to find the h(omega) linearly independent of f
c        i.e. maximise  cf1-cf2 = 1/(f h(omega)) , to minimise h(omega)
c
   90 do 100 l=1,2
         lh = 1
         if(omega.lt.zero) lh = 2
      pm = ci*omega
      etap = eta * pm
         if(donem) go to 130
         pq1 = zero
         paccq = one
         kase = 0
c
c ***            check for small x, i.e. whether to avoid cf2 :
c
      if(mode.ge.3 .and. absx.lt.one ) go to 190
      if(mode.lt.3 .and. (nocf2 .or. absx.lt.xnear .and.
     x   absc(eta)*absx .lt. 5 .and. absc(zlm).lt.4)) then
        kase = 5
        go to 120
        endif
c
c ***  evaluate   cf2 : pq1 = p + i.omega.q  at lambda = zlm
c
         pq1 = cf2(x,eta,zlm,pm,eps,limit,err,npq(lh),acc8,acch)
c
       err = err * max(one,absc(pq1)/max(absc(f-pq1),acc8) )
       if(err.lt.acch)       go to 110
c
c *** check if impossible to get f-pq accurately because of cancellation
               nocf2 = dble(x).lt.xnear .and. abs(imag(x)).lt.-log(acc8)
c                original guess for omega (based on thetam) was wrong
c                use kase 5 or 6 if necessary if re(x) < xnear
  100            omega = - omega
                if(unstab) go to 360
c                if(dble(x).lt.-xnear .and. pr) write(6,1060) '-x',err
  110     rerr = max(rerr,err)
c
c ***  establish case of calculation required for irregular solution
c
  120 if(kase.ge.5) go to 130
      if(dble(x) .gt. xnear) then
c          estimate errors if kase 2 or 3 were to be used:
         paccq = eps * off * absc(pq1) / max(abs(imag(pq1)),acc8)
        endif
      if(paccq .lt. accur) then
          kase = 2
          if(axial) kase = 3
      else
          kase = 1
          if(npq(1) * r20 .lt. jmax)     kase = 4
c             i.e. change to kase=4 if the 2f0 predicted to converge
      endif
  130 go to (190,140,150,170,190,190),  abs(kase)
  140    if(.not.donem)
c
c ***  evaluate   cf2 : pq2 = p - i.omega.q  at lambda = zlm   (kase 2)
c
     x  pq2 = cf2(x,eta,zlm,-pm,eps,limit,err,npq(3-lh),acc8,acch)
c
        p     = (pq2 + pq1) * half
        q     = (pq2 - pq1) * half*pm
      go to 160
  150   p     = dble(pq1)
        q     = imag(pq1)
c
c ***   with kase = 3 on the real axes, p and q are real & pq2 = pq1*
c
        pq2 = conjg(pq1)
c
c *** solve for fcm = f at lambda = zlm,then find norm factor w=fcm/fcl
c
  160 w   = (pq1 - f) * (pq2 - f)
         sf = exp(-abs(scale))
      fcm = sqrt(q / w) * sf
c                  any sqrt given here is corrected by
c                  using sign for fcm nearest to phase of fcl
      if(dble(fcm/fcl).lt.zero) fcm  = - fcm
      gam = (f - p)/q
         ta = absc(gam + pm)
         paccq= eps * max(ta,one/ta)
      hcl = fcm * (gam + pm) * (sfsh/(sf*sf))
c
      if(paccq.gt.accur .and. kase.gt.0) then
c                                    consider a kase = 1 calculation
          f11v= f11(x,eta,z11,p11,acct,limit,0,err,n11,fpmax,acc8,acc16)
          if(err.lt.paccq) go to 200
          endif
      rerr=max(rerr,paccq)
      go to 230
c
c *** arrive here if kase = 4
c     to evaluate the exponentially decreasing h(lh) directly.
c
  170  if(donem) go to 180
      aa = etap - zlm
      bb = etap + zlm + one
      f20v = f20(aa,bb,-half*pm*xi, acct,jmax,err,fpmax,n20,xrcf)
        if(n20.le.0) go to 190
        rerr = max(rerr,err)
         hcl = fpmin
         if(abs(dble(pm*thetam)+omega*scale).gt.fplmax) go to 330
  180 hcl = f20v * exp(pm * thetam + omega*scale)
      fcm = sfsh / ((f - pq1) * hcl )
      go to 230
c
c *** arrive here if kase=1   (or if 2f0 tried mistakenly & failed)
c
c           for small values of x, calculate f(x,sl) directly from 1f1
c               using real*16 arithmetic if possible.
c           where z11 = zll if id>0, or = zlm if id<0
c
  190 f11v = f11(x,eta,z11,p11,acct,limit,0,err,n11,fpmax,acc8,acc16)
c
  200       if(n11.lt.0) then
c                               f11 failed from bb = negative integer
c               write(6,1060) '-l',one
               go to 390
               endif
            if(err.gt.paccq .and. paccq.lt.accb) then
c                               consider a kase 2 or 3 calculation :
                kase = -2
                if(axial) kase = -3
                go to 130
                endif
         rerr = max(rerr, err)
         if(err.gt.fpmax) go to 370
         if(id.lt.0) cll = z11*tlog- hpi*eta - clogam(bb)
     x                       + clogam(z11 + one + p11*eta) - p11*sigma
      ek   = (z11+one)*xlog - p11*x + cll  - abs(scale)
      if(id.gt.0) ek = ek - fesl + log(fcl)
         if(dble(ek).gt.fplmax) go to 350
         if(dble(ek).lt.fplmin) go to 340
      fcm = f11v * exp(ek)
c
      if(kase.ge.5) then
        if(absc(zlm+zlm-nintc(zlm+zlm)).lt.acch) kase = 6
c
c ***  for abs(x) < xnear, then cf2 may not converge accurately, so
c ***      use an expansion for irregular soln from origin :
c
         sl = zlm
            zlneg = dble(zlm) .lt. -one + accb
         if(kase.eq.5 .or. zlneg) sl = - zlm - one
         pk = sl + one
            aa = pk - etap
            ab = pk + etap
            bb = two*pk
                     clgaa = clogam(aa)
                     clgab = clgaa
         if(etane0)  clgab = clogam(ab)
                     clgbb = clogam(bb)
           if(kase.eq.6 .and. .not.zlneg) then
              if(npint(aa,accur)) clgaa = clgab - two*pm*sigma
              if(npint(ab,accur)) clgab = clgaa + two*pm*sigma
             endif
          cll = sl*tlog- hpi*eta - clgbb + (clgaa + clgab) * half
          dsig = (clgaa - clgab) * pm*half
             if(kase.eq.6) p11 = - pm
          ek  = pk * xlog - p11*x + cll  - abs(scale)
                     sf = exp(-abs(scale))
                     chi = zero
       if(.not.( kase.eq.5 .or. zlneg ) ) go to 210
c
c *** use  g(l)  =  (cos(chi) * f(l) - f(-l-1)) /  sin(chi)
c
c      where chi = sig(l) - sig(-l-1) - (2l+1)*pi/2
c
         chi = sigma - dsig - (zlm-sl) * hpi
         f11v=f11(x,eta,sl,p11,acct,limit,0,err,npq(1),fpmax,acc8,acc16)
                    rerr = max(rerr,err)
            if(kase.eq.6) go to 210
         fesl = f11v * exp( ek )
         fcl1 = exp(pm*chi) * fcm
         hcl = fcl1 - fesl
               rerr=max(rerr,acct*max(absc(fcl1),absc(fesl))/absc(hcl))
         hcl = hcl / sin(chi) * (sfsh/(sf*sf))
       go to 220
c
c *** use the logarithmic expansion for the irregular solution (kase 6)
c        for the case that bb is integral so sin(chi) would be zero.
c
  210    rl = bb - one
         n  = nintc(rl)
         zlog = xlog + tlog - pm*hpi
         chi = chi + pm * thetam + omega * scale + ab * zlog
            aa  = one - aa
         if(npint(aa,accur)) then
            hcl = zero
         else
               if(id.gt.0 .and. .not.zlneg) f11v = fcm * exp(-ek)
            hcl = exp(chi - clgbb - clogam(aa)) * (-1)**(n+1)
     x              * ( f11v * zlog +
     x      f11(x,eta,sl,-pm,acct,limit,2,err,npq(2),fpmax,acc8,acc16))
                rerr = max(rerr,err)
            endif
         if(n.gt.0) then
             ek = chi + clogam(rl) - clgab - rl*zlog
             df =f11(x,eta,-sl-one,-pm,zero,n,0,err,l,fpmax,acc8,acc16)
             hcl = hcl + exp(ek) * df
            endif
c
  220    pq1 = f - sfsh/(fcm * hcl)
      else
           if(mode.le.2) hcl = sfsh/((f - pq1) * fcm)
           kase = 1
      endif
c
c ***  now have absolute normalisations for coulomb functions
c          fcm & hcl  at lambda = zlm
c      so determine linear transformations for functions required :
c
  230 ih = abs(mode1) / 10
        if(kfn.eq.3) ih = int((3-imag(cik))/2  + half)
      p11 = one
      if(ih.eq.1) p11 = ci
      if(ih.eq.2) p11 = -ci
                  df = - pm
      if(ih.ge.1) df = - pm + p11
          if(absc(df).lt.acch) df = zero
c
c *** normalisations for spherical or cylindrical bessel functions
c
                          alpha = zero
          if(kfn  .eq. 1) alpha = xi
          if(kfn  .ge. 2) alpha = xi*half
                          beta  = one
          if(kfn  .eq. 1) beta  = xi
          if(kfn  .ge. 2) beta  = sqrt(xi/hpi)
          if(kfn  .ge. 2 .and. dble(beta).lt.zero) beta  = - beta
c
      aa = one
      if(kfn.gt.0) aa = -p11 * beta
      if(kfn.ge.3) then
c                        calculate rescaling factors for i & k output
         p = exp((zlm+dell) * hpi * cik)
         aa= beta * hpi * p
         beta = beta / p
         q = cik * id
        endif
c                        calculate rescaling factors for gc output
      if(ih.eq.0) then
         ta = abs(scale) + imag(pm)*scale
         rk = zero
         if(ta.lt.fplmax) rk = exp(-ta)
       else
         ta = abs(scale) + imag(p11)*scale
c
         if(absc(df).gt.acch .and. ta.gt.fplmax) go to 320
         if(absc(df).gt.acch) df = df * exp(ta)
         sf = two * (lh-ih) * scale
         rk = zero
         if(sf.gt.fplmax) go to 320
         if(sf.gt.fplmin) rk = exp(sf)
      endif
c
         kas((3-id)/2) = kase
      w = fcm / fcl
         if(log(absc(w))+log(absc(fc(lf))) .lt. fplmin) go to 340
         if(mode.ge.3) go to 240
c         if(absc(f-pq1) .lt. acch*absc(f) .and. pr)
c     x        write(6,1020) lh,zlm+dell
      hpl = hcl * pq1
         if(absc(hpl).lt.fpmin.or.absc(hcl).lt.fpmin) go to 330
c
c *** idward recurrence from hcl,hpl(lf) (stored gc(l) is rl if reqd)
c *** renormalise fc,fcp at each lambda
c ***    zl   = zlm - min(id,0) here
c
  240 do 270 l = lf,l1,id
                     fcl = w* fc(l)
                      if(absc(fcl).lt.fpmin) go to 340
            if(ifcp) fpl = w*fcp(l)
                     fc(l)  = beta * fcl
            if(ifcp) fcp(l) = beta * (fpl - alpha * fcl) * cik
                     fc(l)  = tidy(fc(l),accur)
            if(ifcp) fcp(l) = tidy(fcp(l),accur)
       if(mode .ge. 3) go to 260
       if(l.eq.lf)  go to 250
                      zl = zl + id
                      zid= zl * id
                      rl = gc(l)
         if(etane0)   then
                      sl = eta + zl*zl*xi
            if(mode.eq.1) then
              pl = gcp(l)
            else
              pl = zero
              if(absc(zl).gt.acch) pl = (sl*sl - rl*rl)/zid
            endif
           hcl1     = (sl*hcl - zid*hpl) / rl
           hpl      = (sl*hpl - pl *hcl) / rl
         else
           hcl1 = rl * hcl - hpl * id
           hpl  = (hcl - rl * hcl1) * id
         endif
         hcl      = hcl1
         if(absc(hcl).gt.fpmax) go to 320
  250    gc(l) = aa * (rk * hcl + df * fcl)
      if(mode.eq.1) gcp(l) = (aa *(rk*hpl +df*fpl) - alpha * gc(l)) *cik
         gc(l) = tidy(gc(l),accur)
      if(mode.eq.1) gcp(l) = tidy(gcp(l),accur)
         if(kfn.ge.3) aa = aa * q
  260    if(kfn.ge.3) beta = - beta * q
  270  last = min(last,(l1 - l)*id)
c
c *** come here after all soft errors to determine how many l values ok
c
  280  if(id.gt.0 .or.  last.eq.0) ifail = last
       if(id.lt.0 .and. last.ne.0) ifail = -3
c
c *** come here after all errors for this l range (zlm,zll)
c
  290 if(id.gt.0 .and. lf.ne.m1) go to 300
         if(ifail.lt.0) return
c         if(rerr.gt.accb) write(6,1070) rerr
         if(rerr.gt.0.1) ifail = -4
         return
c
c *** so on first block, 'f' started decreasing monotonically,
c                        or hit bound states for low zl.
c     thus redo m1 to lf-1 in reverse direction
c      i.e. do cf1a at zlmin & cf2 at zlm (midway between zlmin & zlmax)
c
  300 id = -1
      if(.not.unstab) lf = lf - 1
      donem = unstab
      lf = min(lf,l1)
      l1 = m1
      go to 10
c
c ***    error messages
c
 310  return
 320  goto 280
 330  goto 280
 340  goto 280
 350  goto 280
 360  goto 280
 370  goto 390
 380  goto 390
 390  ifail = -1
      goto 290

c$$$
c$$$  310 if(pr) write (6,1000) xx
c$$$ 1000 format(/' coulcc: cannot calculate irregular solutions for x =',
c$$$     x 1p,2d10.2,', as abs(x) is too small'/)
c$$$      return
c$$$  320 if(pr) write(6,1010) zl+dell,'ir',hcl,'more',fpmax
c$$$ 1010 format(' coulcc: at zl =',2f8.3,' ',a2,'regular solution (',1p,
c$$$     x 2e10.1,') will be ',a4,' than',e10.1)
c$$$      go to 280
c$$$  330 if(pr) write(6,1010) zl+dell,'ir',hcl,'less',fpmin
c$$$      go to 280
c$$$  340 if(pr) write(6,1010) zl+dell,'  ',fcl,'less',fpmin
c$$$      go to 280
c$$$  350 if(pr) write(6,1010) zl+dell,'  ',fcl,'more',fpmax
c$$$      go to 280
c$$$ 1020 format('0coulcc warning: linear independence between ''f'' and ''h
c$$$     x(',i1,')'' is lost at zl =',2f7.2,' (eg. coulomb eigenstate, or cf
c$$$     x1 unstable)'/)
c$$$  360 if(pr) write(6,1030) zll+dell
c$$$ 1030 format(' coulcc: (eta&l)/x too large for cf1a, and cf1 unstable at
c$$$     x l =',2f8.2)
c$$$      go to 280
c$$$  370 if(pr) write(6,1040) z11,i
c$$$ 1040 format(' coulcc: overflow in 1f1 series at zl =',2f8.3,' at term',
c$$$     x i5)
c$$$      go to 390
c$$$  380 if(pr) write(6,1050) zlmin,zlm,zlm+one,zlmin+nl-one
c$$$ 1050 format(' coulcc: both bound-state poles and f-instabilities occur'
c$$$     x ,', or multiple instabilities present.'
c$$$     x,/,' try calling twice,  first for zl from',2f8.3,' to',2f8.3,
c$$$     x ' (incl.)',/,20x,     'second for zl from',2f8.3,' to',2f8.3)
c$$$      go to 390
c$$$  390 ifail = -1
c$$$      go to 290
c$$$ 1060 format('0coulcc warning: as ''',a2,''' reflection rules not used,
c$$$     #errors can be up to',1p,d12.2/)
c$$$ 1070 format('0coulcc warning: overall roundoff error approx.',1p,e11.1)
      end
      function cf1c(x,eta,zl,eps,fcl,tpk1,etane0,limit,err,nfp,
     x              acch,fpmin,fpmax,pr,caller)
      implicit complex*16(a-h,o-z)
      logical pr,etane0
      real*8 one,two,eps,err,acch,fpmin,fpmax,absc,small,rk,px
      character*6 caller
      data one,two / 1d+0, 2d+0 /
      absc(w) = abs(dble(w)) + abs(imag(w))
c
c
c ***    evaluate cf1  =  f   =  f'(zl,eta,x)/f(zl,eta,x)
c
c        using complex arithmetic
c
      cf1c = 0.0
      sl = 0.0
      fcl = one
      xi = one/x
      pk  = zl + one
      px  = real(pk)  + limit
   10 ek  = eta / pk
        rk2 =          one + ek*ek
      f   = (ek + pk*xi)*fcl + (fcl - one)*xi
      pk1 =  pk + one
         tpk1 = pk + pk1
      tk  = tpk1*(xi + ek/pk1)
      if(etane0) then
c ***   test ensures b1 .ne. zero for negative eta etc.; fixup is exact.
             if(absc(tk) .gt. acch)  go to 20
             fcl  = rk2/(one + (eta/pk1)**2)
             sl   = tpk1*xi * (tpk1+two)*xi
             pk   =  two + pk
             go to 10
         endif
   20 d   =  one/tk
      df  = -fcl*rk2*d
            if(dble(pk).gt.dble(zl)+two) fcl = - rk2 * sl
            fcl = fcl * d * tpk1 * xi
      f   =  f  + df
c
c ***   begin cf1 loop on pk = k = lambda + 1
c
      rk    = one
      small    = sqrt(fpmin)
   30 pk    = pk1
        pk1 = pk1 + one
         tpk1 = pk + pk1
         if(etane0) then
           ek  = eta / pk
           rk2 =          one + ek*ek
          endif
        tk  = tpk1*(xi + ek/pk1)
        d   =  tk - d*rk2
              if(absc(d) .gt. acch)             go to 40
c              if(pr) write (6,1000) caller,d,df,acch,pk,ek,eta,x
              rk= rk +   one
              if( rk .gt. two )                  go to 50
   40 d     = one/d
            fcl = fcl * d * tpk1*xi
            if(absc(fcl).lt.small) fcl = fcl / small
            if(absc(fcl).gt.fpmax) fcl = fcl / fpmax
        df  = df*(d*tk - one)
        f   = f  + df
              if( dble(pk) .gt. px ) go to 50
      if(absc(df) .ge. absc(f)*eps)             go to 30
                nfp = int(real(pk - zl - 1))
                  err = eps * sqrt(dble(nfp))
      cf1c = f
      return
   50 if(pr) write (6,1010) caller,limit,abs(x)
 1010 format(' ',a6,': cf1 has failed to converge after ',i10  ,' iterat
     xions as abs(x) =',f15.0)
      err = two
      return
      end
      function cf2(x,eta,zl,pm,eps,limit,err,npq,acc8,acch)
      implicit complex*16(a-h,o-z)
      real*8 eps,err,acc8,acch,ta,rk,
     x       absc,zero,half,one,two
      data zero,half,one,two / 0d+0, .5d+0, 1d+0, 2d+0 /
      absc(w) = abs(dble(w)) + abs(imag(w))
c
c                                    (omega)        (omega)
c *** evaluate  cf2  = p + pm.q  =  h   (eta,x)' / h   (eta,x)
c                                    zl             zl
c     where pm = omega.i
c
      ta = two*limit
      e2mm1 = eta*eta + zl*zl + zl
      etap = eta * pm
      xi = one/x
      wi = two*etap
      rk = zero
      pq = (one - eta*xi) * pm
      aa = -e2mm1 + etap
      bb = two*(x - eta + pm)
         rl = xi * pm
      if(absc(bb).lt.acch) then
         rl = rl * aa / (aa + rk + wi)
         pq = pq + rl * (bb + two*pm)
            aa = aa + two*(rk+one+wi)
            bb = bb + (two+two)*pm
            rk = rk + (two+two)
         endif
      dd = one/bb
      dl = aa*dd* rl
   10 pq    = pq + dl
         rk = rk + two
         aa = aa + rk + wi
         bb = bb + two*pm
         dd = one/(aa*dd + bb)
         dl = dl*(bb*dd - one)
            err = absc(dl)/absc(pq)
         if(err.ge.max(eps,acc8*rk*half) .and. rk.le.ta) go to 10
c
         npq   = int(rk/two)
         pq    = pq + dl
c           if(pr.and.npq.ge.limit-1 .and. err.gt.accur)
c     x             write(6,1000) caller,int(imag(pm)),npq,err,zl+dell
c 1000 format(' ',a6,': cf2(',i2,') not converged fully in ',i7,
c     x' iterations, so error in irregular solution =',1p,d11.2,' at zl
c     x=', 0p,2f8.3)
      cf2 = pq
      return
      end

      function f11(x,eta,zl,p,eps,limit,kind,err,nits,fpmax,acc8,acc16)
      implicit real*8(a-h,o-z)
      complex*16 x,eta,zl,p,aa,bb,z,f11,cdigam,ci
      complex*16 dd,g,f,ai2,bi2,t2
      logical zllin

      integer mpdim
      parameter (mpdim = 2)

      real*8 ar(mpdim), br(mpdim), gr(mpdim), gi(mpdim), dr(mpdim)
      real*8 di(mpdim), tr(mpdim), ti(mpdim), ur(mpdim), ui(mpdim)
      real*8 fi(mpdim), fi1(mpdim), mpone(mpdim)
      real*8 ai(mpdim), bi(mpdim), den(mpdim), tr2(mpdim)
      real*8 ti2(mpdim), mptmp(mpdim), zi(mpdim), zr(mpdim)

      data zero,one,two / 0d+0, 1d+0, 2d+0 /, ci / (0d+0, 1d+0) /
      
      absc(aa) = abs(dble(aa)) + abs(imag(aa))
      nintc(aa) = nint(real(real(aa)))

      f11 = zero
c
c *** evaluate the hypergeometric function 1f1
c                                        i
c            f (aa;bb; z) = sum  (aa)   z / ( (bb)  i] )
c           1 1              i       i            i
c
c     to accuracy eps with at most limit terms.
c  if kind = 0 : using extended precision but real arithmetic only,
c            1 : using normal precision in complex arithmetic,
c   or       2 : using normal complex arithmetic, but with cdigam factor
c
c  where
         aa = zl+one - eta*p
         bb = two*(zl+one)
c  and
         z  = two*p*x
c
         zllin = dble(bb).le.zero .and. abs(bb-nintc(bb)).lt.acc8**0.25
             if(.not.zllin.or.dble(bb)+limit.lt.1.5) go to 10
                nits = -1
                return
   10 if(limit.le.0) then
         f11 = zero
         err = zero
         nits= 1
         return
         endif
      ta = one
      rk = one
      if(kind.le.0.and.absc(z)*absc(aa).gt.absc(bb) * 1.0) then
         call mpdmc(one, 0d0, dr)
         call mpdmc(zero, 0d0, di)
         call mpdmc(one, 0d0, gr)
         call mpdmc(zero, 0d0, gi)
         call mpdmc(imag(aa), 0d0, ai)
         call mpdmc(dble(aa), 0d0, ar)
         call mpdmc(imag(bb), 0d0, bi)
         call mpdmc(dble(bb), 0d0, br)
         call mpdmc(zero, 0d0, fi)
         call mpdmc(one, 0d0, mpone)
         call mpdmc(dble(z), 0d0, zr)
         call mpdmc(imag(z), 0d0, zi)
         
      do 20 i=2,limit
         call mpadd(fi, mpone, fi1)

         call mpmul(br, fi1, tr)
         call mpmul(bi, fi1, ti)

         call mpmul(tr, tr, tr2)
         call mpmul(ti, tr, ti2)
         call mpadd(tr2, ti2, mptmp)
         call mpdiv(mpone, mptmp, den)

         call mpmul(ar, tr, tr2)
         call mpmul(ai, ti, ti2)
         call mpadd(tr2, ti2, mptmp)
         call mpmul(mptmp, den, ur)

         call mpmul(ai, tr, tr2)
         call mpmul(ar, ti, ti2)
         call mpsub(tr2, ti2, mptmp)
         call mpmul(mptmp, den, ui)

         call mpmul(ur, gr, tr2)
         call mpmul(ui, gi, ti2)
         call mpsub(tr2, ti2, tr)

         call mpmul(ur, gi, tr2)
         call mpmul(ui, gr, ti2)
         call mpadd(tr2, ti2, ti)

         call mpmul(zr, tr, tr2)
         call mpmul(zi, ti, ti2)
         call mpsub(tr2, ti2, gr)

         call mpmul(zr, ti, tr2)
         call mpmul(zi, tr, ti2)
         call mpadd(tr2, ti2, gi)

         call mpadd(dr, gr, dr)
         call mpadd(di, gi, di)
         
         call mpabs(gr, tr2)
         call mpabs(gi, ti2)
         call mpadd(tr2, ti2, mptmp)
         err = mptmp(1) + mptmp(2)
         if(err.gt.fpmax) go to 60

         call mpabs(dr, tr2)
         call mpabs(di, ti2)
         call mpadd(tr2, ti2, mptmp)
         rk = mptmp(1) + mptmp(2)
         ta = max(ta,rk)

         if(err.lt.rk*eps .or. i.ge.4.and.err.lt.acc16) go to 30

         call mpeq(fi1, fi)
         call mpadd(ar, mpone, ar)
         call mpadd(br, mpone, br)
 20      continue
c
 30      f11r = dr(1) + dr(2)
         f11i = di(1) + di(2)
         f11 = dcmplx(f11r, f11i)
         err = acc16 * ta / rk
c
      else
c* ---------------------------------- alternative code
c*    if real*16 arithmetic is not available, (or already using it]),
c*    then use kind > 0
         g = one
          f = one
          if(kind.ge.2) f = cdigam(aa) - cdigam(bb) - cdigam(g)
         dd = f
         do 40 i=2,limit
            ai2 = aa + (i-2)
            bi2 = bb + (i-2)
            r  = i-one
         g = g * z * ai2 / (bi2 * r)
         if(kind.ge.2)
c                              multiply by (psi(a+r)-psi(b+r)-psi(1+r))
     x        f = f + one/ai2 - one/bi2 - one/r
         t2  = g * f
         dd = dd + t2
            err = absc(t2)
               if(err.gt.fpmax) go to 60
            rk = absc(dd)
         ta = max(ta,rk)
         if(err.lt.rk*eps.or.err.lt.acc8.and.i.ge.4) go to 50
   40    continue

   50    err = acc8 * ta / rk
         f11 = dd
c* ------------------------------------------- end of alternative code
      endif
   60    nits = i
      return
      end

      function cf1r(x,eta,zl,eps,fcl,tpk1,etane0,limit,err,nfp,
     x              acch,fpmin,fpmax,pr,caller)
      implicit real*8(a-h,o-z)
      logical pr,etane0
      character*6 caller
      data one,two / 1d+0, 2d+0 /
c
c
c ***    evaluate cf1  =  f   =  f'(zl,eta,x)/f(zl,eta,x)
c
c        using real arithmetic
c
      cf1r = 0.0
      sl  = 0.0
      fcl = one
      xi = one/x
      pk  = zl + one
      px  = pk  + limit
   10 ek  = eta / pk
        rk2 =          one + ek*ek
      f   = (ek + pk*xi)*fcl + (fcl - one)*xi
      pk1 =  pk + one
         tpk1 = pk + pk1
      tk  = tpk1*(xi + ek/pk1)
      if(etane0) then
c ***   test ensures b1 .ne. zero for negative eta etc.; fixup is exact.
             if(abs(tk) .gt. acch)  go to 20
             fcl  = rk2/(one + (eta/pk1)**2)
             sl   = tpk1*xi * (tpk1+two)*xi
             pk   =  two + pk
             go to 10
         endif
   20 d   =  one/tk
      df  = -fcl*rk2*d
            if(pk.gt.zl+two) fcl = - rk2 * sl
            fcl = fcl * d * tpk1 * xi
      f   =  f  + df
c
c ***   begin cf1 loop on pk = k = lambda + 1
c
      rk    = one
      small    = sqrt(fpmin)
   30 pk    = pk1
        pk1 = pk1 + one
         tpk1 = pk + pk1
         if(etane0) then
           ek  = eta / pk
           rk2 =          one + ek*ek
          endif
        tk  = tpk1*(xi + ek/pk1)
        d   =  tk - d*rk2
              if(abs(d) .gt. acch)             go to 40
c              if(pr) write (6,1000) caller,d,df,acch,pk,ek,eta,x
              rk= rk +   one
              if( rk .gt. two )                  go to 50
   40 d     = one/d
            fcl = fcl * d * tpk1*xi
            if(abs(fcl).lt.small) fcl = fcl / small
            if(abs(fcl).gt.fpmax) fcl = fcl / fpmax
        df  = df*(d*tk - one)
        f   = f  + df
              if( pk .gt. px ) go to 50
      if(abs(df) .ge. abs(f)*eps)             go to 30
                nfp = int(pk - zl) - 1
                  err = eps * sqrt(dble(nfp))
      cf1r = f
      return
   50 if(pr) write (6,1010) caller,limit,abs(x)
 1010 format(' ',a6,': cf1 has failed to converge after ',i10  ,' iterat
     xions as abs(x) =',f15.0)
      err = two
      return
      end
      function f20(aa,bb,z,eps,jmax,re,fpmax,n,x)
c
c     evaluate the hypergeometric function 2f0
c                                             i
c            f (aa,bb;;z) = sum  (aa)  (bb)  z / i]
c           2 0              i       i     i
c
c     to accuracy eps with at most jmax terms.
c
c     if the terms start diverging,
c     the corresponding continued fraction is found by rcf
c     & evaluated progressively by steed's method to obtain convergence.
c
c      useful number also input:  fpmax = near-largest f.p. number
c
      implicit complex*16(a-h,o-z)
      dimension x(jmax,4)
      logical finite
      real*8 ep,eps,at,atl,absc,re,fpmax
      data one,zero / (1d+0,0d+0), (0d+0,0d+0) /
      absc(w) = abs(dble(w)) + abs(imag(w))
      nintc(w) = nint(real(real(w)))
c
      at = 0.0
      re = 0.0
      x(1,1) = one
      sum = x(1,1)
      atl = absc(x(1,1))
         f    = sum
         d = one
         df   = sum
      j = 0
      ep = eps * jmax *10.
      ma = - nintc(aa)
      mb = - nintc(bb)
      finite = abs(abs(dble(aa))-ma).lt.ep .and. abs(imag(aa)).lt.ep
     x    .or. abs(abs(dble(bb))-mb).lt.ep .and. abs(imag(bb)).lt.ep
      imax = jmax
      if(finite.and.ma.ge.0) imax = min(ma+1,imax)
      if(finite.and.mb.ge.0) imax = min(mb+1,imax)
      do 10 i=2,imax
      x(i,1) = x(i-1,1) * z * (aa+i-2) * (bb+i-2) / (i-1)
         if(absc(x(i,1)).gt.fpmax) go to 40
      at = absc(x(i,1))
         if(j.eq.0) then
                 sum = sum + x(i,1)
                 if(at .lt. absc(sum)*eps) go to 20
               endif
      if(finite) go to 10
      if(j.gt.0 .or. at.gt.atl .or. i.ge.jmax-2) j = j + 1
         if(j.eq.0) go to 10
         call rcf(x(1,1),x(1,2),j,i,x(1,3),eps)
              if(i.lt.0) go to 40
            do 50 k=max(j,2),i
            d = one/(d*x(k,2) + one)
            df = df*(d - one)
            f = f + df
            if(absc(df) .lt. absc(f)*eps) go to 30
            if(df.eq.zero.and.f.eq.zero.and.i.ge.4) go to 30
   50       continue
         j = i
   10 atl = at
      if(.not.finite) i = -jmax
   20 n = i
       f20 = sum
       if(.not.finite) re  = at / absc(sum)
       return
   30 f20 = f
      re = absc(df) / absc(f)
      n = k
      return
   40 i = 0
      go to 20
      end
      function cf1a(rho,eta,xl,psi,eps,nmax,nused,fcl,re,fpmax,xx,g,c)
c
c     evaluate the asymptotic expansion for the
c            logarithmic derivative of the regular solution
c
c ***        cf1a  =  f   =  f'(xl,eta,rho)/f(xl,eta,rho)
c
c      that is valid for dble(rho)>0, and best for rho >> eta**2, xl,
c      and is derived from the 2f0 expansions for h+ and h-
c      e.g. by froeberg (rev. mod. physics vol 27, p399 , 1955)
c      some lines of this subprogram are for convenience copied from
c           takemasa, tamura & wolter cpc 17 (1979) 351.
c
c     evaluate to accuracy eps with at most nmax terms.
c
c     if the terms start diverging,
c     the corresponding continued fraction is found by rcf
c     & evaluated progressively by steed's method to obtain convergence.
c
c      useful number also input:  fpmax = near-largest f.p. number
c
      implicit complex*16(a-h,o-z)
      dimension xx(2,nmax),g(nmax),c(nmax)
      real*8 re,eps,t1,t2,t3,zero,one,two,at,atl,absc,fpmax
      data zero,one,two,ci / 0d+0, 1d+0, 2d+0, (0d+0,1d+0) /
      absc(w) = abs(dble(w)) + abs(imag(w))
c
      hpi = two*atan(one)
      t1 = sin(dble(psi))
      t2 = cos(dble(psi))
      atl= tanh(imag(psi))
c             give cos(psi)/cosh(im(psi)), which always has correct sign
          cosl = dcmplx( t2 , -t1 * atl )
      tanl = dcmplx(t1,t2*atl) / cosl
      re = zero
      xll1= xl*(xl+one)
      etasq = eta*eta
      sl1=one
      sl=sl1
      sc1=zero
      sc=sc1
      tl1=sc
      tl=tl1
      tc1=one-eta/rho
      tc=tc1
      fcl  = tl + sl*tanl
      g(1) = (tc + sc*tanl) / fcl
      glast = g(1)
      atl = absc(glast)
         f    = glast
         d = one
         df   = glast
      j = 0
      do 10 n=2,nmax
      t1=n-1
      t2=two*t1-one
      t3=t1*(t1-one)
      denom=two*rho*t1
      c1=(eta*t2)/denom
      c2=(etasq+xll1-t3)/denom
      sl2=c1*sl1-c2*tl1
      tl2=c1*tl1+c2*sl1
      sc2=c1*sc1-c2*tc1-sl2/rho
      tc2=c1*tc1+c2*sc1-tl2/rho
      sl=sl+sl2
      tl=tl+tl2
      sc=sc+sc2
      tc=tc+tc2
      sl1=sl2
      tl1=tl2
      sc1=sc2
      tc1=tc2
      fcl  =  tl + sl*tanl
         if(absc(fcl).gt.fpmax .or. absc(fcl).lt.1./fpmax) go to 40
      gsum = (tc + sc*tanl) / fcl
      g(n) = gsum - glast
      glast = gsum
         at = absc(g(n))
         if(at.lt.absc(gsum)*eps) go to 20
      if(j.gt.0 .or. at.gt.atl .or. n.ge.nmax-2) j = j + 1
         if(j.eq.0) go to 10
            call rcf(g,c,j,n,xx,eps)
              if(n.lt.0) go to 40
            do 60 k=max(j,2),n
               d = one/(d*c(k) + one)
               df = df*(d - one)
               f = f + df
         if(absc(df) .lt. absc(f)*eps) go to 30
         if(df.eq.zero.and.f.eq.zero.and.n.ge.4) go to 30
   60         continue
         j = n
   10    atl = at
      k = -nmax
      go to 30
   20 fcl = fcl * cosl
         cf1a = gsum
         re = at / absc(gsum)
         nused = n
         return
   30 cf1a = f
      fcl = fcl * cosl
         re = absc(df) / absc(f)
         nused = k
      return
   40 cf1a = g(1)
      fcl = 1.0
      re = 1.0
      nused = 0
      return
      end
      subroutine rcf(a,b,ibeg,inum,xx,eps)
c
c*******************************************************************
c
c  rcf converts polynomial a to the corresponding continued
c         fraction, in 'normal'  form with coefficients b
c         by the 'p algorithmn' of patry & gupta
c
c   a(z) = a1/z + a2/z**3 + a3/z**5 + ... + an/z**(2n-1)
c
c   b(z) = b1/z+ b2/z+ b3/z+ .../(z+ bn/z)
c
c  data:
c   a     vector a(k), k=1,inum         input
c   b     vector b(k), k=ibeg,inum      output
c   ibeg  order of first coef. calc.    input
c   inum  order of a, even or odd       input
c   xx    auxiliary vector of length .ge. length of vector b
c         caller provides space for a,b,xx
c     note that neither of the first two terms a(1) a(2) should be zero
c             & the user can start the calculation with any value of
c                ibeg provided the c.f. coefs have been already
c                calculated up to inum = ibeg-1
c             & the method breaks down as soon as the absolute value
c                of a c.f. coef. is less than eps.    at the time of the
c                break up xx(1) has been replaced by 1e-50, and inum has
c                been replaced by minus times the number of this coef.
c   algorithm: j.patry & s.gupta,
c              eir-bericht nr. 247,
c              eidg. institut fur reaktorforschung wuerenlingen
c              wueringlingen, schweiz.
c              november 1973
c   see also:  haenggi,roesel & trautmann,
c              jnl. computational physics, vol 137, pp242-258 (1980)
c   note:      restart procedure modified by i.j.thompson
c
c*******************************************************************
c
      implicit complex*16(a-h,o-z)
      dimension a(100),b(100),xx(2,100)
      logical even
      real*8 eps
      common /rcfcm2/ x1,m2m1,mp12,even,m
      x0 = 0.0
c     ibn = ibeg + inum - 1
      ibn = inum
c                             b(ibn) is last value set on this call
      if(ibeg.gt.4 .and. m .ne. ibeg-1) go to 90
c                             b(m) is last value set in previous call
      if(ibeg.gt.4) go to 50
      if(ibeg.eq.4) go to 20
      b(1) = a(1)
      if(ibn.ge.2) b(2) = - a(2)/a(1)
      if(ibn.lt.3) go to 10
      x0 = a(3) / a(2)
      xx(2,1) = b(2)
      xx(1,1) = - x0
      xx(1,2) = 0.
      b(3) = -x0 - b(2)
      x0 = -b(3) * a(2)
      m = 3
      mp12 = 2
      even = .true.
      if(ibn.gt.3) go to 20
   10 return
   20 if(abs(b(3)) .lt. eps*abs(x0)) goto 80
      m = 4
   30 x1 = a(m)
      m2m1 = mp12
      mp12 = m2m1 + 1
      if(even) mp12 = m2m1
      do 40 k=2,mp12
   40 x1 = x1 + a(m-k+1) * xx(1,k-1)
      b(m) = - x1/x0
      if(m.ge.ibn) return
   50 if(abs(b(m)).lt.eps*abs(x0)) go to 80
      k = m2m1
   60 xx(2,k) = xx(1,k) + b(m) * xx(2,k-1)
      k = k-1
      if(k.gt.1) go to 60
      xx(2,1) = xx(1,1) + b(m)
      do 70 k=1,m2m1
      x0 = xx(2,k)
      xx(2,k) = xx(1,k)
   70 xx(1,k) = x0
      x0 = x1
      xx(1,m2m1+1) = 0.
      m = m+1
      even = .not.even
      go to 30
   80 inum = -m
c     xx(1,1) = 1.e-50
c     print 1000,m
c1000 format('0rcf: zero cf coefficient at position ',i4/)
      return
   90 print 1000,m,ibeg-1
 1000 format('0rcf: last call set m =',i4,', but restart requires',i4)
      stop
      end

      function clogam(z)
c
c     this routine computes the logarithm of the gamma function gamma(z)
c     for any complex argument 'z' to any accuracy preset by call logam
c
      implicit real*8(a-h,o-z)
      complex*16 z,u,v,h,r,clogam,cdigam,logam,ser
      dimension b(15),bn(15),bd(15)
c
      data lerr /6/, nx0 /6/, nb /15/,
     x  zero,one,two,four,half,quart /0d+0,1d+0,2d+0,4d+0,.5d+0,.25d+0/
      data bn(1),bd(1)    / +1d+0,   6d+0 /,
     x     bn(2),bd(2)    / -1d+0,  30d+0 /,
     x     bn(3),bd(3)    / +1d+0,  42d+0 /,
     x     bn(4),bd(4)    / -1d+0,  30d+0 /,
     x     bn(5),bd(5)    / +5d+0,  66d+0 /,
     x     bn(6),bd(6)    /          -691d+0,  2730d+0/,
     x     bn(7),bd(7)    /          +  7d+0,     6d+0/,
     x     bn(8),bd(8)    /         -3617d+0,   510d+0/,
     x     bn(9),bd(9)    /         43867d+0,   798d+0/,
     x     bn(10),bd(10)  /       -174611d+0,   330d+0/,
     x     bn(11),bd(11)  /        854513d+0,   138d+0/,
     x     bn(12),bd(12)  /    -236364091d+0,  2730d+0/,
     x     bn(13),bd(13)  /     + 8553103d+0,     6d+0/,
     x     bn(14),bd(14)  /  -23749461029d+0,   870d+0/,
     x     bn(15),bd(15)  / 8615841276005d+0, 14322d+0/
      data fplmin / -140d+0 /
      data accur /-1d30/

      save accur, nt, nx0, pi, hl2p, alpi, b

c
      x=dble(z)
      t=imag(z)
      mx = int(dble(accur*100 - x))
      if(abs(abs(x)-mx) + abs(t).lt.accur*50) go to 60
      f=abs(t)
      v=dcmplx(x,f)
      if(x .lt. zero) v=one-v
      h=zero
      c=dble(v)
      n=nx0-int(c)
      if(n .lt. 0) go to 30
      h=v
      d=imag(v)
      a=atan2(d,c)
      if(n .eq. 0) go to 20
      do 10 i = 1,n
      c=c+one
      v=dcmplx(c,d)
      h=h*v
   10 a=a+atan2(d,c)
   20 h=dcmplx(half*log(dble(h)**2+imag(h)**2),a)
      v=v+one
   30 r=one/v**2
      ser = b(nt)
      do 40 j=2,nt
        k = nt+1 - j
   40 ser = b(k) + r*ser
      clogam = hl2p+(v-half)*log(v)-v + ser/v - h
      if(x .ge. zero) go to 50
c
      a= int(x)-one
      c=pi*(x-a)
      d=pi*f
c     e=exp(-two*d)
        e = zero
        f = -two*d
        if(f.gt.fplmin) e = exp(f)
      f=sin(c)
      e= d + half*log(e*f**2+quart*(one-e)**2)
      f=atan2(cos(c)*tanh(d),f)-a*pi
      clogam=alpi-dcmplx(e,f)-clogam
c
   50 if(sign(one,t) .lt. -half) clogam=conjg(clogam)

      return
c
   60 write(lerr,1000) 'clogam',x
 1000 format(1x,a6,' ... argument is non positive integer = ',f20.2)
      clogam = zero
      return
c
      entry cdigam(z)
c
c     this routine computes the logarithmic derivative of the gamma
c     function  psi(z) = digamma(z) = d (ln gamma(z))/dz  for any
c     complex argument z, to any accuracy preset by call logam(acc)
c
      u=z
      x=dble(u)
      a=abs(x)
      if(abs(imag(u)) + abs(a + int(x)) .lt. accur) go to 110
      if(x .lt. zero) u=-u
      v=u
      h=zero
      n=nx0-int(a)
      if(n .lt. 0) go to 90
      h=one/v
      if(n .eq. 0) go to 80
      do 70 i = 1,n
      v=v+one
   70 h=h+one/v
   80 v=v+one
   90 r=one/v**2
      ser = b(nt) * (2*nt-1)
      do 100 j=2,nt
        k = nt+1 - j
  100 ser = b(k)*(2*k-1) + r*ser
      cdigam = log(v) - half/v - r*ser - h
      if(x .ge. zero) return
      h=pi*u
      cdigam = cdigam + one/u + pi*cos(h)/sin(h)
      return
c
  110 write(lerr,1000) 'cdigam',x
      cdigam=zero
      return
c
      entry logam(acc)
c
c      initialisation call for calculations to accuracy 'acc'
c
      
      clogam = zero
      
      if (abs(acc-accur) .le. acc*1d-2) return

      nx0 = 6
      x0  = nx0 + one
      pi = four*atan(one)
      alpi = log(pi)
      hl2p = log(two*pi) * half
      accur = acc
      do 120 k=1,nb
       f21 = k*2 - one
       b(k) = bn(k) / (bd(k) * k*two * f21)
       err = abs(b(k)) * k*two / x0**f21
  120 if(err.lt.acc) go to 130
       nx0 = int((err/acc)**(one/f21) * x0)
       k = nb
  130 nt = k
c     print *,' logam requires k = ',k ,' with cutoff at x =',nx0+1
      return
      end

      function tidy(z,acc)
c                     tidy a complex number
      real*8 x,y,acc,az
      complex*16 z,tidy
c
      x = dble(z)
      y = imag(z)
      az= (abs(x) + abs(y)) * acc * 5
      if(abs(x) .lt. az) x = 0d+0
      if(abs(y) .lt. az) y = 0d+0
      tidy = dcmplx(x,y)
      return
      end
      
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c this simple package implements the quad-precision
c arithmetics for *, /, +, and -, according to the 
c algorithms of linnainmma, s. acm toms. vol. 7, pp. 272.
c taken directly from the pseudo code in the above publication.
c m. f. gu
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc


ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c multiplication of two double precision to yeild a quad 
c precision number. this is the basic building block of 
c the multiplication algorithm.
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine exactmul(a, c, r)
      double precision a, c, r(2)
      
      double precision a1, a2, c1, c2, c21, c22, t

      integer nbits
      double precision const

c     ieee floating point has 53 bits accuracy.
      parameter (nbits = 53)
      parameter (const = 2.0**(nbits - nbits/2))

      t = const*a
      a1 = a - t
      a1 = a1 + t
      a2 = a - a1
      
      t = const*c
      c1 = c - t
      c1 = c1 + t
      c2 = c - c1

      t = c2*const
      c21 = c2 - t
      c21 = c21 + t
      c22 = c2 - c21

      r(1) = a*c
      t = a1*c1
      r(2) = t - r(1)
      t = a1*c2
      r(2) = r(2) + t
      t = c1*a2
      r(2) = r(2) + t
      t = c21*a2
      r(2) = r(2) + t
      t = c22*a2
      r(2) = r(2) + t
      
      end

      
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c multiplication of two quad precision numbers.            
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mpmul(a, c, r)                            
      double precision a(2), c(2), r(2)                    
      double precision z(2), t, d                          
                                                           
      call exactmul(a(1), c(1), z)                         
      t = a(1)*c(2)                                        
      d = a(2)*c(1)                                        
      t = t + d                                            
      t = t + z(2)                                         
                                                           
      r(1) = z(1) + t                                      
      d = z(1) - r(1)
      r(2) = d + t                                         
                                                           
      end                                                  
                                                           
                                                           
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c division of two quad precision numbers.                  
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mpdiv(a, c, r)                            
      double precision a(2), c(2), r(2)                    
      double precision q(2), z(2), t, d                    
                                                           
      z(1) = a(1)/c(1)                                     
      call exactmul(c(1), z(1), q)                         
      t = a(1) - q(1)                                      
      t = t - q(2)                                         
      t = t + a(2)                                         
      d = z(1)*c(2)                                        
      t = t - d                                            
      z(2) = t/c(1)                                        
                                                           
      r(1) = z(1) + z(2)                                   
      d = z(1) - r(1)                                      
      r(2) = d + z(2)                                      
                                                           
      end                                                  
                                                           
                                                           
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c addition of two quad precision numbers.                  
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mpadd(a, c, r)                            
      double precision a(2), c(2), r(2)                    
      double precision z(2), q, t, d                       
                                                           
      z(1) = a(1) + c(1)                                   
      q = a(1) - z(1)                                      
      t = q + z(1)                                         
      d = a(1) - t                                         
      t = q + c(1)                                         
      d = t + d                                            
      d = d + a(2)                                         
      z(2) = d + c(2)                                      
                                                           
      r(1) = z(1) + z(2)                                   
      t = z(1) - r(1)                                      
      r(2) = t + z(2)                                      
                                                           
      end                                                  
                                                           
                                                           
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c subtraction of two quad precision numbers.               
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mpsub(a, c, r)                            
      double precision a(2), c(2), r(2)                    
      double precision d(2)                                
                                                           
      d(1) = -c(1)                                         
      d(2) = -c(2)                                         
      call mpadd(a, d, r)                                  
                                                           
      end                                                  
                                                           
                                                           
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c asign 1 quad precision number to another                 
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mpeq(s, d)                                
      double precision d(2), s(2)                          
                                                           
      d(1) = s(1)                                          
      d(2) = s(2)                                          
                                                           
      end                                                  
                                                           
                                                           
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c convert double to quad precision                         
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mpdmc(h, l, r)
      double precision h, l, r(2)

      r(1) = h
      r(2) = l

      end
                    
                                       
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c abs value of a quad precision number
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      subroutine mpabs(a, r)
      double precision a(2), r(2)
      
      if (a(1) .lt. 0) then
         r(1) = -a(1)
         r(2) = -a(2)
      else 
         r(1) = a(1)
         r(2) = a(2)
      endif

      end

      subroutine coulfg(rho,eta,minl,maxl,fc,fcp,gc,gcp,accur,iret)
      implicit real*8(a-h,o-z)
c
c     computes regular and irregular coulomb wavefunctions.
c
c       this subroutine returns the regular and irregular coulomb
c     wavefunctions and their derivatives for specific values of
c     eta and rho and for a range of l's.  the range of l's (orbital
c     angular momentum) need not begin at l = 0 and, in general,
c     it will be less time consuming to request only those l's
c     that are actually desired.  the conventions of abramowitz
c     and stegun (handbook of mathematical functions) are used.
c
c       this subroutine is an adaptation of the manchester subroutine
c     of the same name (but slightly different argument list) that
c     was published in computer physics communications 8, 377 (1974).
c     by barnett, feng, steed and goldfarb.  the continued fractions
c     introduced in that article are used in the range rho >
c     rho(turn) but completely different procedures are used
c     for rho < rho(turn).  the new procedures allow results
c     for rho < rho(turn) of accuracy comparable to those for
c     rho > rho(turn) in comparable time.
c
c       one of the continued fractions is not convergent for very
c     small rho and thus this routine will find only f and f' for
c     rho < .005.  in addition the routine becomes rather slow for
c     rho < .1.  for very large rho ( >> 1000 ), another continued
c     fraction converges very slowly and the subroutine will fail
c     to converge in the maximum allowed number of iterations for
c     rho > 9500 with the exception that larger rho are possible
c     if eta is near rho/2.
c
c       aside from the above limitations, the subroutine has a very
c     large range of eta,  rho, and l for which it will return
c     reliable values in reasonable times.  it has been tested for
c     -2000 < eta < 5000,  1e-6 < rho < 10000;  0 < l < 1000 .
c     the tests consisted of comparasons to existing routines to
c     locate coding errors and of comparasons to quadruple precision
c     results to determine the numerical stability of the algorithms.
c     however, no verification of the correctness of g or g' for
c     eta < 0 has been made.
c
c
c     arguments (all floating point arguments are double precision) -
c
c     rho - the value of the radial coordinate at which f, f', g, and
c           g' are desired.  0 < rho < 9500 is required.  if
c           rho < .005, only f and f' will be found.
c
c     eta - the value of the charge parameter to be used.  the routine
c           has been tested for -2000 < eta < 5000.  eta = 0 will
c           result in the computation of the spherical bessel
c           functions times rho.
c
c     minl - the minimum value of l for which the functions are desired.
c     maxl - the maximum value of l.
c
c     fc, fcp, gc, and gcp - these one dimensional arrays will be
c           set to the computed values of f, f', g, and g' respectively.
c           each array must be of length at least maxl+1 and will
c           be set as
c             array(l+1) = function(l)   minl =< l =< maxl .
c           depending on eta and rho, the first minl elements of
c           each array may be used for intermediate computations and
c           their values are not predictable upon exit from coulfg.
c
c     accur - the desired accuracy of convergence of the continued
c          fractions and other series.  in general this will be
c          the relative accuracy of the final values except near
c          a zero of one of the functions.  accur should be in
c          the range
c              1e-6 < accur < 1e-15 ,
c          if it is not, one of the above two values will be used.
c
c     iret - this integer is set to a return code to indicate that
c          coulfg was successful or the reason for failure:
c        0 - all o.k.; all functions found.
c        1 - some o.k., those for higher l's have under/over-flowed
c        2 - rho < .005; only f, f' were computed.
c        3 - rho < .005; only f, f' were computed and some of them
c                        underflowed.
c        4 - all will under/over-flow; no results defined.
c        5 - failed to avoid divide by zero in f'/f loop
c        6 - nonconvergence of r
c        7 - nonconvergence of p + iq
c        8 - nonconvergence of maclauren series
c        9 - nonconvergence of the taylor series about rho = 2*eta
c     iret's of 5 to 9 should be reported to steve pieper ( x4523 )
c
c       the following table gives sample execution times on the
c     /75 and /195.  times are for accur = 1d-14 and for
c     minl = maxl = 0.  in general execution time is weakly dependant
c     on accur and minl.  if maxl > minl, the times required for
c     each additional l are approximatley 0.01 millisec on the /195
c     and 0.15 millisec on the /75.  times for eta < 0 are comparable to
c     to those for !eta!.  the final column of the table gives
c     the number of bits lost due to round off and truncation
c     errors and indicates the maximum precision possible on a
c     given machine.  the /360 and /370 have 56 bits of precision
c     (16.8 decimal places) and each 3.3 bits lost represents
c     one decimal place lost.
c
c      eta    rho     time in milliseconds    bits lost
c                       /195      /75
c
c        0.      .01     .15       1.2            3
c        0.     1.       .13       1.2            3
c        0.   100.      1.0       11.            11
c        0.  1000.      7.6       81.
c        1.      .1     5.4       81.             9
c        1.     1.      1.0       12.5            8
c        1.    10.       .33       3.7            3
c        1.   100.      1.1       11.
c        1.  1000.      8.1       81.
c       10.     1.      1.5       20.             6
c       10.    10.       .8        8.3            6
c      100.    75.      1.9       21.            10
c      100.   100.      2.0       21.            10
c      100.  1000.      7.3       74.            13
c     1000.  5000.     29.       385.            16
c
c
c     may 23, 1976 - revised version by s. pieper.
c     nov 19, 1976 - fix choice of root for rho << eta, eta > 10
c
c
      logical frstsw
      dimension fc(1),fcp(1),gc(1),gcp(1),
     1   iveryb(4)
c
c     vrybig is the result of an overflow
c     big is representative of max number, small is its inverse
c     smalln is  log(small)  (must be accurate)
c     precis is about  100*(machine precision)
c     precln is  > -!log(machine precision)!
c     prert3 is like the cuberoot of the machine precision
c     data concerning vax machine
c
c      data prert3 / 2.1544d-5 /,  precis / 1.d-12 /,precln /32.2362d0/
c      data pi / 3.1415926535897932d0/,
c     2   big/1.7976931348623158D+308/,
c     3   small/2.2250738585072014d-308/,
c     1   vrybig/1.D+309/smalln/-7.08396418532264079d+2/
      data prert3 / 2.d-5 /,  precis / 1.d-13 /,precln /32.2d0/
      data pi / 3.1415926535897932d0/,
     2   big/1.D+300/,
     3   small/1.d-300/,
     1   vrybig/1.D+301/smalln/-6.90775527898213705d+2/
c
c     coulomb wavefunctions calculated at rho = rho by the
c     continued-fraction method of steed   minl,maxl are actual l-values
c     see barnett feng steed and goldfarb computer physics common 1974
c
c
c     here we limit accuracy to reasonable values for the machine
c
      acc  = accur
      acc = dmax1( acc, precis )
      acc = dmin1( acc, prert3 )
C      vrybig=big*big
c
      lmax = maxl
      lmin = minl
      lmin1= lmin + 1
      xll1 = lmin*lmin1
      eta2 = eta*eta
c
c     determin which region we are in
c
c     for rho < .45, q of p+iq is poorly determined so we don't use it.
c     except that for large negative eta the maclauren series also
c     has problems
c
      if ( rho .gt. .45d0 )  go to 20
      if ( eta .ge. 0 )  go to 10
      if ( -eta*rho .gt. 7 )  go to 20
c
c     for rho < .005, we only return f and f' since the p+iq recursion
c     is very slowly convergent  ( for very small eta it is possible
c     to go to smaller rho ( rho > 1e+4*eta )  but we ignor that here.
c
 10   if ( rho .gt. .005d0 )  go to 60
      igoto = 5
      go to 70
c
 20   turn = eta + sqrt(eta2 + xll1)
      igoto = 1
      if ( rho .ge. turn-1.e-4 )  go to 100
c
c     we are inside the turning point for minl, can we get outside
c     of it by reducing minl. (this is always possible for
c     eta < 0).
c
      if ( rho .lt. eta+abs(eta) )  go to 60
c
c     yes, do so - this is the same as  rho > rho(turn)  except
c     we generate some extra f(l), g(l) for  l < minl
c
      lmin = .5*( sqrt(1+4*((rho-eta)**2-eta2)) - 1 )
      lmin1 = lmin + 1
      go to 80
c
c     must use a different method to suppliment the bad i q
c     value.  always start with lmin = 0 for simplicity.
c
c     note only eta > 0 gets to here ( except when rho < .45 )
c
 60   igoto = 2
 70   lmin = 0
      lmin1 = 1
      if ( eta .lt. 10  .or.  rho .le. eta )  go to 80
      igoto = 3
c
 80   xll1 = lmin*lmin1
c
c     here we compute  f'/f  for l = maxl
c     we then recurse down to lmin to generate the unnormalized f's
c     this section is used for all rho.
c
 100  pl   = lmax + 1
      rhouse = rho
 105  plsave = pl
 110  frstsw = .true.
c     continued fraction for  r = fp(maxl)/f(maxl)
      r  = eta/pl + pl/rhouse
      dq  = (eta*rhouse)*2.0 + 6*pl**2
      dr = 12*pl + 6
      del = 0.0
      d   = 0.0
      f   = 1.0
      x   = (pl*pl - pl + (eta*rhouse))*(2.0*pl - 1.0)
      ai = rhouse*pl**2
      di = (2*pl+1)*rhouse
c
c     loop and converge on r
c
      do 139  i = 1, 100000
         h = (ai + rhouse*eta2)*(rhouse - ai)
         x   = x + dq
         d = d*h + x
c
c     if we pass near a zero of the divisor, start over at
c     larger lmax
c
         if ( abs(d) .gt. prert3*abs(dr) )  go to 130
         pl = pl + 1
         if ( pl .lt. plsave+10 )  go to 110
         iret = 5
         return
c
 130     d = 1/d
         dq = dq + dr
         dr = dr + 12
         ai = ai + di
         di = di + 2*rhouse
         del =  del*(d*x - 1.0)
         if (frstsw) del = -rhouse*(pl*pl + eta2)*(pl + 1.0)*d/pl
         frstsw = .false.
         r  = r + del
         if(d.lt.0.0) f = -f
         if ( abs(del) .lt. abs(r*acc) )  go to 140
 139  continue
      iret = 6
      return
c
c     r has converged;  did we increase lmax
c
 140  if ( pl .eq. plsave )  go to 160
c
c     recurse down on r to lmax
c     here the only part of f that is of interest is the sign
c
      pl = pl-1
 150     d = eta/pl + pl/rhouse
         f = (r+d)*f
         r = d - (1+eta2/pl**2)/(r+d)
         pl = pl - 1
         if ( pl .gt. plsave )  go to 150
c
c     now have r(lmax, rho) or if igoto=4, r(lmin, 2*eta)
c
 160  if ( igoto .eq. 4 )  go to 210
      fc (lmax+1) = f
      fcp(lmax+1) = f*r
      if( lmax.eq.lmin) go to 200
c     downward recursion to lmin for f and fp, arrays gc,gcp are storage
      l  = lmax
      pl = lmax
      ar = 1/rho
      do 189 lp  = lmin1,lmax
         gc (l+1) = eta/pl + pl*ar
         gcp(l+1) = sqrt( (eta/pl)**2 + 1 )
         fc (l)   = (gc(l+1)*fc(l+1) + fcp(l+1))/gcp(l+1)
         fcp(l)   =  gc(l+1)*fc(l)   - gcp(l+1)*fc(l+1)
         pl = pl - 1
         l  = l - 1
c
c     if we are getting near an overflow, renormalize everything down
c
         if ( abs(fc(l+1)) .lt. big )  go to 189
         do 179  ll = l, lmax
            fc(ll+1) = small*fc(ll+1)
            fcp(ll+1) = small*fcp(ll+1)
 179     continue
 189  continue
      f  = fc (lmin1)
      r = fcp(lmin1)/f
c
c     here we find
c        p + iq  =  (g'+if')/(g+if)
c     this section is used in all cases except when
c        15 < eta < rho < 2*eta
c
 200  if ( igoto .eq. 3 )  go to 500
      if ( igoto .eq. 5 )  go to 400
c
c     now obtain p + i.q for lmin from continued fraction (32)
c     real arithmetic to facilitate conversion to ibm using real*8
 210  p  = 0.0
      q  = rhouse - eta
      pl = 0.0
      ar = -(eta2 + xll1)
      ai =   eta
      br = q + q
      bi = 2.0
      wi = eta + eta
      dr =   br/(br*br + bi*bi)
      di =  -bi/(br*br + bi*bi)
      dp = -(ar*di + ai*dr)
      dq =  (ar*dr - ai*di)
c
c     loop and converge on p + iq
c
 230     p  =  p + dp
         q  =  q + dq
         pl = pl + 2.0
         ar = ar + pl
         ai = ai + wi
         bi = bi + 2.0
         d  = ar*dr - ai*di + br
         di = ai*dr + ar*di + bi
         t  = 1.0/(d*d + di*di)
         dr =  t*d
         di = -t*di
         h  = br*dr - bi*di - 1.0
         x  = bi*dr + br*di
         t  = dp*h  - dq*x
         dq = dp*x  + dq*h
         dp = t
         if(pl.gt.46000.) go to 920
         if(abs(dp)+abs(dq).ge.(abs(p)+abs(q))*acc) go to 230
      p  = p/rhouse
      q  = q/rhouse
c
c     we now have  r  and  p+iq,  is this enough
c
      if ( igoto .eq. 2 )  go to 400
c
c     solve for fp,g,gp and normalise f  at l=lmin
c
c     since this is for  rho > rho(turn), f and g are reasonable
c     numbers
c
      x = (r-p)/q
      fmag = sqrt( 1/(q*(1+x**2)) )
      w = fmag/abs(f)
      f = w*f
      g = f*x
      gp = r*g - 1/f
      if ( igoto .eq. 4 )  go to 600
      go to 800
c
c     here   rho < eta  or  rho < 2*eta < 20  or  rho < .45
c     we use the maclauren series to get  f( l=0, eta, rho )
c
c     first compute  rho*c(l=0, eta)
c
 400  c = 2*pi*eta
      if ( abs(c) .gt. .5 )  go to 410
c
c     use maclaurin expansion of  x / (exp(x)-1)
c
      x = 0
      t = 1
      ar = 1
      br = c
      ai = 1
      c = 1
 405     ai = ai + 1
         ar = ar*br/ai
         c = c + ar
         if ( abs(ar) .ge. acc*c )  go to 405
      c = 1/c
      go to 430
c
c     here eta is not tiny.
c
 410  if ( eta .gt. 0 )  go to 420
      c = -c
      x = 0
      t = 1
      go to 425
 420  x = -smalln - pi*eta
      t = small
 425  if ( c .lt. precln )  c = c / (1-exp(-c))
 430  c = rho*sqrt(c)
      b1 = 1
      b2 = eta*rho
      sum = b1 + b2
      ai = 6
      di = 6
      do 449  i = 1, 10000
         b3 = ( (2*eta*rho)*b2 - (rho**2)*b1 ) / ai
         ai = ai + di
         di = di + 2
         sum = sum + b3
         stop = abs(b1) + abs(b2) + abs(b3)
         b1 = b2
         b2 = b3
         if ( abs(sum) .lt. big )  go to 445
         x = x - smalln
         sum = sum*small
         b1 = b1*small
         b2 = b2*small
 445     if ( stop .lt. acc*abs(sum) )  go to 450
 449  continue
      iret = 8
      return
c
 450  sum = ( c*exp(x)*sum ) * t
c
c     did it underflow
c
      if ( sum .eq. 0 )  go to 900
c
c     we now have  f (=sum),  r,  and p  ( p only if rho > .005 )
c     use the wronskian as the 4th condition
c
      w = sum/f
      f = sum
      if ( igoto .eq. 5 )  go to 850
      x = (r-p)*f
      if ( abs(x) .gt. prert3 )  go to 480
c
c     here f**3 and f**4 terms are less than machine precision
c
      g = 1/x
      gp = p*g
      go to 800
c
c     here we must include f**3, f**4; we must also worry about
c     which sign of the root to use.
c     the positive root applies for g > f; else the negative root
c     we use q in determining which is correct
c
 480  b1 = .5/x
      b2 = b1 * sqrt(1-4*(x*f)**2)
      g = b1 + b2
c     g > f in all of region 2 for eta > 0
      if ( eta .ge. 0 )  go to 490
      sum = 1/q - f**2
      gp = b1 - b2
      if ( abs(g**2-sum) .gt. abs(gp**2-sum) )  g = gp
 490  gp = p*g - x*f/g
      go to 800
c
c     eta > 15  and  eta < rho < 2*eta
c
c     we find g and g' for lmin, rho=2*eta using the above method
c     consisting of r, p+iq, and w.
c
 500  rhouse = eta+eta
      pl = lmin+1
      igoto = 4
      go to 105
c
c     now we have  g, g'  at the turning point, go in using taylor
c
c
 600  del = rhouse - rho
      b1 = g
      b2  = -del*gp
      b3 = 0
      g = b1+b2
      accr = acc/2
      delinv = -1/del
      dfactr = 3*delinv
      x = del/rhouse
      ai = x+x
      di = ai+ai
      ar = 6
      dr = 6
      do 639  i = 1, 10000
         s = ( ai*b3 + (x*del**2)*b1 ) / ar
         ar = ar + dr
         dr = dr + 2
         ai = ai + di
         di = di + 2*x
         g = g + s
         gp = gp + dfactr*s
         if ( g .ge. vrybig )  go to 900
         dfactr = dfactr + delinv
         b1 = b2
         b2 = b3
         b3 = s
         if ( s .lt. accr*g )  go to 650
 639  continue
      iret = 9
      return
c
c     here we have  r = f'/f,  g,  g'
c     use wronskian as the 4th condition
c
 650  f = fc(lmin1)
      r = fcp(lmin1)/f
      sum = 1/(r*g-gp)
      w = sum/f
      f = sum
c
c     we now have  f, r = f'/f, g, g'  at lmin
c
c     upward recursion from gc(lmin) and gcp(lmin),stored values are rho
c     renormalise fc,fcp for each l-value
c
 800  gc (lmin1) = g
      gcp(lmin1) = gp
      fc(lmin1) = f
      fcp(lmin1) = r*f
      iret = 0
      if(lmax.eq.lmin)  return
      do  829  l = lmin1,lmax
         t        = gc(l+1)
         gc (l+1) = (gc(l)*gc (l+1) - gcp(l))/gcp(l+1)
         gcp(l+1) =  gc(l)*gcp(l+1) - gc(l+1)*t
         fc (l+1) = w*fc (l+1)
 829     fcp(l+1) = w*fcp(l+1)
 840  if ( abs(fc(lmax+1))+abs(fcp(lmax+1)) .eq. 0 )  iret = iret+1
      return
c
c     rho < .005;  we cannot find p or q and so return only f, f'.
c
 850  fc(lmin1) = f
      fcp(lmin1) = r*f
      iret = 2
      if ( lmax .eq. lmin )  return
      do 859  l = lmin1, lmax
         fc(l+1) = w*fc(l+1)
         fcp(l+1) = w*fcp(l+1)
 859  continue
      go to 840
c
c     f and g are out of the machine exponent range for lmin.
c     it will be even worse for  l > lmin  so give up and return
c
 900  iret = 4
      return
c
c     p + iq failed to converge
 920  iret = 7
      return
c
      end