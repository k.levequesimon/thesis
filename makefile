.SUFFIXES: .inc .f .f90 .F

FC = mpiifort
FFLAGS = -O3 

main: gauss.o source.o bessel.o wigner.o coulomb.o tqli.o hypergeometric.o ingamma.o hartree.o octopus.o main.o 
	$(FC) $(FFLAGS) *.o -o main.x
gauss.o: gauss.f90 
	$(FC) $(FFLAGS) -c gauss.f90
source.o: source.f90 
	$(FC) $(FFLAGS) -c source.f90
bessel.o: bessel.f90 
	$(FC) $(FFLAGS) -c bessel.f90
wigner.o: wigner.f 
	$(FC) $(FFLAGS) -c wigner.f
coulomb.o: coulomb.f 
	$(FC) $(FFLAGS) -c coulomb.f
tqli.o: tqli.f 
	$(FC) $(FFLAGS) -c tqli.f
hypergeometric.o: hypergeometric.f90 
	$(FC) $(FFLAGS) -c hypergeometric.f90
ingamma.o: ingamma.f90 
	$(FC) $(FFLAGS) -c ingamma.f90
hartree.o: hartree.f90 
	$(FC) $(FFLAGS) -c hartree.f90
octopus.o: octopus.f90 
	$(FC) $(FFLAGS) -c octopus.f90
main.o: main.f90 
	$(FC) $(FFLAGS) -c main.f90
