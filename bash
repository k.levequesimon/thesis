#!/bin/bash 
nonp=8
ngpt=81
ngpx=96
ngpu=98
gamfs=0
gamis=0
par=1
id=po
np=2
lp=0
nh=2
lh=0
pmax=2
pmin=1
acs=o
ais=o
afs=o
f=0
omega=0.1837
ekmax=1000
h=1
thres=manu
ekmin=1000
form=cba
mode=chem
sens=dir
target=PsH
approx=fci
basis=aug-cc-pv5z
method=g
echo $nonp> para
echo $ngpt>> para
echo $ngpx>> para
echo $ngpu>> para
echo $gamfs>> para
echo $gamis>> para
echo $par>> para
echo $id>> para
echo $np>> para
echo $lp>> para
echo $nh>> para
echo $lh>> para
echo $pmax>> para
echo $pmin>> para
echo $acs>> para
echo $ais>> para
echo $afs>> para
echo $f>> para
echo $omega>> para
echo $ekmax>> para
echo $h>> para
echo $thres>> para
echo $ekmin>> para
echo $form>> para
echo $mode>> para
echo $sens>> para
echo $target>> para
echo $approx>> para
echo $basis>> para
echo $method>> para
